import 'package:get_it/get_it.dart';
import 'package:jma/core/data/remote/data_sources/server_date_api_interface.dart';
import 'package:jma/core/data/remote/data_sources/server_date_remote_data_source.dart';
import 'package:jma/core/data/repositories/server_date_repository_impl.dart';
import 'package:jma/core/domain/repositories/server_date_repository.dart';
import 'package:jma/core/domain/use_case/get_server_date_use_case.dart';
import 'package:jma/core/presentation/manager/export/cubit.dart';
import 'package:jma/core/presentation/manager/server_date_cubit.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/aid_request/data/remote/data_sources/aid_request_api_interface.dart';
import 'package:jma/features/aid_request/data/remote/data_sources/aid_request_remote_data_soruce.dart';
import 'package:jma/features/aid_request/data/repositories/aid_request_repository_impl.dart';
import 'package:jma/features/aid_request/domain/repositories/aid_request_repository.dart';
import 'package:jma/features/aid_request/domain/use_cases/get_aid_requests_file_use_case.dart';
import 'package:jma/features/aid_request/domain/use_cases/get_aid_requests_meter_use_case.dart';
import 'package:jma/features/aid_request/domain/use_cases/get_aid_requests_use_case.dart';
import 'package:jma/features/aid_request/domain/use_cases/get_last_aid_requests_use_case.dart';
import 'package:jma/features/aid_request/presentation/manager/aid_request_cubit.dart';
import 'package:jma/features/auth/data/remote/data_sources/auth_api_interface.dart';
import 'package:jma/features/auth/data/remote/data_sources/auth_remote_data_source.dart';
import 'package:jma/features/auth/data/repositories/auth_repository_impl.dart';
import 'package:jma/features/auth/domain/repositories/auth_repository.dart';
import 'package:jma/features/auth/domain/use_cases/login_use_case.dart';
import 'package:jma/features/auth/domain/use_cases/logout_use_case.dart';
import 'package:jma/features/auth/presentation/manager/auth_cubit.dart';
import 'package:jma/features/customer_comparison/data/remote/data_sources/customer_comparison_api_interface.dart';
import 'package:jma/features/customer_comparison/data/remote/data_sources/customer_comparisons_remote_data_source.dart';
import 'package:jma/features/customer_comparison/data/repositories/customer_comparison_repository_impl.dart';
import 'package:jma/features/customer_comparison/domain/repositories/customer_comparison_repository.dart';
import 'package:jma/features/customer_comparison/domain/use_cases/export_all_use_case.dart';
import 'package:jma/features/customer_comparison/domain/use_cases/get_fullm_customer_comparisons_use_case.dart';
import 'package:jma/features/customer_comparison/domain/use_cases/get_nom_customer_comparisons_use_case.dart';
import 'package:jma/features/customer_comparison/domain/use_cases/get_simim_customer_comparisons_use_case.dart';
import 'package:jma/features/customer_comparison/presentation/manager/customer_comparison_cubit.dart';
import 'package:jma/features/customer_comparison/presentation/manager/export_all_cubit.dart';
import 'package:jma/features/dashboard/data/remote/data_sources/dashboard_api_interface.dart';
import 'package:jma/features/dashboard/data/remote/data_sources/dashboard_remote_data_source.dart';
import 'package:jma/features/dashboard/data/repositories/dashboard_repository_impl.dart';
import 'package:jma/features/dashboard/domain/repositories/dashboard_repository.dart';
import 'package:jma/features/dashboard/domain/use_cases/get_totals_use_case.dart';
import 'package:jma/features/dashboard/presentation/manager/dashboard_cubit.dart';
import 'package:jma/features/summary/data/remote/data_sources/summary_api_interface.dart';
import 'package:jma/features/summary/data/remote/data_sources/summary_remote_data_source.dart';
import 'package:jma/features/summary/data/repositories/summary_repository_impl.dart';
import 'package:jma/features/summary/domain/repositories/summary_repository.dart';
import 'package:jma/features/summary/domain/use_cases/get_province_summary_totals_use_case.dart';
import 'package:jma/features/summary/domain/use_cases/get_summary_totals_use_case.dart';
import 'package:jma/features/summary/presentation/manager/date_cubit.dart';
import 'package:jma/features/summary/presentation/manager/summary_cubit.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //! Features - Auth
  // Bloc
  sl.registerFactory(
    () => AuthCubit(loginUseCase: sl()),
  );

  //Use Cases
  sl.registerLazySingleton(() => LoginUseCase(repository: sl()));
  sl.registerLazySingleton(() => LogoutUseCase(repository: sl()));

  //Repositories
  sl.registerLazySingleton<AuthRepository>(
    () => AuthRepositoryImpl(
      authRemoteDataSource: sl(),
    ),
  );

  // Data sources
  sl.registerLazySingleton<AuthRemoteDataSource>(
    () => AuthRemoteDataSourceImpl(apiInterface: sl()),
  );

  //! Features - Dashboard
  // Bloc
  sl.registerFactory(
    () => DashboardCubit(getTotalsUseCase: sl()),
  );

  //Use Cases
  sl.registerLazySingleton(() => GetTotalsUseCase(repository: sl()));

  //Repositories
  sl.registerLazySingleton<DashboardRepository>(
    () => DashboardRepositoryImpl(
      remoteDataSource: sl(),
    ),
  );

  // Data sources
  sl.registerLazySingleton<DashboardRemoteDataSource>(
    () => DashboardRemoteDataSourceImpl(apiInterface: sl()),
  );

  //! Features - Aid Request
  // Bloc
  sl.registerFactory(
    () => AidRequestCubit(
        getLastAidRequestsUseCase: sl(),
        getAidRequestsUseCase: sl(),
        getAidRequestsUseFileNoCase: sl(),
        getAidRequestsUseMeterNoCase: sl()),
  );

  //Use Cases
  sl.registerLazySingleton(() => GetLastAidRequestsUseCase(repository: sl()));
  sl.registerLazySingleton(() => GetAidRequestsUseCase(repository: sl()));
  sl.registerLazySingleton(
      () => GetLastAidRequestsFileNoUseCase(repository: sl()));
  sl.registerLazySingleton(
      () => GetLastAidRequestsMeterNoUseCase(repository: sl()));
  //Repositories
  sl.registerLazySingleton<AidRequestRepository>(
    () => AidRequestRepositoryImpl(
      aidRequestRemoteDataSource: sl(),
    ),
  );

  // Data sources
  sl.registerLazySingleton<AidRequestRemoteDataSource>(
    () => AidRequestRemoteDataSourceImpl(apiInterface: sl()),
  );

  //! Features - Customer Comparison
  // Bloc
  sl.registerFactory(
    () => CustomerComparisonCubit(
      getFullMCustomerComparisonsUseCase: sl(),
      getNoMCustomerComparisonsUseCase: sl(),
      getSimiMCustomerComparisonsUseCase: sl(),
    ),
  );
  sl.registerFactory(
    () => ExportAllCubit(
      sl(),
    ),
  );
  //Use Cases
  sl.registerLazySingleton(
      () => GetFullMCustomerComparisonsUseCase(repository: sl()));
  sl.registerLazySingleton(
      () => GetSimiMCustomerComparisonsUseCase(repository: sl()));
  sl.registerLazySingleton(
      () => GetNoMCustomerComparisonsUseCase(repository: sl()));
  sl.registerLazySingleton(() => ExportAllUseCase(repository: sl()));

  //Repositories
  sl.registerLazySingleton<CustomerComparisonRepository>(
    () => CustomerComparisonRepositoryImpl(
      customerComparisonsRemoteDataSource: sl(),
    ),
  );

  // Data sources
  sl.registerLazySingleton<CustomerComparisonsRemoteDataSource>(
    () => CustomerComparisonsRemoteDataSourceImpl(apiInterface: sl()),
  );

  //! Features - Summary
  // Bloc
  sl.registerFactory(
    () => SummaryCubit(sl(), sl()),
  );
  sl.registerFactory(
    () => DateCubit(),
  );

  //Use Cases
  sl.registerLazySingleton(
    () => GetSummaryTotalsUseCase(repository: sl()),
  );

  sl.registerLazySingleton(
    () => GetProvinceSummaryTotalsUseCase(repository: sl()),
  );

  //Repositories
  sl.registerLazySingleton<SummaryRepository>(
    () => SummaryRepositoryImpl(
      summaryRemoteDataSource: sl(),
    ),
  );

  // Data sources
  sl.registerLazySingleton<SummaryRemoteDataSource>(
    () => SummaryRemoteDataSourceImpl(apiInterface: sl()),
  );

  //! Core
  sl.registerFactory(
    () => ServerDateCubit(sl()),
  );
  sl.registerFactory(
    () => ExportCubit(),
  );

  sl.registerLazySingleton(
    () => GetServerDateUseCase(sl()),
  );

  sl.registerLazySingleton<ServerDateRepository>(
    () => ServerDateRepositoryImpl(
      sl(),
    ),
  );

  sl.registerLazySingleton<ServerDateRemoteDataSource>(
    () => ServerDateRemoteDataSourceImpl(apiInterface: sl()),
  );

  // sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  final dio = appDio;
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => dio);
  sl.registerLazySingleton(() => AuthAPIInterface(sl()));
  sl.registerLazySingleton(() => DashboardAPIInterface(sl()));
  sl.registerLazySingleton(() => AidRequestApiInterface(sl()));
  sl.registerLazySingleton(() => CustomerComparisonAPIInterface(sl()));
  sl.registerLazySingleton(() => SummaryAPIInterface(sl()));
  sl.registerLazySingleton(() => ServerDateAPIInterface(sl()));
}
