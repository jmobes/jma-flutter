import 'package:flutter/cupertino.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class CustomDataGrid<T> extends DataGridCell {
  final String cName;
  final T value;
  final bool isNumber;
  CustomDataGrid(
    this.cName,
    this.value, {
    this.isNumber = false,
  }) : super(columnName: cName, value: value);
}

class CustomDataGrid2<T> extends DataGridCell {
  final String cName;
  final T value;
  final bool isNumber;
  final bool isText;
  final VoidCallback? historyClick;
  CustomDataGrid2(
    this.cName,
    this.value, {
    this.isNumber = false,
    this.isText = true,
    this.historyClick,
  }) : super(columnName: cName, value: value);
}
