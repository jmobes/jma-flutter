import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class CustomFlag extends StatelessWidget {
  CustomFlag(
      {Key? key,
      required String title,
      required String flag,
      required Locale lang})
      : _title = title,
        _flag = flag,
        _lang = lang,
        super(key: key);

  final Locale _lang;
  final String _flag;
  final String _title;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        //  EasyLocalization.of(context) != null ? EasyLocalization.of(context).setLocale(_lang) : null
        EasyLocalization.of(context)?.setLocale(_lang);
        Navigator.pop(context);
      },
      child: Container(
        width: 100,
        height: 130,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(_flag),
                  ),
                ),
              ),
              Text(_title.tr())
            ],
          ),
        ),
      ),
    );
  }
}
