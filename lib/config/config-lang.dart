import 'package:flutter/material.dart';

class ConfigLanguage {
  static final LANG_PATH = 'assets/Lang';
  static final EN_LOCALE = Locale('en', 'US');
  static final AR_LOCALE = Locale('ar', 'EG');
}
