import 'package:easy_localization/easy_localization.dart' hide TextDirection;
import 'package:flutter/material.dart';
import 'package:jma/config/custom_data_grid.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:jma/core/utils/key-lang.dart';
import 'package:jma/features/summary/domain/entities/summary_totals.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class SummaryAdapter extends DataGridSource {
  /// Creates the employee data source class with required details.
  SummaryAdapter(List<SummaryTotals> summaryTotals) {
    _summarytData =
        summaryTotals.map<DataGridRow>((SummaryTotals summaryTotals) {
      String cat;
      String value;
      if (summaryTotals.requestDate == null) {
        cat = KeyLang.month.tr();
        value = summaryTotals.month.toString();
      } else {
        cat = KeyLang.requestDate.tr();
        value = summaryTotals.requestDate.toString();
      }
      return DataGridRow(cells: <CustomDataGrid>[
        CustomDataGrid<String>(
            KeyLang.customers.tr(), summaryTotals.totalCustomers.toString()),
        CustomDataGrid<String>(
          KeyLang.requests.tr(),
          summaryTotals.totalRequests.toString(),
        ),
        CustomDataGrid<String>(KeyLang.totalCustomerDeserverAidRequest.tr(),
            summaryTotals.totalAidRequests.toString()),
        CustomDataGrid<String>(
            KeyLang.totalCustomerCancelDeserverRequestAid.tr(),
            summaryTotals.totalCancelAidRequests.toString()),
        CustomDataGrid<String>(cat, value),
      ]);
    }).toList();
  }

  List<DataGridRow> _summarytData = <DataGridRow>[];

  @override
  List<DataGridRow> get rows => _summarytData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    Color getRowBackgroundColor() {
      final int i = effectiveRows.indexOf(row);

      if (i % 2 != 0) {
        return AppColors.littelblueTable;
      }

      return Colors.transparent;
    }

    return DataGridRowAdapter(
        color: getRowBackgroundColor(),
        cells: row.getCells().map<Widget>((DataGridCell cell) {
          Widget getCellWidget() {
            if ((cell as CustomDataGrid).isNumber) {
              return Directionality(
                textDirection: TextDirection.ltr,
                child: Text(cell.value.toString()),
              );
            } else
              return Text(cell.value.toString());
          }

          return Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(8.0),
            child: getCellWidget(),
          );
        }).toList());
  }
}
