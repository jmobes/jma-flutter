import 'package:date_time_picker/date_time_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jma/core/domain/entities/chart_data.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:jma/core/presentation/manager/export/cubit.dart';
import 'package:jma/core/presentation/manager/server_date_cubit.dart';
import 'package:jma/core/presentation/manager/server_date_state.dart';
import 'package:jma/core/presentation/widgets/chart.dart';
import 'package:jma/core/presentation/widgets/data_table.dart';
import 'package:jma/core/presentation/widgets/loading_view_with_text.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/core/utils/key-lang.dart';
import 'package:jma/features/summary/presentation/adapters/summary_adapter.dart';
import 'package:jma/features/summary/presentation/manager/summary_cubit.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class SummaryLargeScreen extends StatefulWidget {
  const SummaryLargeScreen({Key? key}) : super(key: key);

  @override
  State<SummaryLargeScreen> createState() => _SummaryLargeScreenState();
}

class _SummaryLargeScreenState extends State<SummaryLargeScreen> {
  late SummaryAdapter _summaryAdapter;

  final GlobalKey<SfDataGridState> _key = GlobalKey<SfDataGridState>();
  int _chartType = 1;
  String dateFrom = '';
  String dateTo = '';
  String dateToMax = '';
  late TextEditingController _dateFrom;
  late TextEditingController _dateTo;
  final GlobalKey<FormState> _keySummaryForm = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _dateFrom = TextEditingController();
    _dateTo = TextEditingController();
    context.read<ServerDateCubit>().getServerDate();
  }

  @override
  Widget build(BuildContext bContext) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    return Column(
      children: [
        Card(
          child: SizedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 20),
                      child: Text(KeyLang.chart_type.tr()),
                    ),
                    Expanded(
                      child: ListTile(
                        title: Text(KeyLang.duringPeriod.tr()),
                        leading: Radio<int>(
                          value: 1,
                          groupValue: _chartType,
                          onChanged: (int? value) {
                            setState(() {
                              _chartType = value ?? 1;
                              _clear(bContext);
                            });
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: ListTile(
                        title: Text(KeyLang.last12months.tr()),
                        leading: Radio<int>(
                          value: 2,
                          groupValue: _chartType,
                          onChanged: (int? value) {
                            setState(() {
                              _chartType = value ?? 1;
                              _clear(bContext);
                            });
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                if (_chartType == 1) const SizedBox(height: 10),
                if (_chartType == 1)
                  BlocBuilder<ServerDateCubit, ServerDateState>(
                    builder: (context, state) {
                      if (state is ServerDateLoaded) {
                        return Form(
                          key: _keySummaryForm,
                          autovalidateMode: AutovalidateMode.always,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              SizedBox(
                                width: width * .10,
                                height: height * .10,
                                child: DateTimePicker(
                                  dateMask: 'yyyy/MM/dd',
                                  controller: _dateFrom,
                                  onChanged: (String _) => setState(() {}),
                                  icon: const Icon(Icons.event),
                                  dateLabelText: KeyLang.fromDate.tr(),
                                  firstDate: DateTime(2000),
                                  lastDate: DateTime(2025),
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return KeyLang.errordate.tr();
                                    }

                                    return null;
                                  },
                                ),
                              ),
                              SizedBox(
                                width: width * .10,
                                height: height * .10,
                                child: DateTimePicker(
                                  dateMask: 'yyyy/MM/dd',
                                  onChanged: (String _) => setState(() {}),
                                  controller: _dateTo,
                                  firstDate: DateTime(
                                    2000,
                                  ),
                                  lastDate: DateTime(2025),
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return KeyLang.errordate.tr();
                                    }

                                    return null;
                                  },
                                  icon: const Icon(Icons.event),
                                  dateLabelText: KeyLang.toDate.tr(),
                                ),
                              ),
                            ],
                          ),
                        );
                      } else if (state is ServerDateLoading) {
                        return loadingViewWithText(
                          'يتم الحصول على تاريخ اليوم',
                        );
                      } else if (state is ServerDateFailed) {
                        return Text(state.message);
                      } else {
                        return const SizedBox();
                      }
                    },
                  ),
                if (!_isDatesValid())
                  Center(
                    child: Text(
                      KeyLang.errorDateMore30.tr(),
                      style: const TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                const SizedBox(height: 15),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: SizedBox(
                        width: width * .10,
                        height: height * .05,
                        child: ElevatedButton(
                          onPressed: () async {
                            if (_chartType == 1) {
                              if (_keySummaryForm.currentState!.validate()) {
                                if (_isDatesValid()) {
                                  _getSummary(bContext);
                                }
                              }
                            } else {
                              _getSummary(bContext);
                            }
                            setState(() {});
                          },
                          style: ElevatedButton.styleFrom(
                            primary: AppColors.buttonandtitle,
                          ),
                          child: Text(
                            KeyLang.search.tr(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize:
                                  Responsive.isSmallScreen(context) ? 7 : 15,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: SizedBox(
                        width: width * .10,
                        height: height * .05,
                        child: ElevatedButton(
                          onPressed: () async {
                            setState(() {
                              _chartType = 1;
                              _clear(bContext);
                            });
                          },
                          style: ElevatedButton.styleFrom(
                            primary: AppColors.buttonandtitle,
                          ),
                          child: Text(
                            KeyLang.clear.tr(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize:
                                  Responsive.isSmallScreen(context) ? 7 : 15,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          child: BlocConsumer<SummaryCubit, SummaryState>(
            buildWhen: (previous, current) {
              if (current is SummaryLoaded ||
                  current is SummaryFailed ||
                  current is SummaryLoading ||
                  current is SummaryInitial) {
                return true;
              }

              return false;
            },
            listener: (context, state) {
              if (state is SummaryFailed) {
                showSnackBar(context, state.message);
              }
            },
            builder: (context, state) {
              if (state is SummaryLoaded) {
                String? catTable;
                _summaryAdapter = SummaryAdapter(state.summaryTotals);
                final List<ChartData> data = state.summaryTotals.map((e) {
                  String cat;
                  switch (_chartType) {
                    case 2:
                      cat = "${KeyLang.month.tr()} ${e.month}";
                      catTable = KeyLang.month.tr();
                      break;
                    default:
                      cat = e.requestDate!;
                      catTable = KeyLang.requestDate.tr();
                      break;
                  }
                  return ChartData(cat, [
                    e.totalRequests,
                    e.totalAidRequests,
                    e.totalCancelAidRequests
                  ]);
                }).toList();

                return SingleChildScrollView(
                  child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Card(
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: appChart(
                            chartTitle: KeyLang.summary_chart_title.tr(),
                            chartData: data,
                            categories: [
                              KeyLang.requests.tr(),
                              KeyLang.totalCustomerDeserverAidRequest.tr(),
                              KeyLang.totalCustomerCancelDeserverRequestAid
                                  .tr(),
                            ],
                            visibleMax: 4,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Card(
                        child: Column(
                          children: [
                            Container(
                              margin: const EdgeInsets.all(5),
                              child: Align(
                                alignment: Alignment.topRight,
                                child: ElevatedButton.icon(
                                    onPressed: () {
                                      context
                                          .read<ExportCubit>()
                                          .exportDataGridToExcel(
                                            _key,
                                            'ملخص_الطلبات.xlsx',
                                          );
                                    },
                                    icon: const Icon(Icons.send_to_mobile),
                                    label: Text(KeyLang.export.tr())),
                              ),
                            ),
                            Container(
                              constraints: const BoxConstraints(
                                minWidth: 100,
                                minHeight: 100,
                              ),
                              height: MediaQuery.of(context).size.height * .40,
                              child: getDataTable(
                                _key,
                                _summaryAdapter,
                                [
                                  KeyLang.customers.tr(),
                                  KeyLang.requests.tr(),
                                  KeyLang.totalCustomerDeserverAidRequest.tr(),
                                  KeyLang.totalCustomerCancelDeserverRequestAid
                                      .tr(),
                                  catTable!,
                                ],
                              ),
                            ),
                            SfDataPager(
                              delegate: _summaryAdapter,
                              pageCount: (state.summaryTotals.length / 10)
                                  .ceil()
                                  .toDouble(),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                );
              } else if (state is SummaryLoading) {
                return Center(
                  child: loadingViewWithText(
                    KeyLang.loading_total_read_chart.tr(),
                  ),
                );
              } else {
                return Container();
              }
            },
          ),
        ),
      ],
    );
  }

  void _getSummary(BuildContext bContext) {
    bContext
        .read<SummaryCubit>()
        .getSummaryTotals(_dateFrom.text, _dateTo.text, _chartType);
  }

  void _clear(BuildContext bContext) {
    bContext.read<SummaryCubit>().clear();
  }

  bool _isDatesValid() {
    if (_dateFrom.text.isNotEmpty && _dateTo.text.isNotEmpty) {
      final theDatesFrom = _dateFrom.text.split('-');
      final year = int.parse(theDatesFrom[0]);
      final month = int.parse(theDatesFrom[1]);
      final day = int.parse(theDatesFrom[2]);
      final DateTime dateFrom = DateTime(
        year,
        month,
        day,
      );

      final theDatesTo = _dateTo.text.split('-');
      final yearT = int.parse(theDatesTo[0]);
      final monthT = int.parse(theDatesTo[1]);
      final dayT = int.parse(theDatesTo[2]);
      final DateTime dateTo = DateTime(
        yearT,
        monthT,
        dayT,
      );

      final int difference = dateTo.difference(dateFrom).inDays.abs();
      if (difference > 30) {
        return false;
      }
    }

    return true;
  }
}
