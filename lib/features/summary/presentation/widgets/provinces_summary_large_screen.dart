import 'package:date_time_picker/date_time_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jma/core/domain/entities/chart_data.dart';
import 'package:jma/core/domain/entities/info_card_data.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:jma/core/presentation/manager/export/cubit.dart';
import 'package:jma/core/presentation/manager/server_date_cubit.dart';
import 'package:jma/core/presentation/manager/server_date_state.dart';
import 'package:jma/core/presentation/widgets/chart.dart';
import 'package:jma/core/presentation/widgets/data_table.dart';
import 'package:jma/core/presentation/widgets/info_card.dart';
import 'package:jma/core/presentation/widgets/info_card_list.dart';
import 'package:jma/core/presentation/widgets/loading_view_with_text.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/core/utils/key-lang.dart';
import 'package:jma/features/summary/presentation/adapters/summary_adapter.dart';
import 'package:jma/features/summary/presentation/manager/summary_cubit.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class ProvincesSummaryLargeScreen extends StatefulWidget {
  const ProvincesSummaryLargeScreen({Key? key}) : super(key: key);

  @override
  State<ProvincesSummaryLargeScreen> createState() =>
      _ProvincesSummaryLargeScreenState();
}

class _ProvincesSummaryLargeScreenState
    extends State<ProvincesSummaryLargeScreen> {
  @override
  void initState() {
    super.initState();
    context.read<SummaryCubit>().getProvincesSummaryTotals();
  }

  @override
  Widget build(BuildContext bContext) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    return BlocConsumer<SummaryCubit, SummaryState>(
      buildWhen: (previous, current) {
        if (current is ProvincesSummaryLoaded ||
            current is ProvincesSummaryFailed ||
            current is ProvincesSummaryLoading ||
            current is ProvincesSummaryInitial) {
          return true;
        }

        return false;
      },
      listener: (context, state) {
        if (state is ProvincesSummaryFailed) {
          showSnackBar(context, state.message);
        }
      },
      builder: (context, state) {
        if (state is ProvincesSummaryLoaded) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Column(
                  children: [
                    Text(
                      " عمان - ${state.provincesTotalRequests[0]} إشتراك ",
                      style:
                          const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Row(
                      children: const [
                        Expanded(
                          child: Text(
                            'نوع الإشتراك',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'تصنيف الإشتراك',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        InfoCardList(
                          infoCardData: state.totals[0][0],
                          topColor: Colors.lightGreen,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        InfoCardList(
                          infoCardData: state.totals[1][0],
                          topColor: Colors.red,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 16,
                ),
                Column(
                  children: [
                    Text(
                      " الزرقاء - ${state.provincesTotalRequests[1]} إشتراك ",
                      style:
                          const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Row(
                      children: const [
                        Expanded(
                          child: Text(
                            'نوع الإشتراك',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'تصنيف الإشتراك',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        InfoCardList(
                          infoCardData: state.totals[0][1],
                          topColor: Colors.lightGreen,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        InfoCardList(
                          infoCardData: state.totals[1][1],
                          topColor: Colors.red,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 16,
                ),
                Column(
                  children: [
                    Text(
                      " مادبا - ${state.provincesTotalRequests[2]} إشتراك ",
                      style:
                          const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Row(
                      children: const [
                        Expanded(
                          child: Text(
                            'نوع الإشتراك',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'تصنيف الإشتراك',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        InfoCardList(
                          infoCardData: state.totals[0][2],
                          topColor: Colors.lightGreen,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        InfoCardList(
                          infoCardData: state.totals[1][2],
                          topColor: Colors.red,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 16,
                ),
                Column(
                  children: [
                    Text(
                      " البلقاء - ${state.provincesTotalRequests[3]} إشتراك ",
                      style:
                          const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Row(
                      children: const [
                        Expanded(
                          child: Text(
                            'نوع الإشتراك',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'تصنيف الإشتراك',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        InfoCardList(
                          infoCardData: state.totals[0][3],
                          topColor: Colors.lightGreen,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        InfoCardList(
                          infoCardData: state.totals[1][3],
                          topColor: Colors.red,
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          );
        } else if (state is ProvincesSummaryLoading) {
          return Center(
            child: loadingViewWithText(
              KeyLang.loading_total_read_chart.tr(),
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
