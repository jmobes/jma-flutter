part of 'date_cubit.dart';

abstract class DateState extends Equatable {
  const DateState();
}

class DateInitial extends DateState {
  @override
  List<Object> get props => [];
}

class DateCalculating extends DateState {
  @override
  List<Object> get props => [];
}

class DateCalculated extends DateState {
  final DateTime maxDate;

  const DateCalculated(this.maxDate);

  @override
  List<Object> get props => [maxDate];
}
