part of 'summary_cubit.dart';

abstract class SummaryState extends Equatable {
  const SummaryState();
}

class SummaryInitial extends SummaryState {
  @override
  List<Object> get props => [];
}

class SummaryLoading extends SummaryState {
  @override
  List<Object> get props => [];
}

class SummaryDashboardLoading extends SummaryState {
  @override
  List<Object> get props => [];
}

class SummaryLoaded extends SummaryState {
  final List<SummaryTotals> summaryTotals;

  const SummaryLoaded(this.summaryTotals);

  @override
  List<Object> get props => [summaryTotals];
}

class SummaryDashboardLoaded extends SummaryState {
  final List<SummaryTotals> summaryTotals;

  const SummaryDashboardLoaded(this.summaryTotals);

  @override
  List<Object> get props => [summaryTotals];
}

class SummaryFailed extends SummaryState {
  final String message;

  const SummaryFailed(this.message);

  @override
  List<Object> get props => [message];
}

class SummaryDashboardFailed extends SummaryState {
  final String message;

  const SummaryDashboardFailed(this.message);

  @override
  List<Object> get props => [message];
}

class ProvincesSummaryInitial extends SummaryState {
  @override
  List<Object> get props => [];
}

class ProvincesSummaryLoading extends SummaryState {
  @override
  List<Object> get props => [];
}

class ProvincesSummaryLoaded extends SummaryState {
  final List<int> provincesTotalRequests;
  final List<List<List<InfoCardData>>> totals;

  const ProvincesSummaryLoaded(this.provincesTotalRequests, this.totals);

  @override
  List<Object> get props => [totals];
}

class ProvincesSummaryFailed extends SummaryState {
  final String message;

  const ProvincesSummaryFailed(this.message);

  @override
  List<Object> get props => [message];
}
