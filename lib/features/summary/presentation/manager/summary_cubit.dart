import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:jma/core/domain/entities/info_card_data.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/summary/domain/entities/summary_totals.dart';
import 'package:jma/features/summary/domain/use_cases/get_province_summary_totals_use_case.dart';
import 'package:jma/features/summary/domain/use_cases/get_summary_totals_use_case.dart';

part 'summary_state.dart';

class SummaryCubit extends Cubit<SummaryState> {
  final GetSummaryTotalsUseCase getSummaryTotalsUseCase;
  final GetProvinceSummaryTotalsUseCase getProvinceSummaryTotalsUseCase;

  SummaryCubit(
      this.getSummaryTotalsUseCase, this.getProvinceSummaryTotalsUseCase)
      : super(SummaryInitial());

  Future<void> getSummaryTotals(
    String? from,
    String? to,
    int type, {
    bool fromDash = false,
  }) async {
    if (fromDash) {
      emit(SummaryDashboardLoading());
    } else {
      emit(SummaryLoading());
    }

    final result = await getSummaryTotalsUseCase(
      Params(
        from: from ?? "",
        to: to ?? "",
        type: type,
      ),
    );

    result.fold(
      (l) {
        if (fromDash) {
          emit(SummaryDashboardFailed(l.message));
        } else {
          emit(SummaryFailed(l.message));
        }
      },
      (r) {
        if (fromDash) {
          emit(SummaryDashboardLoaded(r));
        } else {
          emit(SummaryLoaded(r));
        }
      },
    );
  }

  Future<void> getProvincesSummaryTotals() async {
    emit(ProvincesSummaryLoading());

    final result = await getProvinceSummaryTotalsUseCase(
      NoParams(),
    );

    result.fold(
      (l) {
        emit(ProvincesSummaryFailed(l.message));
      },
      (r) {
        final List<List<List<InfoCardData>>> totals = [];
        final List<List<InfoCardData>> typesTotals = [];
        final List<List<InfoCardData>> categoriesTotals = [];

        final List<InfoCardData> ammanTypesInfoCardData = [];
        final List<InfoCardData> zarqaTypesInfoCardData = [];
        final List<InfoCardData> madabaTypesInfoCardData = [];
        final List<InfoCardData> balqaTypesInfoCardData = [];

        final List<InfoCardData> ammanCategoriesInfoCardData = [];
        final List<InfoCardData> zarqaCategoriesInfoCardData = [];
        final List<InfoCardData> madabaCategoriesInfoCardData = [];
        final List<InfoCardData> balqaCategoriesInfoCardData = [];

        //colors
        //type
        const jordanianTypeColor = Colors.green;
        const gazaTypeColor = Colors.blue;
        const tempPassportTypeColor = Colors.red;
        //category
        const normalCategoryColor = Colors.deepOrange;
        const takafulCategoryColor = Colors.black;
        const marriedToNonJordanianCategoryColor = Colors.pink;

        //Types
        ammanTypesInfoCardData.add(
          InfoCardData(
            'أردني',
            r.provinceTotals[0].userTypeTotals[0],
            percentage: (r.provinceTotals[0].userTypeTotals[0] /
                    r.ammanTotalAidRequests) *
                100,
            percentageColor: jordanianTypeColor,
          ),
        );
        ammanTypesInfoCardData.add(
          InfoCardData(
            'غزة',
            r.provinceTotals[0].userTypeTotals[1],
            percentage: (r.provinceTotals[0].userTypeTotals[1] /
                    r.ammanTotalAidRequests) *
                100,
            percentageColor: gazaTypeColor,
          ),
        );
        ammanTypesInfoCardData.add(
          InfoCardData(
            'جوازات مؤقتة',
            r.provinceTotals[0].userTypeTotals[2],
            percentage: (r.provinceTotals[0].userTypeTotals[2] /
                    r.ammanTotalAidRequests) *
                100,
            percentageColor: tempPassportTypeColor,
          ),
        );
        typesTotals.add(ammanTypesInfoCardData);

        zarqaTypesInfoCardData.add(InfoCardData(
          'أردني',
          r.provinceTotals[1].userTypeTotals[0],
          percentage: (r.provinceTotals[1].userTypeTotals[0] /
                  r.zarqaTotalAidRequests) *
              100,
          percentageColor: jordanianTypeColor,
        ));
        zarqaTypesInfoCardData.add(InfoCardData(
          'غزة',
          r.provinceTotals[1].userTypeTotals[1],
          percentage: (r.provinceTotals[1].userTypeTotals[1] /
                  r.zarqaTotalAidRequests) *
              100,
          percentageColor: gazaTypeColor,
        ));
        zarqaTypesInfoCardData.add(InfoCardData(
          'جوازات مؤقتة',
          r.provinceTotals[1].userTypeTotals[2],
          percentage: (r.provinceTotals[1].userTypeTotals[2] /
                  r.zarqaTotalAidRequests) *
              100,
          percentageColor: tempPassportTypeColor,
        ));
        typesTotals.add(zarqaTypesInfoCardData);

        madabaTypesInfoCardData.add(InfoCardData(
          'أردني',
          r.provinceTotals[2].userTypeTotals[0],
          percentage: (r.provinceTotals[2].userTypeTotals[0] /
                  r.madabaTotalAidRequests) *
              100,
          percentageColor: jordanianTypeColor,
        ));
        madabaTypesInfoCardData.add(InfoCardData(
          'غزة',
          r.provinceTotals[2].userTypeTotals[1],
          percentage: (r.provinceTotals[2].userTypeTotals[1] /
                  r.madabaTotalAidRequests) *
              100,
          percentageColor: gazaTypeColor,
        ));
        madabaTypesInfoCardData.add(InfoCardData(
          'جوازات مؤقتة',
          r.provinceTotals[2].userTypeTotals[2],
          percentage: (r.provinceTotals[2].userTypeTotals[2] /
                  r.madabaTotalAidRequests) *
              100,
          percentageColor: tempPassportTypeColor,
        ));
        typesTotals.add(madabaTypesInfoCardData);

        balqaTypesInfoCardData.add(InfoCardData(
          'أردني',
          r.provinceTotals[3].userTypeTotals[0],
          percentage: (r.provinceTotals[3].userTypeTotals[0] /
                  r.balqaTotalAidRequests) *
              100,
          percentageColor: jordanianTypeColor,
        ));
        balqaTypesInfoCardData.add(InfoCardData(
          'غزة',
          r.provinceTotals[3].userTypeTotals[1],
          percentage: (r.provinceTotals[3].userTypeTotals[1] /
                  r.balqaTotalAidRequests) *
              100,
          percentageColor: gazaTypeColor,
        ));
        balqaTypesInfoCardData.add(InfoCardData(
          'جوازات مؤقتة',
          r.provinceTotals[3].userTypeTotals[2],
          percentage: (r.provinceTotals[3].userTypeTotals[2] /
                  r.balqaTotalAidRequests) *
              100,
          percentageColor: tempPassportTypeColor,
        ));
        typesTotals.add(balqaTypesInfoCardData);

        totals.add(typesTotals);

        //Categories
        ammanCategoriesInfoCardData.add(
          InfoCardData(
            'عادي',
            r.provinceTotals[0].userCategoryTotals[0],
            percentage: (r.provinceTotals[0].userCategoryTotals[0] /
                    r.ammanTotalAidRequests) *
                100,
            percentageColor: normalCategoryColor,
          ),
        );
        ammanCategoriesInfoCardData.add(
          InfoCardData(
            'تكافل',
            r.provinceTotals[0].userCategoryTotals[1],
            percentage: (r.provinceTotals[0].userCategoryTotals[1] /
                    r.ammanTotalAidRequests) *
                100,
            percentageColor: takafulCategoryColor,
          ),
        );
        /*ammanCategoriesInfoCardData.add(
          InfoCardData(
            'لاجئين سوريين',
            r.provinceTotals[0].userCategoryTotals[2],
            percentage: (r.provinceTotals[0].userCategoryTotals[2] /
                    r.ammanTotalAidRequests) *
                100,
          ),
        );*/
        ammanCategoriesInfoCardData.add(
          InfoCardData(
            'متزوجة من غير أردني',
            r.provinceTotals[0].userCategoryTotals[3],
            percentage: (r.provinceTotals[0].userCategoryTotals[3] /
                    r.ammanTotalAidRequests) *
                100,
            percentageColor: marriedToNonJordanianCategoryColor,
          ),
        );
        categoriesTotals.add(ammanCategoriesInfoCardData);

        zarqaCategoriesInfoCardData.add(
          InfoCardData(
            'عادي',
            r.provinceTotals[1].userCategoryTotals[0],
            percentage: (r.provinceTotals[1].userCategoryTotals[0] /
                    r.zarqaTotalAidRequests) *
                100,
            percentageColor: normalCategoryColor,
          ),
        );
        zarqaCategoriesInfoCardData.add(
          InfoCardData(
            'تكافل',
            r.provinceTotals[1].userCategoryTotals[1],
            percentage: (r.provinceTotals[1].userCategoryTotals[1] /
                    r.zarqaTotalAidRequests) *
                100,
            percentageColor: takafulCategoryColor,
          ),
        );
        /*zarqaCategoriesInfoCardData.add(
          InfoCardData(
            'لاجئين سوريين',
            r.provinceTotals[1].userCategoryTotals[2],
            percentage: (r.provinceTotals[1].userCategoryTotals[2] /
                    r.zarqaTotalAidRequests) *
                100,
          ),
        );*/
        zarqaCategoriesInfoCardData.add(
          InfoCardData(
            'متزوجة من غير أردني',
            r.provinceTotals[1].userCategoryTotals[3],
            percentage: (r.provinceTotals[1].userCategoryTotals[3] /
                    r.zarqaTotalAidRequests) *
                100,
            percentageColor: marriedToNonJordanianCategoryColor,
          ),
        );
        categoriesTotals.add(zarqaCategoriesInfoCardData);

        madabaCategoriesInfoCardData.add(
          InfoCardData(
            'عادي',
            r.provinceTotals[2].userCategoryTotals[0],
            percentage: (r.provinceTotals[2].userCategoryTotals[0] /
                    r.madabaTotalAidRequests) *
                100,
            percentageColor: normalCategoryColor,
          ),
        );
        madabaCategoriesInfoCardData.add(
          InfoCardData(
            'تكافل',
            r.provinceTotals[2].userCategoryTotals[1],
            percentage: (r.provinceTotals[2].userCategoryTotals[1] /
                    r.madabaTotalAidRequests) *
                100,
            percentageColor: takafulCategoryColor,
          ),
        );
        /*madabaCategoriesInfoCardData.add(
          InfoCardData(
            'لاجئين سوريين',
            r.provinceTotals[2].userCategoryTotals[2],
            percentage: (r.provinceTotals[2].userCategoryTotals[2] /
                    r.madabaTotalAidRequests) *
                100,
          ),
        );*/
        madabaCategoriesInfoCardData.add(
          InfoCardData(
            'متزوجة من غير أردني',
            r.provinceTotals[2].userCategoryTotals[3],
            percentage: (r.provinceTotals[2].userCategoryTotals[3] /
                    r.madabaTotalAidRequests) *
                100,
            percentageColor: marriedToNonJordanianCategoryColor,
          ),
        );
        categoriesTotals.add(madabaCategoriesInfoCardData);

        balqaCategoriesInfoCardData.add(
          InfoCardData(
            'عادي',
            r.provinceTotals[3].userCategoryTotals[0],
            percentage: (r.provinceTotals[3].userCategoryTotals[0] /
                    r.balqaTotalAidRequests) *
                100,
            percentageColor: normalCategoryColor,
          ),
        );
        balqaCategoriesInfoCardData.add(
          InfoCardData(
            'تكافل',
            r.provinceTotals[3].userCategoryTotals[1],
            percentage: (r.provinceTotals[3].userCategoryTotals[1] /
                    r.balqaTotalAidRequests) *
                100,
            percentageColor: takafulCategoryColor,
          ),
        );
        /*balqaCategoriesInfoCardData.add(
          InfoCardData(
            'لاجئين سوريين',
            r.provinceTotals[3].userCategoryTotals[2],
            percentage: (r.provinceTotals[3].userCategoryTotals[2] /
                    r.balqaTotalAidRequests) *
                100,
          ),
        );*/
        balqaCategoriesInfoCardData.add(
          InfoCardData(
            'متزوجة من غير أردني',
            r.provinceTotals[3].userCategoryTotals[3],
            percentage: (r.provinceTotals[3].userCategoryTotals[3] /
                    r.balqaTotalAidRequests) *
                100,
            percentageColor: marriedToNonJordanianCategoryColor,
          ),
        );
        categoriesTotals.add(balqaCategoriesInfoCardData);

        totals.add(categoriesTotals);

        emit(
          ProvincesSummaryLoaded(
            [
              r.ammanTotalAidRequests,
              r.zarqaTotalAidRequests,
              r.madabaTotalAidRequests,
              r.balqaTotalAidRequests
            ],
            totals,
          ),
        );
      },
    );
  }

  void clear() {
    emit(SummaryInitial());
  }
}
