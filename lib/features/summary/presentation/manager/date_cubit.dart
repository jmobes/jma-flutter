import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'date_state.dart';

class DateCubit extends Cubit<DateState> {
  DateCubit() : super(DateInitial());

  void calculateDate(String from) {
    emit(DateCalculating());
    final theDates = from.split('-');
    final year = int.parse(theDates[0]);
    final month = int.parse(theDates[1]);
    final day = int.parse(theDates[2]);
    final DateTime maxDate = DateTime(
      year,
      month,
      day + 29,
    );
    emit(DateCalculated(maxDate));
  }
}
