import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:jma/core/presentation/constants/controllers.dart';
import 'package:jma/core/presentation/constants/custom_text.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';
import 'package:jma/features/summary/presentation/widgets/summary_large_screen.dart';
import 'package:jma/features/summary/presentation/widgets/summary_small_screen.dart';

class SummaryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Obx(
        () => Row(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: Responsive.isSmallScreen(context) ? 56 : 6,
              ),
              child: CustomText(
                text: menuController.activeItem.value,
                size: 20,
                weight: FontWeight.bold,
                color: Colors.blue,
              ),
            ),
          ],
        ),
      ),
      Expanded(
          child: SingleChildScrollView(
        child: Column(
          children: [
            if (Responsive.isLargeScreen(context) ||
                Responsive.isMediumScreen(context))
              const SummaryLargeScreen(),
            if (Responsive.isSmallScreen(context)) const SummarySmallScreen()
          ],
        ),
      )),
    ]);
  }
}
