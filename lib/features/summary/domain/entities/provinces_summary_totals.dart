import 'package:jma/features/summary/domain/entities/province_totals.dart';

class ProvincesSummaryTotals {
  final int ammanTotalAidRequests;
  final int zarqaTotalAidRequests;
  final int madabaTotalAidRequests;
  final int balqaTotalAidRequests;
  final List<ProvinceTotals> provinceTotals;

  const ProvincesSummaryTotals(
    this.ammanTotalAidRequests,
    this.zarqaTotalAidRequests,
    this.madabaTotalAidRequests,
    this.balqaTotalAidRequests,
    this.provinceTotals,
  );
}
