import 'package:jma/features/summary/domain/entities/province_totals.dart';

class MadabaTotals extends ProvinceTotals {
  const MadabaTotals(List<int> userTypeTotals, List<int> userCategoryTotals) : super(userTypeTotals, userCategoryTotals);
}
/*"intMadbaUserTypeTotalJordaian": 12905,
            "intMadbaUserTypeforeign": 4,
            "intMadbaTypeTotalGaza": 59,
            "intMadbaUserCatgoryNormal": 11369,
            "intMadbaUserCatgoryTakaful": 1472,
            "intMadbaUserCatgoryMarriedtoNonJordanian": 127,
            "intMadbaUserCatgorySyrianRefugee": 0*/
