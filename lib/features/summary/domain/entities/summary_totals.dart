class SummaryTotals {
  final int totalCustomers;
  final int totalRequests;
  final int totalAidRequests;
  final int totalCancelAidRequests;
  final String? requestDate;
  final int month;

  const SummaryTotals({
    required this.totalCustomers,
    required this.totalRequests,
    required this.totalAidRequests,
    required this.totalCancelAidRequests,
    this.requestDate,
    required this.month,
  });
}
/*{
            "intTotalOfCustomers": 8504,
            "intTotalOfRequests": 8514,
            "intTotalCustomerDeserverAidRequestCount": 8514,
            "intTotalCustomerCancelDeserverRequestAidCount": 0,
            "requestDate": "20/01/2022",
            "month": 0
        }*/
