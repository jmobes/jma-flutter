class ProvinceTotals{
  final List<int> userTypeTotals;
  final List<int> userCategoryTotals;

  const ProvinceTotals(this.userTypeTotals, this.userCategoryTotals);
}