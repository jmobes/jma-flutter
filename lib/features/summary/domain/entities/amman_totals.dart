import 'package:jma/features/summary/domain/entities/province_totals.dart';

class AmmanTotals extends ProvinceTotals {
  const AmmanTotals(List<int> userTypeTotals, List<int> userCategoryTotals) : super(userTypeTotals, userCategoryTotals);
}
/*"intAmmanUserTypeTotalJordaian": 288263,
            "intAmmanUserTypeforeign": 475,
            "intAmmanUserTypeTotalGaza": 3351,
            "intAmmanUserCatgoryNormal": 251391,
            "intAmmanUserCatgoryTakaful": 34614,
            "intAmmanUserCatgoryMarriedtoNonJordanian": 6084,
            "intAmmanUserCatgorySyrianRefugee": 0,*/
