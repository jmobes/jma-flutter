import 'package:jma/features/summary/domain/entities/province_totals.dart';

class BalqaTotals extends ProvinceTotals {
  const BalqaTotals(List<int> userTypeTotals, List<int> userCategoryTotals) : super(userTypeTotals, userCategoryTotals);
}
/*"intBalqaUserTypeTotalJordaian": 34651,
            "intBalqaUserTypeforeign": 19,
            "intBalqaTypeTotalGaza": 261,
            "intBalqaUserCatgoryNormal": 30737,
            "intBalqaUserCatgoryTakaful": 3839,
            "intBalqaUserCatgoryMarriedtoNonJordanian": 355,
            "intBalqaUserCatgorySyrianRefugee": 0,*/
