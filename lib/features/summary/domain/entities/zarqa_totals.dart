import 'package:jma/features/summary/domain/entities/province_totals.dart';

class ZarqaTotals extends ProvinceTotals {
  const ZarqaTotals(List<int> userTypeTotals, List<int> userCategoryTotals)
      : super(userTypeTotals, userCategoryTotals);
}
/*"intZarqaUserTypeTotalJordaian": 93950,
            "intZarqaUserTypeforeign": 112,
            "intZarqaTypeTotalGaza": 2055,
            "intZarqaUserCatgoryNormal": 76635,
            "intZarqaUserCatgoryTakaful": 17967,
            "intZarqaUserCatgoryMarriedtoNonJordanian": 1515,
            "intZarqaUserCatgorySyrianRefugee": 0,*/
