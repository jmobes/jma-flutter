import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/features/summary/domain/entities/provinces_summary_totals.dart';
import 'package:jma/features/summary/domain/entities/summary_totals.dart';

abstract class SummaryRepository {
  Future<Either<Failure, List<SummaryTotals>>> getSummaryTotals(
    String from,
    String to,
    int type,
  );

  Future<Either<Failure, ProvincesSummaryTotals>> getProvinceSummaryTotals();
}
