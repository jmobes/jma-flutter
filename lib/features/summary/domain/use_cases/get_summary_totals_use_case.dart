import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/summary/domain/entities/summary_totals.dart';
import 'package:jma/features/summary/domain/repositories/summary_repository.dart';
import 'package:jma/features/summary/domain/repositories/summary_repository.dart';

class GetSummaryTotalsUseCase implements UseCase<List<SummaryTotals>, Params> {
  final SummaryRepository repository;

  const GetSummaryTotalsUseCase({required this.repository});

  @override
  Future<Either<Failure, List<SummaryTotals>>> call(Params params) {
    return repository.getSummaryTotals(params.from, params.to, params.type);
  }
}

class Params extends Equatable {
  final String from;
  final String to;
  final int type;

  const Params({
    required this.from,
    required this.to,
    required this.type,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [from, to, type];
}
