import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/summary/domain/entities/provinces_summary_totals.dart';
import 'package:jma/features/summary/domain/repositories/summary_repository.dart';
import 'package:jma/features/summary/domain/repositories/summary_repository.dart';

class GetProvinceSummaryTotalsUseCase
    implements UseCase<ProvincesSummaryTotals, NoParams> {
  final SummaryRepository repository;

  const GetProvinceSummaryTotalsUseCase({required this.repository});

  @override
  Future<Either<Failure, ProvincesSummaryTotals>> call(NoParams noParams) {
    return repository.getProvinceSummaryTotals();
  }
}
