import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/summary/data/remote/data_sources/summary_remote_data_source.dart';
import 'package:jma/features/summary/domain/entities/provinces_summary_totals.dart';
import 'package:jma/features/summary/domain/entities/summary_totals.dart';
import 'package:jma/features/summary/domain/repositories/summary_repository.dart';

class SummaryRepositoryImpl implements SummaryRepository {
  final SummaryRemoteDataSource summaryRemoteDataSource;

  const SummaryRepositoryImpl({
    required this.summaryRemoteDataSource,
  });

  @override
  Future<Either<Failure, List<SummaryTotals>>> getSummaryTotals(
    String from,
    String to,
    int type,
  ) {
    return getRepositoryResponse<List<SummaryTotals>>(
      summaryRemoteDataSource.getSummaryTotals(from, to, type),
    );
  }

  @override
  Future<Either<Failure, ProvincesSummaryTotals>> getProvinceSummaryTotals() {
    return getRepositoryResponse<ProvincesSummaryTotals>(
      summaryRemoteDataSource.getProvincesSummaryTotals(),
    );
  }
}
