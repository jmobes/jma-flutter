import 'package:jma/features/summary/domain/entities/summary_totals.dart';

class SummaryTotalsModel extends SummaryTotals {
  const SummaryTotalsModel({
    required int totalCustomers,
    required int totalRequests,
    required int totalAidRequests,
    required int totalCancelAidRequests,
    String? requestDate,
    required int month,
  }) : super(
          totalCustomers: totalCustomers,
          totalRequests: totalRequests,
          totalAidRequests: totalAidRequests,
          totalCancelAidRequests: totalCancelAidRequests,
          requestDate: requestDate,
          month: month,
        );

  factory SummaryTotalsModel.fromJson(Map<String, dynamic> json) {
    String? requestDate;

    if (json.containsKey("requestDate")) {
      requestDate = json['requestDate'] as String;
    }

    return SummaryTotalsModel(
      totalCustomers: json['intTotalOfCustomers'] as int,
      totalRequests: json['intTotalOfRequests'] as int,
      totalAidRequests: json['intTotalCustomerDeserverAidRequestCount'] as int,
      totalCancelAidRequests:
          json['intTotalCustomerCancelDeserverRequestAidCount'] as int,
      requestDate: requestDate,
      month: json['month'] as int,
    );
  }
}
/*{
            "intTotalOfCustomers": 8504,
            "intTotalOfRequests": 8514,
            "intTotalCustomerDeserverAidRequestCount": 8514,
            "intTotalCustomerCancelDeserverRequestAidCount": 0,
            "requestDate": "20/01/2022",
            "month": 0
        }*/
