class ProvinceSummaryTotalsModel {
  final int intAmmanTotalRequestsAid;
  final int intZarqaTotalRequestsAid;
  final int intMadbaTotalRequestsAid;
  final int intBalqaTotalRequestsAid;
  final int intAmmanUserTypeTotalJordaian;
  final int intAmmanUserTypeforeign;
  final int intAmmanUserTypeTotalGaza;
  final int intAmmanUserCatgoryNormal;
  final int intAmmanUserCatgoryTakaful;
  final int intAmmanUserCatgoryMarriedtoNonJordanian;
  final int intAmmanUserCatgorySyrianRefugee;
  final int intZarqaUserTypeTotalJordaian;
  final int intZarqaUserTypeforeign;
  final int intZarqaTypeTotalGaza;
  final int intZarqaUserCatgoryNormal;
  final int intZarqaUserCatgoryTakaful;
  final int intZarqaUserCatgoryMarriedtoNonJordanian;
  final int intZarqaUserCatgorySyrianRefugee;
  final int intBalqaUserTypeTotalJordaian;
  final int intBalqaUserTypeforeign;
  final int intBalqaTypeTotalGaza;
  final int intBalqaUserCatgoryNormal;
  final int intBalqaUserCatgoryTakaful;
  final int intBalqaUserCatgoryMarriedtoNonJordanian;
  final int intBalqaUserCatgorySyrianRefugee;
  final int intMadbaUserTypeTotalJordaian;
  final int intMadbaUserTypeforeign;
  final int intMadbaTypeTotalGaza;
  final int intMadbaUserCatgoryNormal;
  final int intMadbaUserCatgoryTakaful;
  final int intMadbaUserCatgoryMarriedtoNonJordanian;
  final int intMadbaUserCatgorySyrianRefugee;

  ProvinceSummaryTotalsModel(
    this.intAmmanTotalRequestsAid,
    this.intZarqaTotalRequestsAid,
    this.intMadbaTotalRequestsAid,
    this.intBalqaTotalRequestsAid,
    this.intAmmanUserTypeTotalJordaian,
    this.intAmmanUserTypeforeign,
    this.intAmmanUserTypeTotalGaza,
    this.intAmmanUserCatgoryNormal,
    this.intAmmanUserCatgoryTakaful,
    this.intAmmanUserCatgoryMarriedtoNonJordanian,
    this.intAmmanUserCatgorySyrianRefugee,
    this.intZarqaUserTypeTotalJordaian,
    this.intZarqaUserTypeforeign,
    this.intZarqaTypeTotalGaza,
    this.intZarqaUserCatgoryNormal,
    this.intZarqaUserCatgoryTakaful,
    this.intZarqaUserCatgoryMarriedtoNonJordanian,
    this.intZarqaUserCatgorySyrianRefugee,
    this.intBalqaUserTypeTotalJordaian,
    this.intBalqaUserTypeforeign,
    this.intBalqaTypeTotalGaza,
    this.intBalqaUserCatgoryNormal,
    this.intBalqaUserCatgoryTakaful,
    this.intBalqaUserCatgoryMarriedtoNonJordanian,
    this.intBalqaUserCatgorySyrianRefugee,
    this.intMadbaUserTypeTotalJordaian,
    this.intMadbaUserTypeforeign,
    this.intMadbaTypeTotalGaza,
    this.intMadbaUserCatgoryNormal,
    this.intMadbaUserCatgoryTakaful,
    this.intMadbaUserCatgoryMarriedtoNonJordanian,
    this.intMadbaUserCatgorySyrianRefugee,
  );

  factory ProvinceSummaryTotalsModel.fromJson(Map<String, dynamic> json) {
    return ProvinceSummaryTotalsModel(
      json['intAmmanTotalRequestAid'] as int,
      json['intZarqaTotalRequestAid'] as int,
      json['intMadbaTotalRequestsAid'] as int,
      json['intBalqaTotalRequestAid'] as int,
      json['intAmmanUserTypeTotalJordaian'] as int,
      json['intAmmanUserTypeforeign'] as int,
      json['intAmmanUserTypeTotalGaza'] as int,
      json['intAmmanUserCatgoryNormal'] as int,
      json['intAmmanUserCatgoryTakaful'] as int,
      json['intAmmanUserCatgoryMarriedtoNonJordanian'] as int,
      json['intAmmanUserCatgorySyrianRefugee'] as int,
      json['intZarqaUserTypeTotalJordaian'] as int,
      json['intZarqaUserTypeforeign'] as int,
      json['intZarqaTypeTotalGaza'] as int,
      json['intZarqaUserCatgoryNormal'] as int,
      json['intZarqaUserCatgoryTakaful'] as int,
      json['intZarqaUserCatgoryMarriedtoNonJordanian'] as int,
      json['intZarqaUserCatgorySyrianRefugee'] as int,
      json['intBalqaUserTypeTotalJordaian'] as int,
      json['intBalqaUserTypeforeign'] as int,
      json['intBalqaTypeTotalGaza'] as int,
      json['intBalqaUserCatgoryNormal'] as int,
      json['intBalqaUserCatgoryTakaful'] as int,
      json['intBalqaUserCatgoryMarriedtoNonJordanian'] as int,
      json['intBalqaUserCatgorySyrianRefugee'] as int,
      json['intMadbaUserTypeTotalJordaian'] as int,
      json['intMadbaUserTypeforeign'] as int,
      json['intMadbaTypeTotalGaza'] as int,
      json['intMadbaUserCatgoryNormal'] as int,
      json['intMadbaUserCatgoryTakaful'] as int,
      json['intMadbaUserCatgoryMarriedtoNonJordanian'] as int,
      json['intMadbaUserCatgorySyrianRefugee'] as int,
    );
  }
}
