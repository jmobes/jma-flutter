import 'package:dio/dio.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/utils/constants.dart';
import 'package:retrofit/http.dart';

part 'summary_api_interface.g.dart';

@RestApi(baseUrl: kBaseUrl)
abstract class SummaryAPIInterface {
  factory SummaryAPIInterface(Dio dio) {
    return _SummaryAPIInterface(dio);
  }

  @POST(kGetSummaryTotalsEndpoint)
  Future<APIResponse> getSummaryTotals(
    @Body() Map<String, dynamic> map,
  );

  @POST(kGetProvincesSummaryTotalsEndpoint)
  Future<APIResponse> getProvincesSummaryTotals(
    @Body() Map<String, dynamic> map,
  );
}
