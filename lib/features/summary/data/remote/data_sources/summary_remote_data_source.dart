import 'package:equatable/equatable.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/data/remote/resource/app_api_res.dart';
import 'package:jma/core/data/remote/resource/lang_params.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/summary/data/remote/data_sources/summary_api_interface.dart';

abstract class SummaryRemoteDataSource {
  Future<APPApiResponse> getSummaryTotals(
    String from,
    String to,
    int type,
  );

  Future<APPApiResponse> getProvincesSummaryTotals();
}

class SummaryRemoteDataSourceImpl implements SummaryRemoteDataSource {
  final SummaryAPIInterface apiInterface;

  const SummaryRemoteDataSourceImpl({required this.apiInterface});

  @override
  Future<APPApiResponse> getSummaryTotals(String from, String to, int type) {
    return getAppApiResponse<APIResponse>(
      apiInterface.getSummaryTotals(
        SParams(from, to, type, 'AR').toJson(),
      ),
    );
  }

  @override
  Future<APPApiResponse> getProvincesSummaryTotals() {
    return getAppApiResponse<APIResponse>(
      apiInterface.getProvincesSummaryTotals(
        const LangParamsReq('AR').toJson(),
      ),
    );
  }
}

class SParams extends Equatable {
  final String from;
  final String to;
  final int type;
  final String languageId;

  const SParams(
    this.from,
    this.to,
    this.type,
    this.languageId,
  );

  Map<String, dynamic> toJson() {
    return {
      "SummaryView": type,
      "AidCancelRequestDateFrom": from,
      "AidCancelRequestDateTo": to,
      "LanguageId": languageId,
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [from, to, type];
}
