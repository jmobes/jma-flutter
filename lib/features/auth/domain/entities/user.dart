import 'package:equatable/equatable.dart';

class User extends Equatable {
  final int id;
  final String username;
  final String password;
  final String fullName;

  const User({
    required this.id,
    required this.username,
    required this.password,
    required this.fullName,
  });

  @override
  List<Object?> get props => [id, username, password, fullName];
}
