import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/auth/domain/entities/user.dart';
import 'package:jma/features/auth/domain/repositories/auth_repository.dart';

class LoginUseCase implements UseCase<User, Params> {
  final AuthRepository repository;

  LoginUseCase({required this.repository});

  @override
  Future<Either<Failure, User>> call(Params params) {
    return repository.login(params.username, params.password);
  }
}

class Params extends Equatable {
  final String username;
  final String password;

  const Params({required this.username, required this.password});

  @override
  List<Object?> get props => [username, password];
}
