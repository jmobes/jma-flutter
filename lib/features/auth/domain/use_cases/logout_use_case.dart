import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/auth/domain/repositories/auth_repository.dart';

class LogoutUseCase implements UseCase<bool, Params> {
  final AuthRepository repository;

  LogoutUseCase({required this.repository});

  @override
  Future<Either<Failure, bool>> call(Params params) {
    return repository.logout(params.userId);
  }
}

class Params extends Equatable {
  final int userId;

  const Params({required this.userId});

  @override
  // TODO: implement props
  List<Object?> get props => [userId];
}
