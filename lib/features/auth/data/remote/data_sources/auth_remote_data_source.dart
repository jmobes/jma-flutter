import 'package:equatable/equatable.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/data/remote/resource/app_api_res.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/auth/data/remote/data_sources/auth_api_interface.dart';
import 'package:json_annotation/json_annotation.dart';

abstract class AuthRemoteDataSource {
  Future<APPApiResponse> login(String username, String password);

  Future<APPApiResponse> logout(int userId);
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  final AuthAPIInterface apiInterface;

  AuthRemoteDataSourceImpl({required this.apiInterface});

  @override
  Future<APPApiResponse> login(
    String username,
    String password,
  ) async {
    return getAppApiResponse<APIResponse>(
      apiInterface
          .login(LoginParams(username, password, 'AR').toJson(),
      ),
    );
  }

  @override
  Future<APPApiResponse> logout(int userId) {
    throw UnimplementedError();
  }
}

@JsonSerializable()
class LoginParams extends Equatable {
  final String username;
  final String password;
  final String languageId;

  const LoginParams(this.username, this.password, this.languageId);

  Map<String, dynamic> toJson() {
    return {
      "username": username,
      "password": password,
      "LanguageId": languageId,
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [username, password, languageId];
}
