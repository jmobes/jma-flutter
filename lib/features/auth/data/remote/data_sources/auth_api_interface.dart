import 'package:dio/dio.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/utils/constants.dart';
import 'package:retrofit/http.dart';

part 'auth_api_interface.g.dart';

@RestApi(baseUrl: kBaseUrl)
abstract class AuthAPIInterface {
  factory AuthAPIInterface(Dio dio) {
    return _AuthAPIInterface(dio);
  }

  @POST(kLoginEndpoint)
  Future<APIResponse> login(@Body() Map<String, dynamic> map);
}
