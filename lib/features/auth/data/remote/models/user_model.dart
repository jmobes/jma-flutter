import 'package:jma/features/auth/domain/entities/user.dart';

/// userID : 2
/// user_Name : "hamza"
/// password : "123"
/// fullName : "hamza al sarabi"
/// email : "hsarabi@jepco.com"
/// active : true
/// createdDate : "2021-01-10T00:00:00"
/// updateDate : "2021-01-10T00:00:00"

class UserModel extends User {
  const UserModel({
    required int userID,
    required String userName,
    required String password,
    required String fullName,
  }) : super(
          id: userID,
          username: userName,
          password: password,
          fullName: fullName,
        );

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      userID: json['userID'] as int,
      userName: json['user_Name'] as String,
      password: json['password'] as String,
      fullName: json['fullName'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['userID'] = id;
    map['user_Name'] = username;
    map['password'] = password;
    map['fullName'] = fullName;
    return map;
  }
}
