import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/auth/data/remote/data_sources/auth_remote_data_source.dart';
import 'package:jma/features/auth/data/remote/models/user_model.dart';
import 'package:jma/features/auth/domain/entities/user.dart';
import 'package:jma/features/auth/domain/repositories/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource authRemoteDataSource;

  AuthRepositoryImpl({
    required this.authRemoteDataSource,
  });

  @override
  Future<Either<Failure, User>> login(String username, String password) async {
    return getRepositoryResponse<UserModel>(
      authRemoteDataSource.login(username, password),
      // networkInfo,
    );
  }

  @override
  Future<Either<Failure, bool>> logout(int userId) {
    throw UnimplementedError();
  }
}
