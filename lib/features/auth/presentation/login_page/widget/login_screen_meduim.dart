import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:jma/core/presentation/constants/validtor.dart';
import 'package:jma/core/presentation/routing/routes.dart';
import 'package:jma/core/utils/constants.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/core/utils/key-lang.dart';
import 'package:jma/features/auth/presentation/manager/auth_cubit.dart';

class Loginscreenmeduim extends StatefulWidget {
  const Loginscreenmeduim({Key? key}) : super(key: key);

  @override
  _LoginscreenmeduimState createState() => _LoginscreenmeduimState();
}

class _LoginscreenmeduimState extends State<Loginscreenmeduim>
    with SingleTickerProviderStateMixin {
  GlobalKey<FormState> _keylogin = GlobalKey<FormState>();
  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  late String username, password;
  final _focusEmail = FocusNode();
  final _focusPassword = FocusNode();
  final ScrollController scrollLogin = ScrollController();
  bool _passwordVisible = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget getLoadingView() {
    return SizedBox(
      height: double.infinity,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget getLoginView() {
    var screenSize = MediaQuery.of(context).size;
    return PreferredSize(
      preferredSize: Size(screenSize.width, screenSize.height),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Form(
            key: _keylogin,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Center(
              // CARD LOGIN
              child: Container(
                width: screenSize.width * .30,
                height: screenSize.height * .50,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 10,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    controller: scrollLogin,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        // title card
                        Text(KeyLang.login.tr(),
                            style: TextStyle(
                                color: AppColors.buttonandtitle,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)),
                        SizedBox(
                          height: 5,
                        ),

                        Text(
                          KeyLang.titleloginweb.tr(),
                          style: TextStyle(
                            color: AppColors.subtitle,
                            fontSize: 15,
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * .12,
                          child: Padding(
                            padding: EdgeInsets.all(8),
                            child: TextFormField(
                              controller: _usernameController,
                              focusNode: _focusEmail,
                              validator: (value) => Validator.validateName(
                                name: value,
                              ),
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person),
                                labelText: KeyLang.username.tr(),
                                helperText: ' ',
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 5.0),
                                border: OutlineInputBorder(),
                                errorBorder: UnderlineInputBorder(
                                  borderRadius: BorderRadius.circular(6.0),
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                              onFieldSubmitted: (value) {
                                _focusEmail.unfocus();
                                _focusPassword.unfocus();
                                if (_keylogin.currentState!.validate()) {
                                  context.read<AuthCubit>().login(
                                      _usernameController.text,
                                      _passwordController.text);
                                }
                              },
                              onSaved: (val) {
                                username = val!;
                              },
                            ),
                          ),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * .12,
                          child: Padding(
                            padding: EdgeInsets.all(8),
                            child: TextFormField(
                              controller: _passwordController,
                              focusNode: _focusPassword,
                              obscureText: !_passwordVisible,
                              validator: (value) => Validator.validatePassword(
                                password: value,
                              ),
                              decoration: InputDecoration(
                                prefixIcon: Icon(Icons.lock),
                                labelText: KeyLang.password.tr(),
                                helperText: ' ',
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 5.0),
                                border: OutlineInputBorder(),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    // Based on passwordVisible state choose the icon
                                    _passwordVisible
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: Theme.of(context).primaryColorDark,
                                  ),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _passwordVisible = !_passwordVisible;
                                    });
                                  },
                                ),
                                errorBorder: UnderlineInputBorder(
                                  borderRadius: BorderRadius.circular(6.0),
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                              onFieldSubmitted: (value) {
                                _focusEmail.unfocus();
                                _focusPassword.unfocus();
                                if (_keylogin.currentState!.validate()) {
                                  context.read<AuthCubit>().login(
                                      _usernameController.text,
                                      _passwordController.text);
                                }
                              },
                              onSaved: (val) {
                                password = val!;
                              },
                            ),
                          ),
                        ),
                        Container(
                          width: screenSize.width * .15,
                          height: screenSize.height * .04,
                          child: ElevatedButton(
                            onPressed: () async {
                              _focusEmail.unfocus();
                              _focusPassword.unfocus();
                              if (_keylogin.currentState!.validate()) {
                                context.read<AuthCubit>().login(
                                    _usernameController.text,
                                    _passwordController.text);
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              primary: AppColors.buttonandtitle,
                            ),
                            child: Text(
                              KeyLang.login.tr(),
                              style:
                                  TextStyle(color: Colors.white, fontSize: 13),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          // LOGO
          Flexible(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    KeyLang.titileweb.tr(),
                    style: TextStyle(
                        color: AppColors.buttonandtitle,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Container(
                        width: screenSize.width * .30,
                        height: screenSize.height * .40,
                        child: SvgPicture.asset(kLogo)),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(kBackground), fit: BoxFit.cover)),
          child: BlocConsumer<AuthCubit, AuthState>(
            listener: (context, state) async {
              if (state is Error) {
                showSnackBar(context, state.message);
              } else if (state is Authenticated) {
                Navigator.popAndPushNamed(context, rootRoute);
              }
            },
            builder: (context, state) {
              if (state is AuthInitial) {
                return getLoginView();
              } else if (state is Authenticating) {
                return getLoadingView();
              } else
                return getLoginView();
            },
          ),
        ),
      ),
    );
  }
}
