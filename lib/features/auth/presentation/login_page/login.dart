import 'package:flutter/material.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';
import 'package:jma/features/auth/presentation/login_page/widget/login_screen_large.dart';
import 'package:jma/features/auth/presentation/login_page/widget/login_screen_small.dart';

// ignore: always_use_package_imports
import 'widget/login_screen_meduim.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Responsive(
      largeScreen: LoginScreenLarge(),
      smallScreen: LoginScreenSmall(),
      mediumScreen: Loginscreenmeduim(),
      customScreen: Loginscreenmeduim(),
    ));
  }
}
