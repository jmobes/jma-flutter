part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class AuthInitial extends AuthState {
  @override
  List<Object> get props => [];
}

class Authenticating extends AuthState {
  @override
  List<Object> get props => [];
}

class Authenticated extends AuthState {
  final User user;

  const Authenticated({required this.user});

  @override
  List<Object> get props => [user];
}

class UnAuthenticated extends AuthState {
  @override
  List<Object> get props => [];
}

class Error extends AuthState {
  final String message;

  const Error({required this.message});

  @override
  List<Object> get props => [message];
}
