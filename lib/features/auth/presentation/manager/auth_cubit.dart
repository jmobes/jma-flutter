import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/features/auth/domain/entities/user.dart';
import 'package:jma/features/auth/domain/use_cases/login_use_case.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final LoginUseCase loginUseCase;

  AuthCubit({required this.loginUseCase}) : super(AuthInitial());

  Future<void> login(String username, String password) async {
    emit(Authenticating());
    final result =
        await loginUseCase(Params(username: username, password: password));

    result.fold(
      (l) => emit(Error(message: l.message)),
      (r) => emit(Authenticated(user: r)),
    );
  }
}
