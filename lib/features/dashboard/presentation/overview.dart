import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jma/core/presentation/constants/controllers.dart';
import 'package:jma/core/presentation/constants/custom_text.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';
import 'package:jma/features/dashboard/presentation/pages/overview_cards_large.dart';
import 'package:jma/features/dashboard/presentation/pages/overview_cards_medium.dart';
import 'package:jma/features/dashboard/presentation/pages/overview_cards_small.dart';

class OverviewPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Obx(
          () => Row(
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: Responsive.isSmallScreen(context) ? 56 : 6,
                ),
                child: CustomText(
                  text: menuController.activeItem.value,
                  size: 20,
                  weight: FontWeight.bold,
                  color: Colors.blue,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                if (Responsive.isLargeScreen(context) ||
                    Responsive.isMediumScreen(context))
                  if (Responsive.isCustomSize(context))
                    OverviewCardsMediumScreen()
                  else
                    OverviewCardsLargeScreen()
                else
                  OverviewCardsSmallScreen(),
              ],
            ),
          ),
        )
      ],
    );
  }
}
