// ignore_for_file: curly_braces_in_flow_control_structures

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jma/core/presentation/constants/style.dart';
import 'package:jma/core/presentation/widgets/info_card_list.dart';
import 'package:jma/features/dashboard/presentation/manager/dashboard_cubit.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class OverviewCardsMediumScreen extends StatefulWidget {
  @override
  State<OverviewCardsMediumScreen> createState() =>
      _OverviewCardsMediumScreenState();
}

class DataPIE {
  DataPIE(this.continent, this.gdp);
  final String continent;
  final double gdp;
}

class _OverviewCardsMediumScreenState extends State<OverviewCardsMediumScreen> {
  late TooltipBehavior _tooltipBehavior;
  @override
  void initState() {
    super.initState();
    _tooltipBehavior = TooltipBehavior(enable: true);
    context.read<DashboardCubit>().getTotals();
    // context.read<SummaryCubit>().getSummaryTotals('', '', 2, fromDash: true);
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget getLoadingView() {
    return const SizedBox(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width;
    final double _height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        BlocBuilder<DashboardCubit, DashboardState>(
          builder: (context, state) {
            if (state is DashboardLoaded) {
              final List<DataPIE> chartData = [
                DataPIE('عمان', state.percentageAidRequest),
                DataPIE('الزرقاء', state.percentageZarqa),
                DataPIE('مادبا', state.percentageMadaba),
                DataPIE('البلقاء', state.percentageBalqa),
              ];

              return Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(left: 5, right: 5),
                      // width: _width,
                      height: 400,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.white60,
                        boxShadow: [
                          BoxShadow(
                            offset: const Offset(0, 6),
                            color: lightGrey.withOpacity(.1),
                            blurRadius: 12,
                          )
                        ],
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                  color: Colors.green,
                                  height: 5,
                                ),
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  SizedBox(
                                    // height: 200,
                                    // width: 200,
                                    child: CircularPercentIndicator(
                                      header: const Text(
                                        "نسبة عدد الطلبات بالنسبة لاشتركات الفعالة",
                                        style: TextStyle(fontSize: 15.0),
                                      ),
                                      radius: 80.0,
                                      lineWidth: 13.0,
                                      animation: true,
                                      percent: state.percentageAidRequest / 100,
                                      center: Text(
                                        "${state.percentageAidRequest}%",
                                        style: const TextStyle(fontSize: 15.0),
                                      ),
                                      circularStrokeCap:
                                          CircularStrokeCap.round,
                                      progressColor: Colors.green,
                                      //  state.percentageAidRequest < 50
                                      //     ? Colors.
                                      //     : Colors.green,
                                    ),
                                  ),
                                  SizedBox(
                                    // height: 200,
                                    // width: 200,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: CircularPercentIndicator(
                                        header: const Text(
                                          "نسبة عداد الخدمات بالنسبة الاشتركات الفعالة",
                                          style: TextStyle(fontSize: 15.0),
                                        ),
                                        radius: 80.0,
                                        lineWidth: 13.0,
                                        animation: true,
                                        percent: state.percentageService / 100,
                                        center: Text(
                                          "${state.percentageService}%",
                                          style:
                                              const TextStyle(fontSize: 20.0),
                                        ),
                                        circularStrokeCap:
                                            CircularStrokeCap.round,
                                        progressColor: Colors.amber,
                                        // state.percentageService < 50
                                        //     ? Colors.amber
                                        //     : Colors.green,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                // width: 400,
                                child: SfCircularChart(
                                  title: ChartTitle(
                                      text: 'نسبة طلبات الدعم في المحافظات'),
                                  legend: Legend(
                                      isVisible: true,
                                      overflowMode:
                                          LegendItemOverflowMode.wrap),
                                  tooltipBehavior: _tooltipBehavior,
                                  series: <CircularSeries>[
                                    PieSeries<DataPIE, String>(
                                        dataSource: chartData,
                                        xValueMapper: (DataPIE data, _) =>
                                            data.continent,
                                        yValueMapper: (DataPIE data, _) =>
                                            data.gdp,
                                        dataLabelSettings:
                                            const DataLabelSettings(
                                                isVisible: true),
                                        enableTooltip: true,
                                        explode: true,
                                        explodeIndex: 0)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: const [
                        Expanded(
                          child: Text(
                            'الطلبات',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'العدادات',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        InfoCardList(
                          infoCardData: state.totals[0][0],
                          topColor: Colors.lightGreen,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        InfoCardList(
                          infoCardData: state.totals[1][1],
                          topColor: Colors.red,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: const [
                        Expanded(
                          child: Text(
                            'نوع الإشتراك',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'تصنيف الإشتراك',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        InfoCardList(
                          infoCardData: state.totals[2][2],
                          topColor: Colors.lightGreen,
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        InfoCardList(
                          infoCardData: state.totals[3][3],
                          topColor: Colors.red,
                        ),
                      ],
                    ),
                  ]);
            } else if (state is DashboardFailed) {
              return Center(child: Text(state.message));
            } else {
              return getLoadingView();
            }
          },
        ),
      ],
    );
  }
}
