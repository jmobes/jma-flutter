import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:get/utils.dart';
import 'package:jma/core/domain/entities/info_card_data.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/dashboard/domain/entities/dbtotal.dart';
import 'package:jma/features/dashboard/domain/use_cases/get_totals_use_case.dart';

part 'dashboard_state.dart';

class DashboardCubit extends Cubit<DashboardState> {
  final GetTotalsUseCase getTotalsUseCase;

  DashboardCubit({required this.getTotalsUseCase}) : super(DashboardInitial());

  Future<void> getTotals() async {
    emit(DashboardLoading());

    final result = await getTotalsUseCase(NoParams());

    result.fold((l) => emit(DashboardFailed(l.message)), (r) {
      final int activeTotal = r[0].restdnitalCustomers - r[0].inactiveCustomers;
      final double percentageAmman =
          (r[0].ammanTotalRequestAid / r[0].totalAidRequests) * 100;
      final double percentageZarqa =
          (r[0].zarqaTotalRequestAid / r[0].totalAidRequests) * 100;

      final double percentageMadaba =
          (r[0].madbaTotalRequestsAid / r[0].totalAidRequests) * 100;
      final double percentageBalaqa =
          (r[0].balqaTotalRequestAid / r[0].totalAidRequests) * 100;
      final List<List<List<InfoCardData>>> totals = [];
      final List<List<InfoCardData>> typesTotals = [];
      final List<InfoCardData> orders = [];
      final List<InfoCardData> meters = [];
      final List<InfoCardData> subscriberType = [];
      final List<InfoCardData> subscriptionType = [];
      const jordanianTypeColor = Colors.green;
      const gazaTypeColor = Colors.blue;
      const tempPassportTypeColor = Colors.red;
      //category
      const normalCategoryColor = Colors.deepOrange;
      const takafulCategoryColor = Colors.black;
      const marriedToNonJordanianCategoryColor = Colors.pink;

      final double percentageAidRequest =
          (r[0].totalAidRequests / activeTotal) * 100;

      final double percentageService =
          (r[0].servicesCustomers / activeTotal) * 100;
      orders.add(
        InfoCardData(
          'عدد الطلبات',
          r[0].totalRequests,
        ),
      );
      orders.add(
        InfoCardData(
          'عدد طلبات الدعم',
          r[0].totalAidRequests,
        ),
      );
      orders.add(
        InfoCardData(
          'عدد طلبات الدعم اليوم',
          r[0].totalAidRequestsToday,
        ),
      );
      orders.add(
        InfoCardData(
          'عدد طلبات الغاء الدعم',
          r[0].totalCancelAidRequests,
        ),
      );
      orders.add(
        InfoCardData(
          'عدد طلبات الغاء الدعم اليوم',
          r[0].totalCancelAidRequestsToday,
        ),
      );
      typesTotals.add(orders);
      totals.add(typesTotals);
      meters.add(
        InfoCardData(
          'عدد عدادات السكنية',
          r[0].restdnitalCustomers,
        ),
      );
      meters.add(
        InfoCardData(
          'عدد الاشتراكات الفعالة',
          activeTotal,
        ),
      );
      meters.add(
        InfoCardData(
          'عدد عداد الخدمات',
          r[0].servicesCustomers,
        ),
      );
      meters.add(
        InfoCardData(
          'عدد الاشتراكات المفصولة',
          r[0].disconnctedMeters,
        ),
      );
      meters.add(
        InfoCardData(
          'عدد الاشتراكات الغير فعالة',
          r[0].inactiveCustomers,
        ),
      );

      typesTotals.add(meters);
      totals.add(typesTotals);
      subscriberType.add(
        InfoCardData(
          'اردني',
          r[0].userTypeTotalJordaian,
          percentage:
              (r[0].userTypeTotalJordaian / r[0].totalAidRequests) * 100,
          percentageColor: jordanianTypeColor,
        ),
      );
      subscriberType.add(
        InfoCardData(
          'جوازات مؤقتة',
          r[0].userTypeforeign,
          percentage: (r[0].userTypeforeign / r[0].totalAidRequests) * 100,
          percentageColor: tempPassportTypeColor,
        ),
      );
      subscriberType.add(
        InfoCardData(
          'غزة',
          r[0].userTypeTotalGaza,
          percentage: (r[0].userTypeTotalGaza / r[0].totalAidRequests) * 100,
          percentageColor: gazaTypeColor,
        ),
      );
      typesTotals.add(subscriberType);
      totals.add(typesTotals);
      subscriptionType.add(
        InfoCardData(
          'عادي',
          r[0].userCatgoryNormal,
          percentage: (r[0].userCatgoryNormal / r[0].totalAidRequests) * 100,
          percentageColor: normalCategoryColor,
        ),
      );
      subscriptionType.add(
        InfoCardData(
          'صندوق معونة ومكرمة ملكية',
          r[0].userCatgoryTakaful,
          percentage: (r[0].userCatgoryTakaful / r[0].totalAidRequests) * 100,
          percentageColor: takafulCategoryColor,
        ),
      );
      subscriptionType.add(
        InfoCardData(
          'متزوجة من غير اردني',
          r[0].userCatgoryMarriedtoNonJordanian,
          percentage:
              (r[0].userCatgoryMarriedtoNonJordanian / r[0].totalAidRequests) *
                  100,
          percentageColor: marriedToNonJordanianCategoryColor,
        ),
      );
      typesTotals.add(subscriptionType);
      totals.add(typesTotals);
      emit(DashboardLoaded(
        r,
        activeTotal,
        percentageAmman.toPrecision(2),
        percentageZarqa.toPrecision(2),
        percentageMadaba.toPrecision(2),
        percentageBalaqa.toPrecision(2),
        totals,
        percentageAidRequest.toPrecision(2),
        percentageService.toPrecision(2),
      ));
    });
  }

  void clear() {
    emit(DashboardInitial());
  }
}
