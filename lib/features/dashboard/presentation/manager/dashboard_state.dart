part of 'dashboard_cubit.dart';

abstract class DashboardState extends Equatable {
  const DashboardState();
}

class DashboardInitial extends DashboardState {
  @override
  List<Object> get props => [];
}

class DashboardLoading extends DashboardState {
  @override
  List<Object> get props => [];
}

class DashboardLoaded extends DashboardState {
  final int activeTotal;
  final List<DBTotal> dbTotals;
  final double percentageAidRequest;
  final double percentageService;
  final double percentageAmmman;
  final double percentageMadaba;
  final double percentageBalqa;
  final double percentageZarqa;
  final List<List<List<InfoCardData>>> totals;
  const DashboardLoaded(
    this.dbTotals,
    this.activeTotal,
    this.percentageAmmman,
    this.percentageBalqa,
    this.percentageMadaba,
    this.percentageZarqa,
    this.totals,
    this.percentageAidRequest,
    this.percentageService,
  );

  @override
  List<Object> get props => [dbTotals];
}

class DashboardFailed extends DashboardState {
  final String message;

  const DashboardFailed(this.message);

  @override
  List<Object> get props => [message];
}
