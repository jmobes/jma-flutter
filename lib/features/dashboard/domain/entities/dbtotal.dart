class DBTotal {
  final int totalCustomers;
  final int totalRequests;
  final int totalAidRequests;
  final int totalCancelAidRequests;
  final int totalCancelAidRequestsToday;
  final int totalAidRequestsToday;
  final int restdnitalCustomers;
  final int servicesCustomers;
  final int inactiveCustomers;
  final int disconnctedMeters;
  final int ammanTotalRequestAid;
  final int zarqaTotalRequestAid;
  final int balqaTotalRequestAid;
  final int madbaTotalRequestsAid;
  final int userTypeTotalJordaian;
  final int userTypeforeign;
  final int userTypeTotalGaza;
  final int userCatgoryNormal;
  final int userCatgoryTakaful;
  final int userCatgoryMarriedtoNonJordanian;
  final int userCatgorySyrianRefugee;
  const DBTotal({
    required this.totalCustomers,
    required this.totalRequests,
    required this.totalAidRequests,
    required this.totalAidRequestsToday,
    required this.totalCancelAidRequests,
    required this.totalCancelAidRequestsToday,
    required this.restdnitalCustomers,
    required this.servicesCustomers,
    required this.inactiveCustomers,
    required this.disconnctedMeters,
    required this.ammanTotalRequestAid,
    required this.zarqaTotalRequestAid,
    required this.balqaTotalRequestAid,
    required this.madbaTotalRequestsAid,
    required this.userTypeTotalJordaian,
    required this.userTypeforeign,
    required this.userTypeTotalGaza,
    required this.userCatgoryNormal,
    required this.userCatgoryTakaful,
    required this.userCatgoryMarriedtoNonJordanian,
    required this.userCatgorySyrianRefugee,
  });
}
/* {
            "intTotalCustomers": 435857,
            "intTotalRequests": 437960,
            "intTotalCustomerDeserverAidRequestCount": 436740,
            "intTotalCustomerCancelDeserverRequestAidCount": 1220,
            "intTotalCustomerCancelDeserverAidRequestCountToday": 225,
            "intTotalCustomerDeserverAidRequestCountToday": 471,
            "intRestdnitalCustomers": 1310282,
            "intServicesCustomers": 84819,
            "intInactiveCustomers": 126779,
            "intDisconnctedMeters": 89215,
            "intAmmanTotalRequestAid": 292089,
            "intZarqaTotalRequestAid": 96117,
            "intBalqaTotalRequestAid": 34931,
            "intMadbaTotalRequestsAid": 12968,
            "intUserTypeTotalJordaian": 430404,
            "intUserTypeforeign": 610,
            "intUserTypeTotalGaza": 5726,
            "intUserCatgoryNormal": 370678,
            "intUserCatgoryTakaful": 57970,
            "intUserCatgoryMarriedtoNonJordanian": 8092,
            "intUserCatgorySyrianRefugee": 0
        }*/
