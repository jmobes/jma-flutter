import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/features/dashboard/domain/entities/dbtotal.dart';

abstract class DashboardRepository {
  Future<Either<Failure, List<DBTotal>>> getTotals();
}
