import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/dashboard/domain/entities/dbtotal.dart';
import 'package:jma/features/dashboard/domain/repositories/dashboard_repository.dart';

class GetTotalsUseCase implements UseCase<List<DBTotal>, NoParams> {
  final DashboardRepository repository;

  const GetTotalsUseCase({required this.repository});

  @override
  Future<Either<Failure, List<DBTotal>>> call(NoParams params) {
    return repository.getTotals();
  }
}
