import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/dashboard/data/remote/data_sources/dashboard_remote_data_source.dart';
import 'package:jma/features/dashboard/domain/entities/dbtotal.dart';
import 'package:jma/features/dashboard/domain/repositories/dashboard_repository.dart';

class DashboardRepositoryImpl implements DashboardRepository {
  final DashboardRemoteDataSource remoteDataSource;

  const DashboardRepositoryImpl({
    required this.remoteDataSource,
  });

  @override
  Future<Either<Failure, List<DBTotal>>> getTotals() async {
    return getRepositoryResponse<List<DBTotal>>(
      remoteDataSource.getTotals(),
      // networkInfo,
    );
  }
}
