import 'package:jma/features/dashboard/domain/entities/dbtotal.dart';

class DBTotalModel extends DBTotal {
  const DBTotalModel({
    required int totalCustomers,
    required int totalRequests,
    required int totalAidRequests,
    required int totalAidRequestsToday,
    required int totalCancelAidRequests,
    required int totalCancelAidRequestsToday,
    required int restdnitalCustomers,
    required int servicesCustomers,
    required int inactiveCustomers,
    required int disconnctedMeters,
    required int ammanTotalRequestAid,
    required int zarqaTotalRequestAid,
    required int balqaTotalRequestAid,
    required int madbaTotalRequestsAid,
    required int userTypeTotalJordaian,
    required int userTypeforeign,
    required int userTypeTotalGaza,
    required int userCatgoryNormal,
    required int userCatgoryTakaful,
    required int userCatgoryMarriedtoNonJordanian,
    required int userCatgorySyrianRefugee,
  }) : super(
          totalCustomers: totalCustomers,
          totalRequests: totalRequests,
          totalAidRequests: totalAidRequests,
          totalAidRequestsToday: totalAidRequestsToday,
          totalCancelAidRequests: totalCancelAidRequests,
          totalCancelAidRequestsToday: totalCancelAidRequestsToday,
          restdnitalCustomers: restdnitalCustomers,
          servicesCustomers: servicesCustomers,
          inactiveCustomers: inactiveCustomers,
          disconnctedMeters: disconnctedMeters,
          ammanTotalRequestAid: ammanTotalRequestAid,
          zarqaTotalRequestAid: zarqaTotalRequestAid,
          balqaTotalRequestAid: balqaTotalRequestAid,
          madbaTotalRequestsAid: madbaTotalRequestsAid,
          userTypeTotalJordaian: userTypeTotalJordaian,
          userTypeforeign: userTypeforeign,
          userTypeTotalGaza: userTypeTotalGaza,
          userCatgoryNormal: userCatgoryNormal,
          userCatgoryTakaful: userCatgoryTakaful,
          userCatgoryMarriedtoNonJordanian: userCatgoryMarriedtoNonJordanian,
          userCatgorySyrianRefugee: userCatgorySyrianRefugee,
        );

  factory DBTotalModel.fromJson(Map<String, dynamic> json) {
    return DBTotalModel(
      totalCustomers: json['intTotalCustomers'] as int,
      totalRequests: json['intTotalRequests'] as int,
      totalAidRequests: json['intTotalCustomerDeserverAidRequestCount'] as int,
      totalAidRequestsToday:
          json['intTotalCustomerDeserverAidRequestCountToday'] as int,
      totalCancelAidRequests:
          json['intTotalCustomerCancelDeserverRequestAidCount'] as int,
      totalCancelAidRequestsToday:
          json['intTotalCustomerCancelDeserverAidRequestCountToday'] as int,
      restdnitalCustomers: json['intRestdnitalCustomers'] as int,
      servicesCustomers: json['intServicesCustomers'] as int,
      inactiveCustomers: json['intInactiveCustomers'] as int,
      disconnctedMeters: json['intDisconnctedMeters'] as int,
      ammanTotalRequestAid: json['intAmmanTotalRequestAid'] as int,
      zarqaTotalRequestAid: json['intZarqaTotalRequestAid'] as int,
      balqaTotalRequestAid: json['intBalqaTotalRequestAid'] as int,
      madbaTotalRequestsAid: json['intMadbaTotalRequestsAid'] as int,
      userTypeTotalJordaian: json['intUserTypeTotalJordaian'] as int,
      userTypeforeign: json['intUserTypeforeign'] as int,
      userTypeTotalGaza: json['intUserTypeTotalGaza'] as int,
      userCatgoryNormal: json['intUserCatgoryNormal'] as int,
      userCatgoryTakaful: json['intUserCatgoryTakaful'] as int,
      userCatgoryMarriedtoNonJordanian:
          json['intUserCatgoryMarriedtoNonJordanian'] as int,
      userCatgorySyrianRefugee: json['intUserCatgorySyrianRefugee'] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['intTotalCustomers'] = totalCustomers;
    map['intTotalRequests'] = totalRequests;
    map['intTotalCustomerDeserverAidRequestCount'] = totalAidRequests;
    map['intTotalCustomerDeserverAidRequestCountToday'] = totalAidRequestsToday;
    map['intTotalCustomerCancelDeserverRequestAidCount'] =
        totalCancelAidRequests;
    map['intTotalCustomerCancelDeserverAidRequestCountToday'] =
        totalCancelAidRequestsToday;
    map['intServicesCustomers'] = servicesCustomers;
    map['intInactiveCustomers'] = inactiveCustomers;
    map['intDisconnctedMeters'] = disconnctedMeters;
    map['intAmmanTotalRequestAid'] = ammanTotalRequestAid;
    map['intZarqaTotalRequestAid'] = zarqaTotalRequestAid;
    map['intBalqaTotalRequestAid'] = balqaTotalRequestAid;
    map['intMadbaTotalRequestsAid'] = madbaTotalRequestsAid;
    map['intUserTypeTotalJordaian'] = userTypeTotalJordaian;
    map['intUserTypeforeign'] = userTypeforeign;
    map['intUserTypeTotalGaza'] = userTypeTotalGaza;
    map['intUserCatgoryNormal'] = userCatgoryNormal;
    map['intUserCatgoryTakaful'] = userCatgoryTakaful;
    map['intUserCatgoryMarriedtoNonJordanian'] =
        userCatgoryMarriedtoNonJordanian;
    map['intUserCatgorySyrianRefugee'] = userCatgorySyrianRefugee;
    return map;
  }

  // {
  //           "intTotalCustomers": 435857,
  //           "intTotalRequests": 437960,
  //           "intTotalCustomerDeserverAidRequestCount": 436740,
  //           "intTotalCustomerCancelDeserverRequestAidCount": 1220,
  //           "intTotalCustomerCancelDeserverAidRequestCountToday": 225,
  //           "intTotalCustomerDeserverAidRequestCountToday": 471,
  //           "intRestdnitalCustomers": 1310282,
  //           "intServicesCustomers": 84819,
  //           "intInactiveCustomers": 126779,
  //           "intDisconnctedMeters": 89215,
  //           "intAmmanTotalRequestAid": 292089,
  //           "intZarqaTotalRequestAid": 96117,
  //           "intBalqaTotalRequestAid": 34931,
  //           "intMadbaTotalRequestsAid": 12968,
  //           "intUserTypeTotalJordaian": 430404,
  //           "intUserTypeforeign": 610,
  //           "intUserTypeTotalGaza": 5726,
  //           "intUserCatgoryNormal": 370678,
  //           "intUserCatgoryTakaful": 57970,
  //           "intUserCatgoryMarriedtoNonJordanian": 8092,
  //           "intUserCatgorySyrianRefugee": 0
  //       }
}
