import 'package:dio/dio.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/utils/constants.dart';
import 'package:retrofit/http.dart';

part 'dashboard_api_interface.g.dart';

@RestApi(baseUrl: kBaseUrl)
abstract class DashboardAPIInterface {
  factory DashboardAPIInterface(Dio dio) {
    return _DashboardAPIInterface(dio);
  }

  @POST(kGetTotalsEndpoint)
  Future<APIResponse> getTotals(@Body() Map<String, dynamic> map);
}
