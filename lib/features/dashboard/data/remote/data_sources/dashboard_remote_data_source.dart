import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/data/remote/resource/app_api_res.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/dashboard/data/remote/data_sources/dashboard_api_interface.dart';
import 'package:json_annotation/json_annotation.dart';

abstract class DashboardRemoteDataSource {
  Future<APPApiResponse> getTotals();
}

class DashboardRemoteDataSourceImpl implements DashboardRemoteDataSource {
  final DashboardAPIInterface apiInterface;

  DashboardRemoteDataSourceImpl({required this.apiInterface});

  @override
  Future<APPApiResponse> getTotals() async {
    return getAppApiResponse<APIResponse>(
      apiInterface.getTotals(
        const GetTotalsParams('AR').toJson(),
      ),
    );
  }
}

@JsonSerializable()
class GetTotalsParams extends Equatable {
  final String languageId;

  const GetTotalsParams(this.languageId);

  Map<String, dynamic> toJson() {
    return {
      "LanguageId": languageId,
    };
  }

  @override
  List<Object?> get props => [languageId];
}
