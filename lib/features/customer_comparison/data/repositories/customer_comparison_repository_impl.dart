import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/customer_comparison/data/remote/data_sources/customer_comparisons_remote_data_source.dart';
import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';
import 'package:jma/features/customer_comparison/domain/entities/export_all.dart';
import 'package:jma/features/customer_comparison/domain/repositories/customer_comparison_repository.dart';

class CustomerComparisonRepositoryImpl implements CustomerComparisonRepository {
  final CustomerComparisonsRemoteDataSource customerComparisonsRemoteDataSource;

  const CustomerComparisonRepositoryImpl({
    required this.customerComparisonsRemoteDataSource,
  });

  @override
  Future<Either<Failure, List<CustomerComparison>>>
      getFullMatchCustomerComparisons(String from, String to) {
    return getRepositoryResponse<List<CustomerComparison>>(
      customerComparisonsRemoteDataSource.getFullMCustomerComparisons(from, to),
      // networkInfo,
    );
  }

  @override
  Future<Either<Failure, List<CustomerComparison>>>
      getNoMatchCustomerComparisons(String from, String to) {
    return getRepositoryResponse<List<CustomerComparison>>(
      customerComparisonsRemoteDataSource.getNoMCustomerComparisons(from, to),
      // networkInfo,
    );
  }

  @override
  Future<Either<Failure, List<CustomerComparison>>>
      getSimiMatchCustomerComparisons(String from, String to) {
    return getRepositoryResponse<List<CustomerComparison>>(
      customerComparisonsRemoteDataSource.getSimiMCustomerComparisons(from, to),
      // networkInfo,
    );
  }

  @override
  Future<Either<Failure, ExportAll>> exportAll(int type) {
    switch (type) {
      case 2:
        {
          return getRepositoryResponse<ExportAll>(
            customerComparisonsRemoteDataSource.exportAllSimiMatch(),
          );
        }
      case 3:
        {
          return getRepositoryResponse<ExportAll>(
            customerComparisonsRemoteDataSource.exportAllNoMatch(),
          );
        }
      default:
        {
          return getRepositoryResponse<ExportAll>(
            customerComparisonsRemoteDataSource.exportAllFullMatch(),
          );
        }
    }
  }
}
