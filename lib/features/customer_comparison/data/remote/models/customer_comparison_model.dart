import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';

class CustomerComparisonModel extends CustomerComparison {
  CustomerComparisonModel({
    required String fileNumber,
    required String emrcMeterNumber,
    required String jepcoMeterNumber,
    required String emrcCustomerName,
    required String jepcoCustomerName,
    required String fullNameComparisonStatusText,
    required String fullNameComparisonPercentage,
    required String emrcCustomerPhoneNumber,
    required String emrcNationalNumber,
  }) : super(
          fileNumber: fileNumber,
          emrcMeterNumber: emrcMeterNumber,
          jepcoMeterNumber: jepcoMeterNumber,
          emrcCustomerName: emrcCustomerName,
          jepcoCustomerName: jepcoCustomerName,
          fullNameComparisonStatusText: fullNameComparisonStatusText,
          fullNameComparisonPercentage: fullNameComparisonPercentage,
          emrcCustomerPhoneNumber: emrcCustomerPhoneNumber,
          emrcNationalNumber: emrcNationalNumber,
        );

  factory CustomerComparisonModel.fromJson(Map<String, dynamic> json) {
    return CustomerComparisonModel(
      fileNumber: json['fileNumber'] as String,
      emrcMeterNumber: json['emrcMeterNumber'] as String,
      jepcoMeterNumber: json['jepcoMeterNumber'] as String,
      emrcCustomerName: json['emrcCustomerName'] as String,
      jepcoCustomerName: json['jepcoCustomerName'] as String,
      fullNameComparisonStatusText: json['fullNameCompersionStatus'] as String,
      fullNameComparisonPercentage:
          json['fullNameCompersionPercentage'] as String,
      emrcCustomerPhoneNumber: json['emrcCustomerPhoneNumber'] as String,
      emrcNationalNumber: json['emrcNationalNumber'] as String,
    );
  }

/*{
            "fileNumber": "0110001001264",
            "emrcMeterNumber": "20142053780",
            "jepcoMeterNumber": "20142053780",
            "emrcCustomerName": "موسى محمود سعيد بج",
            "jepcoCustomerName": "موسى محمود سعيد بج",
            "fullNameCompersionStatus": "الاسم كامل مطابق",
            "fullNameCompersionPercentage": "100%",
            "emrcCustomerPhoneNumber": "07 7796 9555",
            "emrcNationalNumber": "9721002454"
        }*/
}
