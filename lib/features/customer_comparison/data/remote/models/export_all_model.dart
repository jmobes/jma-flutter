import 'package:jma/features/customer_comparison/domain/entities/export_all.dart';

class ExportAllModel extends ExportAll {
  const ExportAllModel(String fileBytes) : super(fileBytes);

  factory ExportAllModel.fromJson(Map<String, dynamic> json) {
    return ExportAllModel(json['execlBytes'] as String);
  }
}
