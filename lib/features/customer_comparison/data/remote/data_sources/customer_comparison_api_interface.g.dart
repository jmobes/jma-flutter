// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_comparison_api_interface.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps

class _CustomerComparisonAPIInterface
    implements CustomerComparisonAPIInterface {
  _CustomerComparisonAPIInterface(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://mobile.jepco.com.jo/EMRCPortalApis/';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<APIResponse> getFullMatchCustomerComparisons(map) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(map);
    final _result = await _dio.fetch<Map<String, dynamic>>(_setStreamType<
        APIResponse>(Options(
            method: 'POST', headers: _headers, extra: _extra)
        .compose(_dio.options,
            'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDataFullMatch',
            queryParameters: queryParameters, data: _data)
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = APIResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<APIResponse> getSimiMatchCustomerComparisons(map) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(map);
    final _result = await _dio.fetch<Map<String, dynamic>>(_setStreamType<
        APIResponse>(Options(
            method: 'POST', headers: _headers, extra: _extra)
        .compose(_dio.options,
            'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDatasimiMatch',
            queryParameters: queryParameters, data: _data)
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = APIResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<APIResponse> getNoMatchCustomerComparisons(map) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(map);
    final _result = await _dio.fetch<Map<String, dynamic>>(_setStreamType<
        APIResponse>(Options(
            method: 'POST', headers: _headers, extra: _extra)
        .compose(_dio.options,
            'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDataNotMatch',
            queryParameters: queryParameters, data: _data)
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = APIResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<APIResponse> exportAllFullMatch(map) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(map);
    final _result = await _dio.fetch<Map<String, dynamic>>(_setStreamType<
        APIResponse>(Options(
            method: 'POST', headers: _headers, extra: _extra)
        .compose(_dio.options,
            'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDataFullMatchExportExcel',
            queryParameters: queryParameters, data: _data)
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = APIResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<APIResponse> exportAllSimiMatch(map) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(map);
    final _result = await _dio.fetch<Map<String, dynamic>>(_setStreamType<
        APIResponse>(Options(
            method: 'POST', headers: _headers, extra: _extra)
        .compose(_dio.options,
            'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDatasimiMatchExportExcel',
            queryParameters: queryParameters, data: _data)
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = APIResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<APIResponse> exportAllNoMatch(map) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _headers = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(map);
    final _result = await _dio.fetch<Map<String, dynamic>>(_setStreamType<
        APIResponse>(Options(
            method: 'POST', headers: _headers, extra: _extra)
        .compose(_dio.options,
            'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDataNotMatchExportExcel',
            queryParameters: queryParameters, data: _data)
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = APIResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
