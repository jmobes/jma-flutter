import 'package:dio/dio.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/utils/constants.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';

part 'customer_comparison_api_interface.g.dart';

@RestApi(baseUrl: kBaseUrl)
abstract class CustomerComparisonAPIInterface {
  factory CustomerComparisonAPIInterface(Dio dio) {
    return _CustomerComparisonAPIInterface(dio);
  }

  @POST(kGetFullMCustomerComparisonsEndpoint)
  Future<APIResponse> getFullMatchCustomerComparisons(
    @Body() Map<String, dynamic> map,
  );

  @POST(kGetSimiMCustomerComparisonsEndpoint)
  Future<APIResponse> getSimiMatchCustomerComparisons(
    @Body() Map<String, dynamic> map,
  );

  @POST(kGetNoMCustomerComparisonsEndpoint)
  Future<APIResponse> getNoMatchCustomerComparisons(
    @Body() Map<String, dynamic> map,
  );

  @POST(kExportAllFullMatchEndpoint)
  Future<APIResponse> exportAllFullMatch(
    @Body() Map<String, dynamic> map,
  );

  @POST(kExportAllSimiMatchEndpoint)
  Future<APIResponse> exportAllSimiMatch(
    @Body() Map<String, dynamic> map,
  );

  @POST(kExportAllNoMatchEndpoint)
  Future<APIResponse> exportAllNoMatch(
    @Body() Map<String, dynamic> map,
  );
}
