import 'package:equatable/equatable.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/data/remote/resource/app_api_res.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/customer_comparison/data/remote/data_sources/customer_comparison_api_interface.dart';
import 'package:json_annotation/json_annotation.dart';

abstract class CustomerComparisonsRemoteDataSource {
  Future<APPApiResponse> getFullMCustomerComparisons(String from, String to);

  Future<APPApiResponse> getSimiMCustomerComparisons(String from, String to);

  Future<APPApiResponse> getNoMCustomerComparisons(String from, String to);

  Future<APPApiResponse> exportAllFullMatch();

  Future<APPApiResponse> exportAllSimiMatch();

  Future<APPApiResponse> exportAllNoMatch();
}

class CustomerComparisonsRemoteDataSourceImpl
    implements CustomerComparisonsRemoteDataSource {
  final CustomerComparisonAPIInterface apiInterface;

  const CustomerComparisonsRemoteDataSourceImpl({required this.apiInterface});

  @override
  Future<APPApiResponse> getFullMCustomerComparisons(String from, String to) {
    return getAppApiResponse<APIResponse>(
      apiInterface.getFullMatchCustomerComparisons(
        MatchParams(from, to, 'AR').toJson(),
      ),
    );
  }

  @override
  Future<APPApiResponse> getNoMCustomerComparisons(String from, String to) {
    return getAppApiResponse<APIResponse>(
      apiInterface.getNoMatchCustomerComparisons(
        MatchParams(from, to, 'AR').toJson(),
      ),
    );
  }

  @override
  Future<APPApiResponse> getSimiMCustomerComparisons(String from, String to) {
    return getAppApiResponse<APIResponse>(
      apiInterface.getSimiMatchCustomerComparisons(
        MatchParams(from, to, 'AR').toJson(),
      ),
    );
  }

  @override
  Future<APPApiResponse> exportAllFullMatch() {
    return getAppApiResponse<APIResponse>(
      apiInterface.exportAllFullMatch(
        const ExportParams('AR').toJson(),
      ),
    );
  }

  @override
  Future<APPApiResponse> exportAllNoMatch() {
    return getAppApiResponse<APIResponse>(
      apiInterface.exportAllNoMatch(
        const ExportParams('AR').toJson(),
      ),
    );
  }

  @override
  Future<APPApiResponse> exportAllSimiMatch() {
    return getAppApiResponse<APIResponse>(
      apiInterface.exportAllSimiMatch(
        const ExportParams('AR').toJson(),
      ),
    );
  }
}

@JsonSerializable()
class MatchParams extends Equatable {
  final String from;
  final String to;
  final String languageId;

  const MatchParams(this.from, this.to, this.languageId);

  Map<String, dynamic> toJson() {
    return {
      "AidRequestDateFrom": from,
      "AidRequestDateTo": to,
      "LanguageId": languageId,
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [from, to, languageId];
}

class ExportParams extends Equatable {
  final String languageId;

  const ExportParams(this.languageId);

  Map<String, dynamic> toJson() {
    return {
      "LanguageId": languageId,
    };
  }

  @override
  // TODO: implement props
  List<Object?> get props => [languageId];
}
