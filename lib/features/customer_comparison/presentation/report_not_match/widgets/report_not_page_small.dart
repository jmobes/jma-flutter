import 'package:date_time_picker/date_time_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:jma/core/presentation/manager/export/cubit.dart';
import 'package:jma/core/presentation/manager/server_date_cubit.dart';
import 'package:jma/core/presentation/manager/server_date_state.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/core/utils/key-lang.dart';
import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';
import 'package:jma/features/customer_comparison/presentation/adpaters/report_adapter.dart';
import 'package:jma/features/customer_comparison/presentation/manager/customer_comparison_cubit.dart';
import 'package:jma/features/customer_comparison/presentation/manager/export_all_cubit.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

import '../../../../../core/presentation/widgets/loading_view_with_text.dart';

class ReportNotMatchSmallScreen extends StatefulWidget {
  const ReportNotMatchSmallScreen({Key? key}) : super(key: key);

  @override
  State<ReportNotMatchSmallScreen> createState() =>
      _ReportNotMatchSmallScreenState();
}

class _ReportNotMatchSmallScreenState extends State<ReportNotMatchSmallScreen> {
  String? dateFrom;
  String? dateTo;
  late ReportAdapter _reportNotMatchAdapter;
  late TextEditingController _dateFrom;
  late TextEditingController _dateTo;
  GlobalKey<FormState> _keyReportNotMatchpage = GlobalKey<FormState>();
  final GlobalKey<SfDataGridState> _key = GlobalKey<SfDataGridState>();

  @override
  void initState() {
    super.initState();
    _dateFrom = TextEditingController();
    _dateTo = TextEditingController();
    context.read<ServerDateCubit>().getServerDate();
  }

  Widget getLoadingView() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return BlocListener<ExportAllCubit, ExportAllState>(
      listener: (context, state) {
        if (state is ExportAllFailed) {
          showSnackBar(context, state.message);
        }
      },
      child: BlocBuilder<ExportAllCubit, ExportAllState>(
        builder: (context, state) {
          if (state is ExportAllInitial) {
            return getReportnotmatchView();
          } else if (state is ExportAllNotLoading) {
            return Center(
                child: loadingViewWithText2(
                    ' جاري تحميل الملف قد تستغرق العملية بضع دقائق'));
          } else {
            return getReportnotmatchView();
          }
        },
      ),
    );
  }

  Widget getReportnotmatchView() {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        Card(
          child: SizedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                BlocBuilder<ServerDateCubit, ServerDateState>(
                    builder: (context, state) {
                  if (state is ServerDateLoaded) {
                    return Form(
                      key: _keyReportNotMatchpage,
                      autovalidateMode: AutovalidateMode.always,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: width * .30,
                            height: height * .10,
                            child: DateTimePicker(
                              type: DateTimePickerType.date,
                              dateMask: 'yyyy/MM/dd',
                              controller: _dateFrom,
                              onChanged: (String _) => setState(() {}),
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2023),
                              // initialValue: dateC,
                              icon: Icon(Icons.event),
                              dateLabelText: KeyLang.fromDate.tr(),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return KeyLang.errordate.tr();
                                }
                                return null;
                              },
                            ),
                          ),
                          Container(
                            width: width * .30,
                            height: height * .10,
                            child: DateTimePicker(
                              type: DateTimePickerType.date,
                              dateMask: 'yyyy/MM/dd',
                              controller: _dateTo,
                              onChanged: (String _) => setState(() {}),
                              // initialValue: dateC,
                              firstDate: DateTime(
                                2000,
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return KeyLang.errordate.tr();
                                }
                                return null;
                              },
                              lastDate: DateTime(2023),
                              icon: Icon(Icons.event),
                              dateLabelText: KeyLang.toDate.tr(),
                            ),
                          ),
                        ],
                      ),
                    );
                  } else if (state is ServerDateLoading) {
                    return loadingViewWithText(
                      'يتم الحصول على تاريخ اليوم',
                    );
                  } else if (state is ServerDateFailed) {
                    return Text(state.message);
                  } else {
                    return Container();
                  }
                }),
                if (!_isDatesValid())
                  Text(
                    KeyLang.errorDateMore30.tr(),
                    style: const TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                SizedBox(height: 10),
                Text('** تصدير الكل قد تستغرق  بعض الوقت ',
                    style: TextStyle(color: Colors.red)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [buttonSearch(), buttonClear(), buttonExport()],
                ),
              ],
            ),
          ),
        ),
        BlocConsumer<CustomerComparisonCubit, CustomerComparisonState>(
            listener: (context, state) {
          if (state is CustomerComparisonFailed) {
            showSnackBar(context, state.message);
          }
        }, builder: (context, state) {
          return BlocBuilder<CustomerComparisonCubit, CustomerComparisonState>(
              builder: (uContext, uState) {
            if (uState is CustomerComparisonLoaded) {
              _reportNotMatchAdapter =
                  ReportAdapter(uState.customerComparisons);
              return SizedBox(
                height: MediaQuery.of(context).size.height * .60,
                child: Card(
                    child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        margin: EdgeInsets.all(5),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: ElevatedButton.icon(
                              onPressed: () {
                                context
                                    .read<ExportCubit>()
                                    .exportDataGridToExcel(
                                        _key, 'ReportNotMatch.xlsx');
                              },
                              icon: Icon(Icons.send_to_mobile),
                              label: Text(KeyLang.export.tr())),
                        ),
                      ),
                      Container(
                        constraints: const BoxConstraints(
                          minWidth: 100,
                          minHeight: 100,
                        ),
                        height: MediaQuery.of(context).size.height * .40,
                        child: getDataTable(uState.customerComparisons),
                      ),
                      SfDataPager(
                        delegate: _reportNotMatchAdapter,
                        pageCount: (uState.customerComparisons.length / 10)
                            .ceil()
                            .toDouble(),
                        direction: Axis.horizontal,
                      ),
                    ],
                  ),
                )),
              );
            } else if (uState is CustomerComparisonLoading)
              return getLoadingView();
            else
              return Container();
          });
        })
      ],
    );
  }

  Widget buttonClear() {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: width * .20,
        height: height * .05,
        child: ElevatedButton(
          onPressed: () async {
            _clearData();
          },
          style: ElevatedButton.styleFrom(
            primary: AppColors.buttonandtitle,
          ),
          child: Text(
            KeyLang.clear.tr(),
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
        ),
      ),
    );
  }

  Widget buttonExport() {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: width * .25,
        height: height * .05,
        child: ElevatedButton(
          onPressed: () async {
            context
                .read<ExportAllCubit>()
                .exportAllNot(3, 'Reportnotmatchall.xlsx');
          },
          style: ElevatedButton.styleFrom(
            primary: AppColors.buttonandtitle,
          ),
          child: Text(
            KeyLang.exportAll.tr(),
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
        ),
      ),
    );
  }

  Widget buttonSearch() {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: width * .20,
        height: height * .05,
        child: ElevatedButton(
          onPressed: () async {
            if (_keyReportNotMatchpage.currentState!.validate() &&
                _isDatesValid()) {
              context
                  .read<CustomerComparisonCubit>()
                  .getNoMCustomerComparisons(_dateFrom.text, _dateTo.text);
            }
          },
          style: ElevatedButton.styleFrom(
            primary: AppColors.buttonandtitle,
          ),
          child: Text(
            KeyLang.search.tr(),
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
        ),
      ),
    );
  }

  Widget getDataTable(List<CustomerComparison> customerComparison) {
    return SfDataGridTheme(
      data: SfDataGridThemeData(
        headerColor: AppColors.headerTable,
      ),
      child: SfDataGrid(
          isScrollbarAlwaysShown: true,
          rowsPerPage: 10,
          defaultColumnWidth: 150,
          key: _key,
          allowSorting: true,
          source: _reportNotMatchAdapter,
          columns: getColumns(),
          columnWidthMode: ColumnWidthMode.none),
    );
  }

  List<GridColumn> getColumns() {
    return [
      getGridColumn(KeyLang.fileNumber.tr()),
      getGridColumn(KeyLang.emrcMeterNumber.tr()),
      getGridColumn(KeyLang.jepcoMeterNumber.tr()),
      getGridColumn(KeyLang.emrcCustomerName.tr()),
      getGridColumn(KeyLang.jepcoCustomerName.tr()),
      getGridColumn(KeyLang.fullNameCompersionStatus.tr()),
      getGridColumn(KeyLang.fullNameCompersionPercentage.tr()),
      getGridColumn(KeyLang.emrcCustomerPhoneNumber.tr()),
      getGridColumn(KeyLang.emrcNationalNumber.tr(), showExport: true),
    ];
  }

  GridColumn getGridColumn(String title,
      {bool showExport = false, Color tColor = Colors.white}) {
    return GridColumn(
        columnName: title,
        label: Center(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Center(
                  child: Text(
                    title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: tColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  _clearData() {
    setState(() {
      _dateFrom.clear();
      _dateTo.clear();

      context.read<CustomerComparisonCubit>().clear();
      ;
    });
  }

  bool _isDatesValid() {
    if (_dateFrom.text.isNotEmpty && _dateTo.text.isNotEmpty) {
      final theDatesFrom = _dateFrom.text.split('-');
      final year = int.parse(theDatesFrom[0]);
      final month = int.parse(theDatesFrom[1]);
      final day = int.parse(theDatesFrom[2]);
      final DateTime dateFrom = DateTime(
        year,
        month,
        day,
      );

      final theDatesTo = _dateTo.text.split('-');
      final yearT = int.parse(theDatesTo[0]);
      final monthT = int.parse(theDatesTo[1]);
      final dayT = int.parse(theDatesTo[2]);
      final DateTime dateTo = DateTime(
        yearT,
        monthT,
        dayT,
      );

      final int difference = dateTo.difference(dateFrom).inDays.abs();
      if (difference > 30) {
        return false;
      }
    }

    return true;
  }
}
