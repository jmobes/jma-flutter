import 'package:easy_localization/easy_localization.dart' hide TextDirection;
import 'package:flutter/material.dart';
import 'package:jma/config/custom_data_grid.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:jma/core/utils/key-lang.dart';
import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class ReportAdapter extends DataGridSource {
  /// Creates the employee data source class with required details.
  ReportAdapter(List<CustomerComparison> reportFullmatch) {
    _customerComparisonData = reportFullmatch
        .map<DataGridRow>((CustomerComparison customerComparison) {
      return DataGridRow(cells: <CustomDataGrid>[
        CustomDataGrid<String>(
            KeyLang.fileNumber.tr(), customerComparison.fileNumber),
        CustomDataGrid<String>(
          KeyLang.emrcMeterNumber.tr(),
          customerComparison.emrcMeterNumber,
        ),
        CustomDataGrid<String>(
            KeyLang.jepcoMeterNumber.tr(), customerComparison.jepcoMeterNumber),
        CustomDataGrid<String>(
            KeyLang.emrcCustomerName.tr(), customerComparison.emrcCustomerName),
        CustomDataGrid<String>(KeyLang.jepcoCustomerName.tr(),
            customerComparison.jepcoCustomerName),
        CustomDataGrid<String>(KeyLang.fullNameCompersionStatus.tr(),
            customerComparison.fullNameComparisonStatusText),
        CustomDataGrid<String>(KeyLang.fullNameCompersionPercentage.tr(),
            customerComparison.fullNameComparisonPercentage),
        CustomDataGrid<String>(
          KeyLang.emrcCustomerPhoneNumber.tr(),
          customerComparison.emrcCustomerPhoneNumber,
          isNumber: true,
        ),
        CustomDataGrid<String>(KeyLang.emrcNationalNumber.tr(),
            customerComparison.emrcNationalNumber),
      ]);
    }).toList();
  }

  List<DataGridRow> _customerComparisonData = <DataGridRow>[];

  @override
  List<DataGridRow> get rows => _customerComparisonData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    Color getRowBackgroundColor() {
      final int i = effectiveRows.indexOf(row);

      if (i % 2 != 0) {
        return AppColors.littelblueTable;
      }

      return Colors.transparent;
    }

    return DataGridRowAdapter(
        color: getRowBackgroundColor(),
        cells: row.getCells().map<Widget>((DataGridCell cell) {
          Widget getCellWidget() {
            if ((cell as CustomDataGrid).isNumber) {
              return Directionality(
                textDirection: TextDirection.ltr,
                child: Text(cell.value.toString()),
              );
            } else
              return Text(cell.value.toString());
          }

          return Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(8.0),
            child: getCellWidget(),
          );
        }).toList());
  }
}
