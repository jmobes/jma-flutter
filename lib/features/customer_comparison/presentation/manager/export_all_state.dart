part of 'export_all_cubit.dart';

abstract class ExportAllState extends Equatable {
  const ExportAllState();
}

class ExportAllInitial extends ExportAllState {
  @override
  List<Object> get props => [];
}

class ExportAllFullLoading extends ExportAllState {
  @override
  List<Object> get props => [];
}

class ExportAllFullSucceeded extends ExportAllState {
  @override
  List<Object> get props => [];
}

class ExportAllSimiLoading extends ExportAllState {
  @override
  List<Object> get props => [];
}

class ExportAllSimiSucceeded extends ExportAllState {
  @override
  List<Object> get props => [];
}

class ExportAllNotLoading extends ExportAllState {
  @override
  List<Object> get props => [];
}

class ExportAllNotSucceeded extends ExportAllState {
  @override
  List<Object> get props => [];
}

class ExportAllFailed extends ExportAllState {
  final String message;

  const ExportAllFailed(this.message);

  @override
  List<Object> get props => [message];
}
