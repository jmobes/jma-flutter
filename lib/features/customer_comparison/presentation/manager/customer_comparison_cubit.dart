import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';
import 'package:jma/features/customer_comparison/domain/use_cases/get_fullm_customer_comparisons_use_case.dart';
import 'package:jma/features/customer_comparison/domain/use_cases/get_nom_customer_comparisons_use_case.dart';
import 'package:jma/features/customer_comparison/domain/use_cases/get_simim_customer_comparisons_use_case.dart';

part 'customer_comparison_state.dart';

class CustomerComparisonCubit extends Cubit<CustomerComparisonState> {
  final GetFullMCustomerComparisonsUseCase getFullMCustomerComparisonsUseCase;
  final GetSimiMCustomerComparisonsUseCase getSimiMCustomerComparisonsUseCase;
  final GetNoMCustomerComparisonsUseCase getNoMCustomerComparisonsUseCase;

  CustomerComparisonCubit({
    required this.getFullMCustomerComparisonsUseCase,
    required this.getSimiMCustomerComparisonsUseCase,
    required this.getNoMCustomerComparisonsUseCase,
  }) : super(CustomerComparisonInitial());

  Future<void> getFullMCustomerComparisons(
    String from,
    String to,
  ) async {
    emit(CustomerComparisonLoading());
    final result =
        await getFullMCustomerComparisonsUseCase(FParams(from: from, to: to));

    result.fold(
      (l) => emit(CustomerComparisonFailed(l.message)),
      (r) => emit(CustomerComparisonLoaded(r)),
    );
  }

  Future<void> getSimiMCustomerComparisons(
    String from,
    String to,
  ) async {
    emit(CustomerComparisonLoading());
    final result =
        await getSimiMCustomerComparisonsUseCase(SParams(from: from, to: to));

    result.fold(
      (l) => emit(CustomerComparisonFailed(l.message)),
      (r) => emit(CustomerComparisonLoaded(r)),
    );
  }

  Future<void> getNoMCustomerComparisons(
    String from,
    String to,
  ) async {
    emit(CustomerComparisonLoading());
    final result =
        await getNoMCustomerComparisonsUseCase(NParams(from: from, to: to));

    result.fold(
      (l) => emit(CustomerComparisonFailed(l.message)),
      (r) => emit(CustomerComparisonLoaded(r)),
    );
  }

  void clear() {
    emit(CustomerComparisonInitial());
  }
}
