import 'dart:html';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/features/customer_comparison/domain/use_cases/export_all_use_case.dart';

part 'export_all_state.dart';

class ExportAllCubit extends Cubit<ExportAllState> {
  final ExportAllUseCase exportAllUseCase;

  ExportAllCubit(this.exportAllUseCase) : super(ExportAllInitial());

  Future<void> exportAllFull(int type, String title) async {
    emit(ExportAllFullLoading());

    final result = await exportAllUseCase(EParams(type));

    result.fold((l) => emit(ExportAllFailed(l.message)), (r) {
      try {
        AnchorElement(
          href:
              'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,${r.fileBytes}',
        )
          ..setAttribute('download', title)
          ..click();
        emit(ExportAllFullSucceeded());
      } on Exception catch (e) {
        emit(ExportAllFailed(e.toString()));
      }
    });
  }

  Future<void> exportAllSimi(int type, String title) async {
    emit(ExportAllSimiLoading());

    final result = await exportAllUseCase(EParams(type));

    result.fold((l) => emit(ExportAllFailed(l.message)), (r) {
      try {
        AnchorElement(
          href:
              'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,${r.fileBytes}',
        )
          ..setAttribute('download', title)
          ..click();
        emit(ExportAllFullSucceeded());
      } on Exception catch (e) {
        emit(ExportAllFailed(e.toString()));
      }
    });
  }

  Future<void> exportAllNot(int type, String title) async {
    emit(ExportAllNotLoading());

    final result = await exportAllUseCase(EParams(type));

    result.fold((l) => emit(ExportAllFailed(l.message)), (r) {
      try {
        AnchorElement(
          href:
              'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,${r.fileBytes}',
        )
          ..setAttribute('download', title)
          ..click();
        emit(ExportAllNotSucceeded());
      } on Exception catch (e) {
        emit(ExportAllFailed(e.toString()));
      }
    });
  }

  void clear() {
    emit(ExportAllInitial());
  }
}
