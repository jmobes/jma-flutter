part of 'customer_comparison_cubit.dart';

abstract class CustomerComparisonState extends Equatable {
  const CustomerComparisonState();
}

class CustomerComparisonInitial extends CustomerComparisonState {
  @override
  List<Object> get props => [];
}

class CustomerComparisonLoading extends CustomerComparisonState {
  @override
  List<Object> get props => [];
}

class CustomerComparisonLoaded extends CustomerComparisonState {
  final List<CustomerComparison> customerComparisons;

  const CustomerComparisonLoaded(this.customerComparisons);

  @override
  List<Object> get props => [customerComparisons];
}

class CustomerComparisonFailed extends CustomerComparisonState {
  final String message;

  const CustomerComparisonFailed(this.message);

  @override
  List<Object> get props => [message];
}
