import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';
import 'package:jma/features/customer_comparison/domain/repositories/customer_comparison_repository.dart';

class GetNoMCustomerComparisonsUseCase
    implements UseCase<List<CustomerComparison>, NParams> {
  final CustomerComparisonRepository repository;

  GetNoMCustomerComparisonsUseCase({required this.repository});

  @override
  Future<Either<Failure, List<CustomerComparison>>> call(NParams params) {
    return repository.getNoMatchCustomerComparisons(params.from, params.to);
  }
}

class NParams extends Equatable {
  final String from;
  final String to;

  const NParams({
    required this.from,
    required this.to,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [from, to];
}