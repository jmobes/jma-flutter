import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';
import 'package:jma/features/customer_comparison/domain/entities/export_all.dart';
import 'package:jma/features/customer_comparison/domain/repositories/customer_comparison_repository.dart';

class ExportAllUseCase implements UseCase<ExportAll, EParams> {
  final CustomerComparisonRepository repository;

  ExportAllUseCase({required this.repository});

  @override
  Future<Either<Failure, ExportAll>> call(EParams eParams) {
    return repository.exportAll(eParams.reportType);
  }
}

class EParams extends Equatable {
  final int reportType;

  const EParams(this.reportType);

  @override
  List<Object?> get props => [reportType];
}
