import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';
import 'package:jma/features/customer_comparison/domain/repositories/customer_comparison_repository.dart';

class GetSimiMCustomerComparisonsUseCase
    implements UseCase<List<CustomerComparison>, SParams> {
  final CustomerComparisonRepository repository;

  GetSimiMCustomerComparisonsUseCase({required this.repository});

  @override
  Future<Either<Failure, List<CustomerComparison>>> call(SParams params) {
    return repository.getSimiMatchCustomerComparisons(params.from, params.to);
  }
}

class SParams extends Equatable {
  final String from;
  final String to;

  const SParams({
    required this.from,
    required this.to,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [from, to];
}
