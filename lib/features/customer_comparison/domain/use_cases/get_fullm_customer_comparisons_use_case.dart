import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';
import 'package:jma/features/customer_comparison/domain/repositories/customer_comparison_repository.dart';

class GetFullMCustomerComparisonsUseCase
    implements UseCase<List<CustomerComparison>, FParams> {
  final CustomerComparisonRepository repository;

  GetFullMCustomerComparisonsUseCase({required this.repository});

  @override
  Future<Either<Failure, List<CustomerComparison>>> call(FParams params) {
    return repository.getFullMatchCustomerComparisons(params.from, params.to);
  }
}

class FParams extends Equatable {
  final String from;
  final String to;

  const FParams({
    required this.from,
    required this.to,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [from, to];
}
