import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';
import 'package:jma/features/customer_comparison/domain/entities/export_all.dart';

abstract class CustomerComparisonRepository {
  Future<Either<Failure, List<CustomerComparison>>>
      getFullMatchCustomerComparisons(String from, String to);

  Future<Either<Failure, List<CustomerComparison>>>
      getSimiMatchCustomerComparisons(String from, String to);

  Future<Either<Failure, List<CustomerComparison>>>
      getNoMatchCustomerComparisons(String from, String to);

  Future<Either<Failure, ExportAll>>
  exportAll(int type);
}
