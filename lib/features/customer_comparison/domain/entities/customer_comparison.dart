class CustomerComparison {
  final String fileNumber;
  final String emrcMeterNumber;
  final String jepcoMeterNumber;
  final String emrcCustomerName;
  final String jepcoCustomerName;
  final String fullNameComparisonStatusText;
  final String fullNameComparisonPercentage;
  final String emrcCustomerPhoneNumber;
  final String emrcNationalNumber;

  const CustomerComparison({
    required this.fileNumber,
    required this.emrcMeterNumber,
    required this.jepcoMeterNumber,
    required this.emrcCustomerName,
    required this.jepcoCustomerName,
    required this.fullNameComparisonStatusText,
    required this.fullNameComparisonPercentage,
    required this.emrcCustomerPhoneNumber,
    required this.emrcNationalNumber,
  });
}
/*{
            "fileNumber": "0110001001264",
            "emrcMeterNumber": "20142053780",
            "jepcoMeterNumber": "20142053780",
            "emrcCustomerName": "موسى محمود سعيد بج",
            "jepcoCustomerName": "موسى محمود سعيد بج",
            "fullNameCompersionStatus": "الاسم كامل مطابق",
            "fullNameCompersionPercentage": "100%",
            "emrcCustomerPhoneNumber": "07 7796 9555",
            "emrcNationalNumber": "9721002454"
        }*/
