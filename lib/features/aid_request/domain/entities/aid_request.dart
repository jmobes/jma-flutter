class AidRequest {
  final int id;
  final String nationalNumber;
  final String fileNumber;
  final String meterNumber;
  final String firstName;
  final String secondName;
  final String thirdName;
  final String lastName;
  final String mobileNumber;
  final bool isAided;
  final String requestDate;
  final dynamic supportType;
  final dynamic renewableFlag;
  final dynamic userCategory;
  final dynamic userType;
  const AidRequest({
    required this.id,
    required this.nationalNumber,
    required this.fileNumber,
    required this.meterNumber,
    required this.firstName,
    required this.secondName,
    required this.thirdName,
    required this.lastName,
    required this.mobileNumber,
    required this.isAided,
    required this.requestDate,
    required this.supportType,
    required this.renewableFlag,
    required this.userCategory,
    required this.userType,
  });
}
/*{
"id":1075657,
"national_Number":"9691013982",
"fileNumber":"0110708688773",
"meterNumber":"2007100163",
"first_Name":"سعود",
"second_Name":"ماجد",
"third_Name":"سعود",
"family_Name":"غيث ",
"customer_MobileNumber":"07 8572 0760",
"deserveEnergyAid":true,
"createdDate":"2022-01-30T00:00:00"
}
*/
