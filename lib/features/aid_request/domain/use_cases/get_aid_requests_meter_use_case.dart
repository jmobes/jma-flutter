import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:jma/features/aid_request/domain/repositories/aid_request_repository.dart';

class GetLastAidRequestsMeterNoUseCase
    implements UseCase<List<AidRequest>, GLParamsMeter> {
  final AidRequestRepository repository;

  const GetLastAidRequestsMeterNoUseCase({required this.repository});

  @override
  Future<Either<Failure, List<AidRequest>>> call(GLParamsMeter params) {
    return repository.getLastAidRequesmeterNo(params.meterNo);
  }
}

class GLParamsMeter extends Equatable {
  final String meterNo;

  const GLParamsMeter({
    required this.meterNo,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [meterNo];
}
