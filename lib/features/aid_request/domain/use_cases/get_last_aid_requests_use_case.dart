import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:jma/features/aid_request/domain/repositories/aid_request_repository.dart';

class GetLastAidRequestsUseCase implements UseCase<List<AidRequest>, GLParams> {
  final AidRequestRepository repository;

  const GetLastAidRequestsUseCase({required this.repository});

  @override
  Future<Either<Failure, List<AidRequest>>> call(GLParams params) {
    return repository.getLastAidRequests(
      params.from,
      params.to,
      params.type,
      params.supportType,
      params.renewableFlag,
      params.userCategory,
      params.userType,
    );
  }
}

class GLParams extends Equatable {
  final String from;
  final String to;
  final int type;
  final dynamic supportType;
  final dynamic renewableFlag;
  final dynamic userCategory;
  final dynamic userType;

  const GLParams({
    required this.from,
    required this.to,
    required this.type,
    required this.supportType,
    required this.renewableFlag,
    required this.userCategory,
    required this.userType,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [from, to, type];
}
