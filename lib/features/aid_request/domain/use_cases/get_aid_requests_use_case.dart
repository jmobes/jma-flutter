import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:jma/features/aid_request/domain/repositories/aid_request_repository.dart';

class GetAidRequestsUseCase implements UseCase<List<AidRequest>, Params> {
  final AidRequestRepository repository;

  const GetAidRequestsUseCase({required this.repository});

  @override
  Future<Either<Failure, List<AidRequest>>> call(Params params) {
    return repository.getAidRequestsForFileNumber(params.fileNumber);
  }
}

class Params extends Equatable {
  final String fileNumber;

  const Params({required this.fileNumber});

  @override
  // TODO: implement props
  List<Object?> get props => [fileNumber];
}
