import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:jma/features/aid_request/domain/repositories/aid_request_repository.dart';

class GetLastAidRequestsFileNoUseCase
    implements UseCase<List<AidRequest>, GLParamsFile> {
  final AidRequestRepository repository;

  const GetLastAidRequestsFileNoUseCase({required this.repository});

  @override
  Future<Either<Failure, List<AidRequest>>> call(GLParamsFile params) {
    return repository.getLastAidRequestfileNo(params.fileNo);
  }
}

class GLParamsFile extends Equatable {
  final String fileNo;

  const GLParamsFile({
    required this.fileNo,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [fileNo];
}
