import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';

abstract class AidRequestRepository {
  Future<Either<Failure, List<AidRequest>>> getLastAidRequests(
    String from,
    String to,
    int type,
    dynamic supportType,
    dynamic renewableFlag,
    dynamic userCategory,
    dynamic userType,
  );
  Future<Either<Failure, List<AidRequest>>> getLastAidRequestfileNo(
    String fileNumber,
  );
  Future<Either<Failure, List<AidRequest>>> getLastAidRequesmeterNo(
    String meterNo,
  );

  Future<Either<Failure, List<AidRequest>>> getAidRequestsForFileNumber(
    String fileNumber,
  );
}
