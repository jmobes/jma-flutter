import 'package:easy_localization/easy_localization.dart' hide TextDirection;
import 'package:flutter/material.dart';
import 'package:jma/config/custom_data_grid.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:jma/core/utils/key-lang.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class HistoryRequestAdapter extends DataGridSource {
  /// Creates the employee data source class with required details.
  HistoryRequestAdapter(List<AidRequest> aidRequest) {
    _historyRequestData = aidRequest.map<DataGridRow>((AidRequest aidRequest) {
      String supportTypeval;
      String renewableFlagval;
      String userCategoryval;
      String userTypeval;
      String isAddidval;
      if (aidRequest.supportType == 0) {
        supportTypeval = KeyLang.fullSupport.tr();
      } else if (aidRequest.supportType == 1) {
        supportTypeval = KeyLang.unstableSupport.tr();
      } else {
        supportTypeval = KeyLang.notFound.tr();
      }
      if (aidRequest.renewableFlag == 0) {
        renewableFlagval = KeyLang.renewableEnergy.tr();
      } else if (aidRequest.renewableFlag == 1) {
        renewableFlagval = KeyLang.nonrenewableEnergy.tr();
      } else {
        renewableFlagval = KeyLang.notFound.tr();
      }
      if (aidRequest.userCategory == 0) {
        userCategoryval = KeyLang.standardSubscription.tr();
      } else if (aidRequest.userCategory == 1) {
        userCategoryval = KeyLang.royalGenerosityAndaidfund.tr();
      } else if (aidRequest.userCategory == 2) {
        userCategoryval = KeyLang.marriedtoanonJordanian.tr();
      } else if (aidRequest.userCategory == 3) {
        userCategoryval = KeyLang.syrianRefugee.tr();
      } else {
        userCategoryval = KeyLang.notFound.tr();
      }
      if (aidRequest.userType == 0) {
        userTypeval = KeyLang.jordanian.tr();
      } else if (aidRequest.userType == 1) {
        userTypeval = KeyLang.temporaryPassports.tr();
      } else if (aidRequest.userType == 2) {
        userTypeval = KeyLang.gaza.tr();
      } else {
        userTypeval = KeyLang.notFound.tr();
      }
      if (aidRequest.isAided == true) {
        isAddidval = KeyLang.aidRequest.tr();
      } else {
        isAddidval = KeyLang.cancelAid.tr();
      }
      return DataGridRow(cells: <CustomDataGrid>[
        CustomDataGrid<String>(
            KeyLang.emrcNationalNumber.tr(), aidRequest.nationalNumber),
        CustomDataGrid<String>(
          KeyLang.fileNumber.tr(),
          aidRequest.fileNumber,
        ),
        CustomDataGrid<String>(
            KeyLang.meterNumber.tr(), aidRequest.meterNumber),
        CustomDataGrid<String>(KeyLang.firstName.tr(), aidRequest.firstName),
        CustomDataGrid<String>(KeyLang.secondName.tr(), aidRequest.secondName),
        CustomDataGrid<String>(KeyLang.thirdName.tr(), aidRequest.thirdName),
        CustomDataGrid<String>(KeyLang.lastName.tr(), aidRequest.lastName),
        CustomDataGrid<String>(
          KeyLang.emrcCustomerPhoneNumber.tr(),
          aidRequest.mobileNumber,
          isNumber: true,
        ),
        CustomDataGrid<String>(
          KeyLang.requestType.tr(),
          isAddidval,
        ),
        CustomDataGrid<String>(
          KeyLang.requestDate.tr(),
          aidRequest.requestDate,
        ),
        CustomDataGrid<String>(KeyLang.supportType.tr(), supportTypeval),
        CustomDataGrid<String>(KeyLang.renewableFlag.tr(), renewableFlagval),
        CustomDataGrid<String>(KeyLang.userCategory.tr(), userCategoryval),
        CustomDataGrid<String>(KeyLang.userType.tr(), userTypeval),
      ]);
    }).toList();
  }

  List<DataGridRow> _historyRequestData = <DataGridRow>[];

  @override
  List<DataGridRow> get rows => _historyRequestData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    Color getRowBackgroundColor() {
      final int i = effectiveRows.indexOf(row);

      if (i % 2 != 0) {
        return AppColors.littelblueTable;
      }

      return Colors.transparent;
    }

    return DataGridRowAdapter(
        color: getRowBackgroundColor(),
        cells: row.getCells().map<Widget>((DataGridCell cell) {
          Widget getCellWidget() {
            if ((cell as CustomDataGrid).isNumber) {
              return Directionality(
                textDirection: TextDirection.ltr,
                child: Text(cell.value.toString()),
              );
            } else {
              return Text(
                cell.value.toString(),
              );
            }
          }

          return Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(8.0),
            child: getCellWidget(),
          );
        }).toList());
  }
}
