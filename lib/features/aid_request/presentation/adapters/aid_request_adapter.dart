import 'package:easy_localization/easy_localization.dart' hide TextDirection;
import 'package:flutter/material.dart';
import 'package:jma/config/custom_data_grid.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:jma/core/utils/key-lang.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:jma/features/aid_request/presentation/history_request_page/history_page.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class AidRequestAdapter extends DataGridSource {
  /// Creates the employee data source class with required details.
  AidRequestAdapter(BuildContext context, List<AidRequest> aidRequest) {
    _aidRequestData = aidRequest.map<DataGridRow>((AidRequest aidRequest) {
      String supportTypeval;
      String renewableFlagval;

      String userCategoryval;
      String userTypeval;
      String isAddidval;
      if (aidRequest.supportType == 0) {
        supportTypeval = KeyLang.fullSupport.tr();
      } else if (aidRequest.supportType == 1) {
        supportTypeval = KeyLang.unstableSupport.tr();
      } else {
        supportTypeval = KeyLang.notFound.tr();
      }
      if (aidRequest.renewableFlag == 0) {
        renewableFlagval = KeyLang.renewableEnergy.tr();
      } else if (aidRequest.renewableFlag == 1) {
        renewableFlagval = KeyLang.nonrenewableEnergy.tr();
      } else {
        renewableFlagval = KeyLang.notFound.tr();
      }
      if (aidRequest.userCategory == 0) {
        userCategoryval = KeyLang.standardSubscription.tr();
      } else if (aidRequest.userCategory == 1) {
        userCategoryval = KeyLang.royalGenerosityAndaidfund.tr();
      } else if (aidRequest.userCategory == 2) {
        userCategoryval = KeyLang.marriedtoanonJordanian.tr();
      } else if (aidRequest.userCategory == 3) {
        userCategoryval = KeyLang.syrianRefugee.tr();
      } else {
        userCategoryval = KeyLang.notFound.tr();
      }
      if (aidRequest.userType == 0) {
        userTypeval = KeyLang.jordanian.tr();
      } else if (aidRequest.userType == 1) {
        userTypeval = KeyLang.temporaryPassports.tr();
      } else if (aidRequest.userType == 2) {
        userTypeval = KeyLang.gaza.tr();
      } else {
        userTypeval = KeyLang.notFound.tr();
      }
      if (aidRequest.isAided == true) {
        isAddidval = KeyLang.aidRequest.tr();
      } else {
        isAddidval = KeyLang.cancelAid.tr();
      }
      return DataGridRow(cells: <CustomDataGrid2>[
        CustomDataGrid2<String>(
            KeyLang.emrcNationalNumber.tr(), aidRequest.nationalNumber),
        CustomDataGrid2<String>(
          KeyLang.fileNumber.tr(),
          aidRequest.fileNumber,
        ),
        CustomDataGrid2<String>(
            KeyLang.meterNumber.tr(), aidRequest.meterNumber),
        CustomDataGrid2<String>(KeyLang.firstName.tr(), aidRequest.firstName),
        CustomDataGrid2<String>(KeyLang.secondName.tr(), aidRequest.secondName),
        CustomDataGrid2<String>(KeyLang.thirdName.tr(), aidRequest.thirdName),
        CustomDataGrid2<String>(KeyLang.lastName.tr(), aidRequest.lastName),
        CustomDataGrid2<String>(
          KeyLang.emrcCustomerPhoneNumber.tr(),
          aidRequest.mobileNumber,
          isNumber: true,
        ),
        CustomDataGrid2<String>(
          KeyLang.requestType.tr(),
          isAddidval,
        ),
        CustomDataGrid2<String>(
          KeyLang.requestDate.tr(),
          aidRequest.requestDate,
        ),
        CustomDataGrid2<String>(KeyLang.supportType.tr(), supportTypeval),
        CustomDataGrid2<String>(KeyLang.renewableFlag.tr(), renewableFlagval),
        CustomDataGrid2<String>(KeyLang.userCategory.tr(), userCategoryval),
        CustomDataGrid2<String>(KeyLang.userType.tr(), userTypeval),
        CustomDataGrid2<String>(KeyLang.history.tr(), KeyLang.history.tr(),
            isText: false, historyClick: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => HistoryRequestPage(
                fileNumber: aidRequest.fileNumber,
              ),
            ),
          );
        }),
      ]);
    }).toList();
  }

  List<DataGridRow> _aidRequestData = <DataGridRow>[];

  @override
  List<DataGridRow> get rows => _aidRequestData;

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    Color getRowBackgroundColor() {
      final int i = effectiveRows.indexOf(row);

      if (i % 2 != 0) {
        return AppColors.littelblueTable;
      }

      return Colors.transparent;
    }

    return DataGridRowAdapter(
        color: getRowBackgroundColor(),
        cells: row.getCells().map<Widget>((DataGridCell cell) {
          Widget getCellWidget() {
            if ((cell as CustomDataGrid2).isNumber && cell.isText) {
              return Directionality(
                textDirection: TextDirection.ltr,
                child: Text(cell.value.toString()),
              );
            } else {
              if (cell.isText) {
                return Text(
                  cell.value.toString(),
                );
              } else {
                return SizedBox(
                  child: TextButton(
                    onPressed: cell.historyClick,
                    child: Text(KeyLang.history.tr()),
                  ),
                );
              }
            }
          }

          return Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(8.0),
            child: getCellWidget(),
          );
        }).toList());
  }
}
