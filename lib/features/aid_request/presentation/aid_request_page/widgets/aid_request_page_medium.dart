import 'package:date_time_picker/date_time_picker.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:jma/core/presentation/constants/validtor.dart';
import 'package:jma/core/presentation/manager/export/cubit.dart';
import 'package:jma/core/presentation/manager/server_date_cubit.dart';
import 'package:jma/core/presentation/manager/server_date_state.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/core/utils/key-lang.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:jma/features/aid_request/presentation/adapters/aid_request_adapter.dart';
import 'package:jma/features/aid_request/presentation/manager/aid_request_cubit.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

import '../../../../../core/presentation/widgets/loading_view_with_text.dart';

class AidRequestMediumScreen extends StatefulWidget {
  const AidRequestMediumScreen({Key? key}) : super(key: key);

  @override
  State<AidRequestMediumScreen> createState() => _AidRequestMediumScreenState();
}

class Type {
  const Type(this.name, this.index);
  final String name;
  final int index;
}

class _AidRequestMediumScreenState extends State<AidRequestMediumScreen> {
  TextEditingController _fileNoController = new TextEditingController();
  TextEditingController _meterNoController = new TextEditingController();
  String? dateFrom;
  String? dateTo;
  int _Type = 1;
  bool checkedValue = false;
  late AidRequestAdapter _aidRequestAdapter;
  late TextEditingController _dateFrom;
  late TextEditingController _dateTo;
  GlobalKey<FormState> _keyAidRequestpage = GlobalKey<FormState>();
  GlobalKey<FormState> _keyFile = GlobalKey<FormState>();
  GlobalKey<FormState> _keyMeter = GlobalKey<FormState>();
  final DataGridController _controller = DataGridController();
  final GlobalKey<SfDataGridState> _key = GlobalKey<SfDataGridState>();
  String? _dropdownError;
  Type? selectedUser;
  int? selectedSupport;
  int? selectedRenewableFlag;
  int? selectedUserCategory;
  int? selectedUserType;
  bool _isVisible = false;
  void showRow() {
    _isVisible = !_isVisible;
  }

  String mm = KeyLang.fullSupport.tr();
  var supportType = <String>[
    'دعم كامل',
    'دعم غير ثابت',
  ];
  var renewableFlag = <String>[
    'طاقة متجددة',
    'ليس طاقة متجددة',
  ];
  var userCategory = <String>[
    'اشتراك عادي',
    'مكرمة ملكية و صندوق معونة',
    'متزوج من غير اردني ',
    'لاجئ سوري  ',
  ];
  var userType = <String>[
    'اردني ',
    'جوازات مؤقتة ',
    'غزة',
  ];
  final List<Type> _selectedType = <Type>[
    Type(KeyLang.all.tr(), 1),
    Type(KeyLang.aidRequest.tr(), 2),
    Type(KeyLang.cancelAid.tr(), 3),
  ];
  @override
  void initState() {
    super.initState();
    _dateFrom = TextEditingController();
    _dateTo = TextEditingController();
    context.read<ServerDateCubit>().getServerDate();
  }

  Widget getLoadingView() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Column(
      children: [
        Card(
          child: SizedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 20),
                      child: Text(KeyLang.searchThrough.tr()),
                    ),
                    Expanded(
                      child: ListTile(
                        title: Text(KeyLang.duringPeriod.tr()),
                        leading: Radio<int>(
                          value: 1,
                          groupValue: _Type,
                          onChanged: (int? value) {
                            setState(() {
                              _clearData();
                              _Type = value ?? 1;
                            });
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: ListTile(
                        title: Text(
                          KeyLang.fileNumber.tr(),
                        ),
                        leading: Radio<int>(
                          value: 2,
                          groupValue: _Type,
                          onChanged: (int? value) {
                            setState(() {
                              _clearData();
                              _Type = value ?? 1;
                            });
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: ListTile(
                        title: Text(
                          KeyLang.meterNumber.tr(),
                        ),
                        leading: Radio<int>(
                          value: 3,
                          groupValue: _Type,
                          onChanged: (int? value) {
                            setState(() {
                              _clearData();
                              _Type = value ?? 1;
                            });
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                if (_Type == 1)
                  BlocBuilder<ServerDateCubit, ServerDateState>(
                      builder: (context, state) {
                    if (state is ServerDateLoaded) {
                      return Form(
                        key: _keyAidRequestpage,
                        autovalidateMode: AutovalidateMode.always,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: width * .20,
                                  // height: height * .5,
                                  child: DateTimePicker(
                                    type: DateTimePickerType.date,
                                    dateMask: 'yyyy/MM/dd',
                                    controller: _dateFrom,
                                    onChanged: (String _) => setState(() {}),
                                    firstDate: DateTime(2000),
                                    lastDate: DateTime(2023),
                                    icon: const Icon(Icons.event),
                                    dateLabelText: KeyLang.fromDate.tr(),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return KeyLang.errordate.tr();
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                                SizedBox(
                                  width: width * .20,
                                  child: DateTimePicker(
                                    type: DateTimePickerType.date,
                                    dateMask: 'yyyy/MM/dd',
                                    controller: _dateTo,
                                    onChanged: (String _) => setState(() {}),
                                    // initialValue: dateC,
                                    firstDate: DateTime(
                                      2000,
                                    ),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return KeyLang.errordate.tr();
                                      }
                                      return null;
                                    },
                                    lastDate: DateTime(2023),
                                    icon: const Icon(Icons.event),
                                    dateLabelText: KeyLang.toDate.tr(),
                                  ),
                                ),
                                SizedBox(
                                  width: width * .20,
                                  // height: height * .15,
                                  child: DropdownButtonFormField<Type>(
                                    isExpanded: true,
                                    style: GoogleFonts.lato(fontSize: 15),
                                    decoration: InputDecoration(
                                      labelText: KeyLang.requestType.tr(),
                                      border: const OutlineInputBorder(),
                                    ),
                                    value: selectedUser,
                                    onChanged: (newValue) {
                                      setState(() {
                                        selectedUser = newValue;
                                      });
                                    },
                                    items: _selectedType.map((Type user) {
                                      return DropdownMenuItem(
                                        child: new Text(user.name),
                                        value: user,
                                      );
                                    }).toList(),
                                    validator: (_mySelectedType) =>
                                        _mySelectedType == null
                                            ? KeyLang.dropdownError.tr()
                                            : null,
                                  ),
                                ),
                                _dropdownError == null
                                    ? const SizedBox.shrink()
                                    : Text(
                                        _dropdownError ?? "",
                                        style:
                                            const TextStyle(color: Colors.red),
                                      ),
                              ],
                            ),
                            CheckboxListTile(
                              title: const Text("بحث متقدم"),
                              value: checkedValue,
                              onChanged: (newValue) {
                                setState(() {
                                  checkedValue = newValue!;
                                  showRow();
                                });
                              },
                              controlAffinity: ListTileControlAffinity
                                  .leading, //  <-- leading Checkbox
                            ),
                            Visibility(
                              visible: _isVisible,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  //supportType
                                  SizedBox(
                                    width: width * .15,
                                    // height: height * .15,
                                    child: DropdownButtonFormField<String>(
                                      isExpanded: true,
                                      decoration: InputDecoration(
                                        labelText: KeyLang.supportType.tr(),
                                        labelStyle: TextStyle(
                                          fontSize: 15,
                                        ),
                                        border: const OutlineInputBorder(),
                                      ),
                                      style: GoogleFonts.lato(fontSize: 15),
                                      value: selectedSupport == null
                                          ? null
                                          : supportType[selectedSupport!],
                                      onChanged: (newValue) {
                                        setState(() {
                                          selectedSupport =
                                              supportType.indexOf(newValue!);
                                        });
                                      },
                                      items: supportType.map((String value) {
                                        return DropdownMenuItem(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                  // renewable Flag
                                  SizedBox(
                                    width: width * .15,
                                    // height: height * .15,
                                    child: DropdownButtonFormField<String>(
                                      isExpanded: true,
                                      decoration: InputDecoration(
                                        labelText: KeyLang.renewableFlag.tr(),
                                        border: const OutlineInputBorder(),
                                        labelStyle: TextStyle(
                                          fontSize: 13,
                                        ),
                                      ),
                                      style: GoogleFonts.lato(fontSize: 15),
                                      value: selectedRenewableFlag == null
                                          ? null
                                          : renewableFlag[
                                              selectedRenewableFlag!],
                                      onChanged: (newValue) {
                                        setState(() {
                                          selectedRenewableFlag =
                                              renewableFlag.indexOf(newValue!);
                                        });
                                      },
                                      items: renewableFlag.map((String value) {
                                        return DropdownMenuItem(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                  // categoryType
                                  SizedBox(
                                    width: width * .15,
                                    // height: height * .15,
                                    child: DropdownButtonFormField<String>(
                                      isExpanded: true,
                                      decoration: InputDecoration(
                                        labelText: KeyLang.userCategory.tr(),
                                        border: const OutlineInputBorder(),
                                        labelStyle: TextStyle(
                                          fontSize: 13,
                                        ),
                                      ),
                                      style: GoogleFonts.lato(fontSize: 15),
                                      value: selectedUserCategory == null
                                          ? null
                                          : userCategory[selectedUserCategory!],
                                      onChanged: (newValue) {
                                        setState(() {
                                          selectedUserCategory =
                                              userCategory.indexOf(newValue!);
                                        });
                                      },
                                      items: userCategory.map((String value) {
                                        return DropdownMenuItem(
                                          value: value,
                                          child: Text(value,
                                              overflow: TextOverflow.visible),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                  //userType
                                  SizedBox(
                                    width: width * .15,
                                    // height: height * .15,
                                    child: DropdownButtonFormField<String>(
                                      isExpanded: true,
                                      decoration: InputDecoration(
                                        labelText: KeyLang.userType.tr(),
                                        border: const OutlineInputBorder(),
                                        labelStyle: TextStyle(
                                          fontSize: 13,
                                        ),
                                      ),
                                      style: GoogleFonts.lato(fontSize: 15),
                                      value: selectedUserType == null
                                          ? null
                                          : userType[selectedUserType!],
                                      onChanged: (newValue) {
                                        setState(() {
                                          selectedUserType =
                                              userType.indexOf(newValue!);
                                        });
                                      },
                                      items: userType.map((String value) {
                                        return DropdownMenuItem(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    } else if (state is ServerDateLoading) {
                      return loadingViewWithText(
                        'يتم الحصول على تاريخ اليوم',
                      );
                    } else if (state is ServerDateFailed) {
                      return Text(state.message);
                    } else {
                      return Container();
                    }
                  }),
                if (!_isDatesValid())
                  Center(
                    child: Text(
                      KeyLang.errorDateMore30.tr(),
                      style: const TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                const SizedBox(height: 15),
                if (_Type == 2)
                  Form(
                    key: _keyFile,
                    child: Center(
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * .20,
                        child: TextFormField(
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          controller: _fileNoController,
                          keyboardType: TextInputType.number,
                          validator: (value) => Validator.validateFileNo(
                            FileNo: value,
                          ),
                          decoration: InputDecoration(
                            labelText: KeyLang.fileNumber.tr(),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            contentPadding: const EdgeInsets.all(8),
                            errorBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: const BorderSide(
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                if (_Type == 3)
                  Form(
                    key: _keyMeter,
                    child: Center(
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * .20,
                        child: TextFormField(
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          controller: _meterNoController,
                          keyboardType: TextInputType.number,
                          validator: (value) => Validator.validateMeterNo(
                            MeterNo: value,
                          ),
                          decoration: InputDecoration(
                            labelText: KeyLang.meterNumber.tr(),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            contentPadding: const EdgeInsets.all(8),
                            errorBorder: UnderlineInputBorder(
                              borderRadius: BorderRadius.circular(6.0),
                              borderSide: const BorderSide(
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                Row(
                  children: [
                    const SizedBox(width: 10),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                        width: width * .10,
                        height: height * .05,
                        child: ElevatedButton(
                          onPressed: () async {
                            if (_Type == 1) {
                              if (_keyAidRequestpage.currentState!.validate() &&
                                  _isDatesValid()) {
                                context
                                    .read<AidRequestCubit>()
                                    .getLastAidRequests(
                                      _dateFrom.text,
                                      _dateTo.text,
                                      selectedUser!.index,
                                      selectedSupport,
                                      selectedRenewableFlag,
                                      selectedUserCategory,
                                      selectedUserType,
                                    );
                              }
                            } else if (_Type == 2 &&
                                _keyFile.currentState!.validate()) {
                              context
                                  .read<AidRequestCubit>()
                                  .getLastAidRequestsFileNo(
                                      _fileNoController.text);
                            } else if (_Type == 3 &&
                                _keyMeter.currentState!.validate()) {
                              context
                                  .read<AidRequestCubit>()
                                  .getLastAidRequestsmeterNo(
                                      _meterNoController.text);
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            primary: AppColors.buttonandtitle,
                          ),
                          child: Text(
                            KeyLang.search.tr(),
                            style: const TextStyle(
                                color: Colors.white, fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: width * .10,
                        height: height * .05,
                        child: ElevatedButton(
                          onPressed: () async {
                            _clearData();
                          },
                          style: ElevatedButton.styleFrom(
                            primary: AppColors.buttonandtitle,
                          ),
                          child: Text(
                            KeyLang.clear.tr(),
                            style: const TextStyle(
                                color: Colors.white, fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        BlocConsumer<AidRequestCubit, AidRequestState>(
          listener: (context, state) {
            if (state is AidRequestFailed) {
              showSnackBar(context, state.message);
            }
          },
          builder: (context, state) {
            return BlocBuilder<AidRequestCubit, AidRequestState>(
              buildWhen: (previous, current) {
                if (current is AidRequestLoaded ||
                    current is AidRequestLoading ||
                    current is AidRequestInitial) {
                  return true;
                } else {
                  return false;
                }
              },
              builder: (uContext, uState) {
                if (uState is AidRequestLoaded) {
                  _aidRequestAdapter =
                      AidRequestAdapter(context, uState.aidRequests);
                  return SizedBox(
                    height: MediaQuery.of(context).size.height * .65,
                    child: Card(
                        child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            margin: const EdgeInsets.all(5),
                            child: Align(
                              alignment: Alignment.topRight,
                              child: ElevatedButton.icon(
                                  onPressed: () {
                                    context
                                        .read<ExportCubit>()
                                        .exportDataGridToExcel(
                                            _key, 'AidRequest.xlsx');
                                  },
                                  icon: const Icon(Icons.send_to_mobile),
                                  label: Text(KeyLang.export.tr())),
                            ),
                          ),
                          Container(
                            constraints: const BoxConstraints(
                              minWidth: 100,
                              minHeight: 100,
                            ),
                            height: MediaQuery.of(context).size.height * .48,
                            child: getDataTable(uState.aidRequests),
                          ),
                          SfDataPager(
                            delegate: _aidRequestAdapter,
                            pageCount: (uState.aidRequests.length / 10)
                                .ceil()
                                .toDouble(),
                          ),
                        ],
                      ),
                    )),
                  );
                } else if (uState is AidRequestLoading) {
                  return getLoadingView();
                } else {
                  return Container();
                }
              },
            );
          },
        )
      ],
    );
  }

  Widget getDataTable(List<AidRequest> aidRequest) {
    return SfDataGridTheme(
      data: SfDataGridThemeData(
        headerColor: AppColors.headerTable,
      ),
      child: SfDataGrid(
        isScrollbarAlwaysShown: true,
        rowsPerPage: 10,
        key: _key,
        allowSorting: true,
        source: _aidRequestAdapter,
        defaultColumnWidth: 150,
        columns: getColumns(),
        columnWidthMode: ColumnWidthMode.none,
      ),
    );
  }

  List<GridColumn> getColumns() {
    return [
      getGridColumn(KeyLang.emrcNationalNumber.tr()),
      getGridColumn(KeyLang.fileNumber.tr()),
      getGridColumn(KeyLang.meterNumber.tr()),
      getGridColumn(KeyLang.firstName.tr()),
      getGridColumn(KeyLang.secondName.tr()),
      getGridColumn(KeyLang.thirdName.tr()),
      getGridColumn(KeyLang.lastName.tr()),
      getGridColumn(KeyLang.emrcCustomerPhoneNumber.tr()),
      getGridColumn(KeyLang.requestType.tr()),
      getGridColumn(KeyLang.requestDate.tr()),
      getGridColumn(KeyLang.supportType.tr()),
      getGridColumn(KeyLang.renewableFlag.tr()),
      getGridColumn(KeyLang.userCategory.tr()),
      getGridColumn(KeyLang.userType.tr()),
      getGridColumn(KeyLang.history.tr(), showExport: true),
    ];
  }

  GridColumn getGridColumn(String title,
      {bool showExport = false, Color tColor = Colors.white}) {
    return GridColumn(
        columnName: title,
        label: Center(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Center(
                  child: Text(
                    title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: tColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  _clearData() {
    setState(() {
      _dateFrom.clear();
      _dateTo.clear();
      _fileNoController.clear();
      _meterNoController.clear();
      selectedUser = null;
      context.read<AidRequestCubit>().clear();
    });
  }

  bool _isDatesValid() {
    if (_dateFrom.text.isNotEmpty && _dateTo.text.isNotEmpty) {
      final theDatesFrom = _dateFrom.text.split('-');
      final year = int.parse(theDatesFrom[0]);
      final month = int.parse(theDatesFrom[1]);
      final day = int.parse(theDatesFrom[2]);
      final DateTime dateFrom = DateTime(
        year,
        month,
        day,
      );

      final theDatesTo = _dateTo.text.split('-');
      final yearT = int.parse(theDatesTo[0]);
      final monthT = int.parse(theDatesTo[1]);
      final dayT = int.parse(theDatesTo[2]);
      final DateTime dateTo = DateTime(
        yearT,
        monthT,
        dayT,
      );

      final int difference = dateTo.difference(dateFrom).inDays.abs();
      if (difference > 30) {
        return false;
      }
    }

    return true;
  }
}
