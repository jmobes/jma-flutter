import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jma/core/presentation/constants/controllers.dart';
import 'package:jma/core/presentation/constants/custom_text.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';
import 'package:jma/features/aid_request/presentation/aid_request_page/widgets/aid_request_page_large.dart';
import 'package:jma/features/aid_request/presentation/aid_request_page/widgets/aid_request_page_medium.dart';
import 'package:jma/features/aid_request/presentation/aid_request_page/widgets/aid_request_page_small.dart';

class AidRequestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Obx(
          () => Row(
            children: [
              Container(
                  margin: EdgeInsets.only(
                    top: Responsive.isSmallScreen(context) ? 56 : 6,
                  ),
                  child: CustomText(
                    text: menuController.activeItem.value,
                    size: 20,
                    weight: FontWeight.bold,
                    color: Colors.blue,
                  )),
            ],
          ),
        ),
        Expanded(
            child: SingleChildScrollView(
          child: Column(
            children: [
              // if (Responsive.isLargeScreen(context) ||
              //     Responsive.isMediumScreen(context))
              //   if (Responsive.isCustomSize(context))
              //     const ReportFullMatchLargeScreen()
              //   else
              //     const ReportFullMatchLargeScreen()
              // else
              //   const ReportFullMatchSmallscreen()
              if (Responsive.isLargeScreen(context))
                const AidRequestLargeScreen(),
              if (Responsive.isMediumScreen(context))
                const AidRequestMediumScreen(),
              if (Responsive.isSmallScreen(context))
                const AidRequestSmallScreen()
              // if (!Responsive.isSmallScreen(context))
              //   mm()
              // else
              //   StatusLargeScreen(),
            ],
          ),
        ))
      ],
    );
  }
}
