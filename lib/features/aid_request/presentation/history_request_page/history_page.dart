import 'package:flutter/material.dart';
import 'package:jma/core/presentation/constants/custom_text.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';
import 'package:jma/features/aid_request/presentation/history_request_page/widgets/history_page_large.dart';
import 'package:jma/features/aid_request/presentation/history_request_page/widgets/history_page_medium.dart';
import 'package:jma/features/aid_request/presentation/history_request_page/widgets/history_page_small.dart';

class HistoryRequestPage extends StatefulWidget {
  const HistoryRequestPage({Key? key, required this.fileNumber})
      : super(key: key);
  final String fileNumber;
  @override
  State<HistoryRequestPage> createState() => _HistoryRequestPageState();
}

class _HistoryRequestPageState extends State<HistoryRequestPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
                margin: EdgeInsets.only(
                  top: Responsive.isSmallScreen(context) ? 56 : 6,
                ),
                child: const CustomText(
                  text: 'السجل',
                  size: 20,
                  weight: FontWeight.bold,
                  color: Colors.blue,
                )),
          ],
        ),
        Expanded(
            child: SingleChildScrollView(
          child: Column(
            children: [
              // if (Responsive.isLargeScreen(context) ||
              //     Responsive.isMediumScreen(context))
              //   if (Responsive.isCustomSize(context))
              //     const ReportFullMatchLargeScreen()
              //   else
              //     const ReportFullMatchLargeScreen()
              // else
              //   const ReportFullMatchSmallscreen()
              if (Responsive.isLargeScreen(context))
                HistoryLargeScreen(
                  fileNumber: widget.fileNumber,
                ),
              if (Responsive.isMediumScreen(context))
                HistoryMediumScreen(
                  fileNumber: widget.fileNumber,
                ),
              if (Responsive.isSmallScreen(context))
                HistorySmallScreen(
                  fileNumber: widget.fileNumber,
                )
              // if (!Responsive.isSmallScreen(context))
              //   mm()
              // else
              //   StatusLargeScreen(),
            ],
          ),
        ))
      ],
    );
  }
}
