import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:jma/core/presentation/manager/export/cubit.dart';
import 'package:jma/core/utils/key-lang.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:jma/features/aid_request/presentation/adapters/history_request_adapter.dart';
import 'package:jma/features/aid_request/presentation/manager/aid_request_cubit.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class HistoryLargeScreen extends StatefulWidget {
  const HistoryLargeScreen({Key? key, required this.fileNumber})
      : super(key: key);
  final String fileNumber;
  @override
  State<HistoryLargeScreen> createState() => _HistoryLargeScreenState();
}

Widget getLoadingView() {
  return Center(
    child: CircularProgressIndicator(),
  );
}

class _HistoryLargeScreenState extends State<HistoryLargeScreen> {
  @override
  void initState() {
    super.initState();

    context.read<AidRequestCubit>().getAidRequests(widget.fileNumber);
  }

  final GlobalKey<SfDataGridState> _key = GlobalKey<SfDataGridState>();
  late HistoryRequestAdapter _historyRequestAdapter;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'السجل لرقم الملف :${widget.fileNumber}',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: AppColors.buttonandtitle),
        ),
        BlocBuilder<AidRequestCubit, AidRequestState>(
          builder: (uContext, uState) {
            if (uState is HistoryRequestLoaded) {
              _historyRequestAdapter =
                  HistoryRequestAdapter(uState.aidRequests);
              return Center(
                child: Card(
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height * .60,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: const EdgeInsets.all(5),
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: ElevatedButton.icon(
                                      onPressed: () {
                                        context
                                            .read<ExportCubit>()
                                            .exportDataGridToExcel(
                                                _key, 'history.xlsx');
                                      },
                                      icon: const Icon(Icons.send_to_mobile),
                                      label: Text(KeyLang.export.tr())),
                                ),
                              ),
                              buttonBack(),
                            ],
                          ),
                          Container(
                            constraints: const BoxConstraints(
                              minWidth: 100,
                              minHeight: 100,
                            ),
                            height: MediaQuery.of(context).size.height * .40,
                            child: getDataTable(uState.aidRequests),
                          ),
                          SfDataPager(
                            delegate: _historyRequestAdapter,
                            pageCount: (uState.aidRequests.length / 10)
                                .ceil()
                                .toDouble(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            } else if (uState is HistoryRequestLoading) {
              return getLoadingView();
            } else {
              return Container();
            }
          },
        )
      ],
    );
  }

  Widget getDataTable(List<AidRequest> aidRequest) {
    return SfDataGridTheme(
      data: SfDataGridThemeData(
        headerColor: AppColors.headerTable,
      ),
      child: SfDataGrid(
        isScrollbarAlwaysShown: true,
        rowsPerPage: 10,
        key: _key,
        allowSorting: true,
        source: _historyRequestAdapter,
        columns: getColumns(),
        columnWidthMode: ColumnWidthMode.auto,
      ),
    );
  }

  Widget buttonBack() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        margin: const EdgeInsets.all(5),
        child: ElevatedButton(
          onPressed: () async {
            Navigator.pop(context);
          },
          child: Text(
            KeyLang.back.tr(),
          ),
        ),
      ),
    );
  }

  List<GridColumn> getColumns() {
    return [
      getGridColumn(KeyLang.emrcNationalNumber.tr()),
      getGridColumn(KeyLang.fileNumber.tr()),
      getGridColumn(KeyLang.meterNumber.tr()),
      getGridColumn(KeyLang.firstName.tr()),
      getGridColumn(KeyLang.secondName.tr()),
      getGridColumn(KeyLang.thirdName.tr()),
      getGridColumn(KeyLang.lastName.tr()),
      getGridColumn(KeyLang.emrcCustomerPhoneNumber.tr()),
      getGridColumn(KeyLang.requestType.tr()),
      getGridColumn(KeyLang.requestDate.tr()),
      getGridColumn(KeyLang.supportType.tr()),
      getGridColumn(KeyLang.renewableFlag.tr()),
      getGridColumn(KeyLang.userCategory.tr()),
      getGridColumn(KeyLang.userType.tr(), showExport: true),
    ];
  }

  GridColumn getGridColumn(String title,
      {bool showExport = false, Color tColor = Colors.white}) {
    return GridColumn(
      columnName: title,
      label: Center(
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Center(
                child: Text(
                  title,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: tColor,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
