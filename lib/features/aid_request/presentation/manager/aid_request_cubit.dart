import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:jma/features/aid_request/domain/use_cases/get_aid_requests_file_use_case.dart';
import 'package:jma/features/aid_request/domain/use_cases/get_aid_requests_meter_use_case.dart';
import 'package:jma/features/aid_request/domain/use_cases/get_aid_requests_use_case.dart';
import 'package:jma/features/aid_request/domain/use_cases/get_last_aid_requests_use_case.dart';

part 'aid_request_state.dart';

class AidRequestCubit extends Cubit<AidRequestState> {
  final GetLastAidRequestsUseCase getLastAidRequestsUseCase;
  final GetAidRequestsUseCase getAidRequestsUseCase;
  final GetLastAidRequestsFileNoUseCase getAidRequestsUseFileNoCase;
  final GetLastAidRequestsMeterNoUseCase getAidRequestsUseMeterNoCase;
  AidRequestCubit({
    required this.getLastAidRequestsUseCase,
    required this.getAidRequestsUseCase,
    required this.getAidRequestsUseFileNoCase,
    required this.getAidRequestsUseMeterNoCase,
  }) : super(AidRequestInitial());

  Future<void> getLastAidRequests(
      String from,
      String to,
      int type,
      dynamic supportType,
      dynamic renewableFlag,
      dynamic userCategory,
      dynamic userType) async {
    emit(AidRequestLoading());

    final result = await getLastAidRequestsUseCase(GLParams(
        from: from,
        to: to,
        type: type,
        supportType: supportType,
        renewableFlag: renewableFlag,
        userCategory: userCategory,
        userType: userType));

    result.fold(
      (l) => emit(AidRequestFailed(l.message)),
      (r) => emit(AidRequestLoaded(r)),
    );
  }

  Future<void> getLastAidRequestsFileNo(String fileNo) async {
    emit(AidRequestLoading());

    final result =
        await getAidRequestsUseFileNoCase(GLParamsFile(fileNo: fileNo));

    result.fold(
      (l) => emit(AidRequestFailed(l.message)),
      (r) => emit(AidRequestLoaded(r)),
    );
  }

  Future<void> getLastAidRequestsmeterNo(String meterNo) async {
    emit(AidRequestLoading());

    final result =
        await getAidRequestsUseMeterNoCase(GLParamsMeter(meterNo: meterNo));

    result.fold(
      (l) => emit(AidRequestFailed(l.message)),
      (r) => emit(AidRequestLoaded(r)),
    );
  }

  Future<void> getAidRequests(String fileNumber) async {
    emit(HistoryRequestLoading());

    final result = await getAidRequestsUseCase(Params(fileNumber: fileNumber));

    result.fold(
      (l) => emit(AidRequestFailed(l.message)),
      (r) => emit(HistoryRequestLoaded(r)),
    );
  }

  void clear() {
    emit(AidRequestInitial());
  }
}
