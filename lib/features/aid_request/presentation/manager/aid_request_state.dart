part of 'aid_request_cubit.dart';

abstract class AidRequestState extends Equatable {
  const AidRequestState();
}

class AidRequestInitial extends AidRequestState {
  @override
  List<Object> get props => [];
}

class AidRequestLoading extends AidRequestState {
  @override
  List<Object> get props => [];
}

class AidRequestLoaded extends AidRequestState {
  final List<AidRequest> aidRequests;

  const AidRequestLoaded(this.aidRequests);

  @override
  List<Object> get props => [aidRequests];
}

class AidRequestFailed extends AidRequestState {
  final String message;

  const AidRequestFailed(this.message);

  @override
  List<Object> get props => [];
}

class HistoryRequestLoaded extends AidRequestState {
  final List<AidRequest> aidRequests;

  const HistoryRequestLoaded(this.aidRequests);

  @override
  List<Object> get props => [aidRequests];
}

class HistoryRequestLoading extends AidRequestState {
  @override
  List<Object> get props => [];
}
