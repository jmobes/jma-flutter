import 'package:jma/features/aid_request/domain/entities/aid_request.dart';

class AidRequestModel extends AidRequest {
  AidRequestModel({
    required int id,
    required String nationalNumber,
    required String fileNumber,
    required String meterNumber,
    required String firstName,
    required String secondName,
    required String thirdName,
    required String lastName,
    required String mobileNumber,
    required bool isAided,
    required String requestDate,
    required dynamic supportType,
    required dynamic renewableFlag,
    required dynamic userCategory,
    required dynamic userType,
  }) : super(
          id: id,
          nationalNumber: nationalNumber,
          fileNumber: fileNumber,
          meterNumber: meterNumber,
          firstName: firstName,
          secondName: secondName,
          thirdName: thirdName,
          lastName: lastName,
          mobileNumber: mobileNumber,
          isAided: isAided,
          requestDate: requestDate,
          supportType: supportType,
          renewableFlag: renewableFlag,
          userCategory: userCategory,
          userType: userType,
        );

  factory AidRequestModel.fromJson(Map<String, dynamic> json) {
    return AidRequestModel(
      id: json['id'] as int,
      nationalNumber: json['national_Number'] as String,
      fileNumber: json['fileNumber'] as String,
      meterNumber: json['meterNumber'] as String,
      firstName: json['first_Name'] as String,
      secondName: json['second_Name'] as String,
      thirdName: json['third_Name'] as String,
      lastName: json['family_Name'] as String,
      mobileNumber: json['customer_MobileNumber'] as String,
      isAided: json['deserveEnergyAid'] as bool,
      supportType: json['supportType'] as dynamic,
      renewableFlag: json['renewableFlag'] as dynamic,
      userCategory: json['userCategory'] as dynamic,
      userType: json['userType'] as dynamic,
      requestDate: (json['createdDate'] as String).split('T')[0],
    );
  }
}

/*/*{
"id":1075657,
"national_Number":"9691013982",
"fileNumber":"0110708688773",
"meterNumber":"2007100163",
"first_Name":"سعود",
"second_Name":"ماجد",
"third_Name":"سعود",
"family_Name":"غيث ",
"customer_MobileNumber":"07 8572 0760",
"deserveEnergyAid":true,
"createdDate":"2022-01-30T00:00:00"
}
*/*/
