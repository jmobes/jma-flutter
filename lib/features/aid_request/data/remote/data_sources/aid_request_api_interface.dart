import 'package:dio/dio.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/utils/constants.dart';
import 'package:retrofit/http.dart';

part 'aid_request_api_interface.g.dart';

@RestApi(baseUrl: kBaseUrl)
abstract class AidRequestApiInterface {
  factory AidRequestApiInterface(Dio dio) {
    return _AidRequestApiInterface(dio);
  }

  @POST(kGetLastAidRequestsEndpoint)
  Future<APIResponse> getLastAidRequests(@Body() Map<String, dynamic> map);

  @POST(kGetAidRequestsEndpoint)
  Future<APIResponse> getAidRequests(@Body() Map<String, dynamic> map);
}
