import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/data/remote/resource/app_api_res.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/aid_request/data/remote/data_sources/aid_request_api_interface.dart';

abstract class AidRequestRemoteDataSource {
  Future<APPApiResponse> getLastAidRequests(
      String from,
      String to,
      int type,
      dynamic supportType,
      dynamic renewableFlag,
      dynamic userCategory,
      dynamic userType);
  Future<APPApiResponse> getLastAidRequestsfileNumber(String fileNo);
  Future<APPApiResponse> getLastAidRequestsmeterNo(String meterNo);
  Future<APPApiResponse> getAidRequests(String fileNumber);
}

class AidRequestRemoteDataSourceImpl implements AidRequestRemoteDataSource {
  final AidRequestApiInterface apiInterface;

  AidRequestRemoteDataSourceImpl({
    required this.apiInterface,
  });

  @override
  Future<APPApiResponse> getAidRequests(String fileNumber) async {
    return getAppApiResponse<APIResponse>(
      apiInterface.getAidRequests(
        GetAidRequestParams(
          fileNumber: fileNumber,
          languageId: 'AR',
        ).toJson(),
      ),
    );
  }

  @override
  Future<APPApiResponse> getLastAidRequests(
    String from,
    String to,
    int type,
    dynamic supportType,
    dynamic renewableFlag,
    dynamic userCategory,
    dynamic userType,
  ) async {
    return getAppApiResponse<APIResponse>(
      apiInterface.getLastAidRequests(
        GetLastAidRequestParams(
          from: from,
          to: to,
          type: type,
          supportType: supportType,
          renewableFlag: renewableFlag,
          userCategory: userCategory,
          userType: userType,
          languageId: 'AR',
        ).toJson(),
      ),
    );
  }

  @override
  Future<APPApiResponse> getLastAidRequestsfileNumber(
    String fileNumber,
  ) async {
    return getAppApiResponse<APIResponse>(
      apiInterface.getLastAidRequests(
        GetAidRequestfileNoParams(
          fileNumber: fileNumber,
          languageId: 'AR',
        ).toJson(),
      ),
    );
  }

  @override
  Future<APPApiResponse> getLastAidRequestsmeterNo(
    String meterNo,
  ) async {
    return getAppApiResponse<APIResponse>(
      apiInterface.getLastAidRequests(
        GetAidRequestParamsmeterNo(
          meterNo: meterNo,
          languageId: 'AR',
        ).toJson(),
      ),
    );
  }
}

class GetLastAidRequestParams {
  final String from;
  final String to;
  final int type;
  final dynamic supportType;
  final dynamic renewableFlag;
  final dynamic userCategory;
  final dynamic userType;

  final String languageId;

  const GetLastAidRequestParams({
    required this.from,
    required this.to,
    required this.type,
    required this.supportType,
    required this.renewableFlag,
    required this.userCategory,
    required this.userType,
    required this.languageId,
  });

  Map<String, dynamic> toJson() {
    return {
      'AidRequestDateFrom': from,
      'AidRequestDateTo': to,
      'RequestsType': type,
      'SupportType': supportType,
      'RenewableFlag': renewableFlag,
      'UserCategory': userCategory,
      'UserType': userType,
      'LanguageId': languageId,
    };
  }
/*{
    "LanguageId": "AR",
   "AidRequestDateFrom":"2022-01-01",
    "AidRequestDateTo":"2022-01-30",
    "RequestsType":1
}*/
}

class GetAidRequestParams {
  final String fileNumber;
  final String languageId;

  const GetAidRequestParams({
    required this.fileNumber,
    required this.languageId,
  });

  Map<String, dynamic> toJson() {
    return {
      'FileNumber': fileNumber,
      'LanguageId': languageId,
    };
  }

/*{
    "LanguageId": "AR",
   "FileNumber":"2022-01-01",
}*/
}

class GetAidRequestfileNoParams {
  final String fileNumber;
  final String languageId;

  const GetAidRequestfileNoParams({
    required this.fileNumber,
    required this.languageId,
  });

  Map<String, dynamic> toJson() {
    return {
      'FileNumber': fileNumber,
      'LanguageId': languageId,
    };
  }
}

class GetAidRequestParamsmeterNo {
  final String meterNo;
  final String languageId;

  const GetAidRequestParamsmeterNo({
    required this.meterNo,
    required this.languageId,
  });

  Map<String, dynamic> toJson() {
    return {
      'MeterNo': meterNo,
      'LanguageId': languageId,
    };
  }
}
