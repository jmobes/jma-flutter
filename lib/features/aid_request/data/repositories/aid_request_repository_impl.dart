import 'package:dartz/dartz.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/utils/helpers.dart';
import 'package:jma/features/aid_request/data/remote/data_sources/aid_request_remote_data_soruce.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:jma/features/aid_request/domain/repositories/aid_request_repository.dart';

class AidRequestRepositoryImpl implements AidRequestRepository {
  final AidRequestRemoteDataSource aidRequestRemoteDataSource;

  const AidRequestRepositoryImpl({
    required this.aidRequestRemoteDataSource,
  });

  @override
  Future<Either<Failure, List<AidRequest>>> getAidRequestsForFileNumber(
      String fileNumber) {
    return getRepositoryResponse<List<AidRequest>>(
      aidRequestRemoteDataSource.getAidRequests(fileNumber),
      // networkInfo,
    );
  }

  @override
  Future<Either<Failure, List<AidRequest>>> getLastAidRequestfileNo(
      String fileNumber) {
    return getRepositoryResponse<List<AidRequest>>(
      aidRequestRemoteDataSource.getLastAidRequestsfileNumber(fileNumber),
      // networkInfo,
    );
  }

  @override
  Future<Either<Failure, List<AidRequest>>> getLastAidRequesmeterNo(
      String meterNo) {
    return getRepositoryResponse<List<AidRequest>>(
      aidRequestRemoteDataSource.getLastAidRequestsmeterNo(meterNo),
      // networkInfo,
    );
  }

  @override
  Future<Either<Failure, List<AidRequest>>> getLastAidRequests(
    String from,
    String to,
    int type,
    dynamic supportType,
    dynamic renewableFlag,
    dynamic userCategory,
    dynamic userType,
  ) {
    return getRepositoryResponse<List<AidRequest>>(
      aidRequestRemoteDataSource.getLastAidRequests(
          from, to, type, supportType, renewableFlag, userCategory, userType),
      // networkInfo,
    );
  }
}
