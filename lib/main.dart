import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jma/config/config-lang.dart';
import 'package:jma/core/presentation/controllers/menu_controller.dart';
import 'package:jma/core/presentation/controllers/navigation_controller.dart';
import 'package:jma/core/presentation/manager/export/cubit.dart';
import 'package:jma/core/presentation/manager/server_date_cubit.dart';
import 'package:jma/core/presentation/page/layout.dart';
import 'package:jma/core/presentation/routing/routes.dart';
import 'package:jma/core/utils/app_bloc_observer.dart';
import 'package:jma/di/injector.dart' as di;
import 'package:jma/features/aid_request/presentation/manager/aid_request_cubit.dart';
import 'package:jma/features/auth/presentation/login_page/login.dart';
import 'package:jma/features/auth/presentation/manager/auth_cubit.dart';
import 'package:jma/features/customer_comparison/presentation/manager/customer_comparison_cubit.dart';
import 'package:jma/features/dashboard/presentation/manager/dashboard_cubit.dart';
import 'package:jma/features/summary/presentation/manager/date_cubit.dart';
import 'package:jma/features/summary/presentation/manager/summary_cubit.dart';
import 'package:syncfusion_localizations/syncfusion_localizations.dart';

import 'features/customer_comparison/presentation/manager/export_all_cubit.dart';

Future<void> main() async {
  Get.put(MenuController());
  Get.put(NavigationController());
  await di.init();
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  BlocOverrides.runZoned(
    () => runApp(
      EasyLocalization(
        supportedLocales: [ConfigLanguage.EN_LOCALE, ConfigLanguage.AR_LOCALE],
        path: ConfigLanguage.LANG_PATH,
        // fallbackLocale: ConfigLanguage.AR_LOCALE,
        startLocale: ConfigLanguage.AR_LOCALE,
        child: MultiBlocProvider(
          providers: [
            BlocProvider<AuthCubit>(
              create: (_) => di.sl<AuthCubit>(),
            ),
            BlocProvider<DashboardCubit>(
              create: (_) => di.sl<DashboardCubit>(),
            ),
            BlocProvider<AidRequestCubit>(
              create: (_) => di.sl<AidRequestCubit>(),
            ),
            BlocProvider<CustomerComparisonCubit>(
              create: (_) => di.sl<CustomerComparisonCubit>(),
            ),
            BlocProvider<SummaryCubit>(
              create: (_) => di.sl<SummaryCubit>(),
            ),
            BlocProvider<DateCubit>(
              create: (_) => di.sl<DateCubit>(),
            ),
            BlocProvider<ServerDateCubit>(
              create: (_) => di.sl<ServerDateCubit>(),
            ),
            BlocProvider<ExportCubit>(
              create: (_) => di.sl<ExportCubit>(),
            ),
            BlocProvider(
              create: (_) => di.sl<ExportAllCubit>(),
            ),
          ],
          child: const MyApp(),
        ),
      ),
    ),
    blocObserver: AppBlocObserver(),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    final List<LocalizationsDelegate<dynamic>> count = [];
    for (int i = 0; i < context.localizationDelegates.length; i++) {
      count.add(context.localizationDelegates[i]);
    }
    count.add(SfGlobalLocalizations.delegate);
    return GetMaterialApp(
      title: 'Energy Requisition Support Portal',
      locale: context.locale,
      supportedLocales: context.supportedLocales,
      localizationsDelegates: count,
      debugShowCheckedModeBanner: false,
      initialRoute: authenticationPageRoute,
      builder: (context, widget) {
        final lang = Localizations.localeOf(context).languageCode;
        return Theme(
          data: ThemeData(
            primarySwatch: Colors.blue,
            textTheme: lang == 'en'
                ? GoogleFonts.latoTextTheme(
                    Theme.of(context).textTheme,
                  )
                : GoogleFonts.cairoTextTheme(Theme.of(context).textTheme),
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          child: widget!,
        );
      },
      getPages: [
        GetPage(
          name: rootRoute,
          page: () {
            return SiteLayout();
          },
        ),
        GetPage(name: authenticationPageRoute, page: () => Login()),
      ],
    );
  }
}
