class ChartData {
  String date;
  List<int> values;

  ChartData(this.date, this.values);
}
