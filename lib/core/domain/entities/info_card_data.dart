import 'package:flutter/material.dart';

class InfoCardData {
  final String title;
  final int value;
  final double? percentage;
  final Color? percentageColor;

  const InfoCardData(this.title, this.value,
      {this.percentage, this.percentageColor});
}
