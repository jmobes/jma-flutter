import 'package:dartz/dartz.dart';
import 'package:jma/core/domain/entities/server_date.dart';
import 'package:jma/core/error/failure.dart';

abstract class ServerDateRepository {
  Future<Either<Failure, ServerDate>> getServerDate();
}
