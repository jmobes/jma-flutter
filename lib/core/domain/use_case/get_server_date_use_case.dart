import 'package:dartz/dartz.dart';
import 'package:jma/core/domain/entities/server_date.dart';
import 'package:jma/core/domain/repositories/server_date_repository.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/usecases/use_case.dart';
import 'package:jma/core/utils/helpers.dart';

class GetServerDateUseCase implements UseCase<ServerDate, NoParams> {
  final ServerDateRepository repository;

  const GetServerDateUseCase(this.repository);

  @override
  Future<Either<Failure, ServerDate>> call(NoParams params) {
    return repository.getServerDate();
  }
}
