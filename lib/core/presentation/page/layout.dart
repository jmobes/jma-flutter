import 'package:flutter/material.dart';
import 'package:jma/core/presentation/Menu/custom_screen.dart';
import 'package:jma/core/presentation/Menu/large_screen.dart';
import 'package:jma/core/presentation/Menu/meduim_screen.dart';
import 'package:jma/core/presentation/Menu/side_menu.dart';
import 'package:jma/core/presentation/Menu/top_nav.dart';
import 'package:jma/core/presentation/widgets/localnav.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';
import 'package:jma/core/utils/constants.dart';

class SiteLayout extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      extendBodyBehindAppBar: true,
      appBar: topNavigationBar(context, scaffoldKey),
      drawer: const Drawer(
        child: SideMenu(),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(kBackground),
            fit: BoxFit.cover,
          ),
        ),
        child: Responsive(
          largeScreen: const LargeScreen(),
          smallScreen: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: localNavigator(),
          ),
          mediumScreen: const MediumScreen(),
          customScreen: const CustomScreen(),
        ),
      ),
    );
  }
}
