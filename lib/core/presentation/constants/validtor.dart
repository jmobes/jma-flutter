// ignore_for_file: avoid_classes_with_only_static_members

import 'package:easy_localization/easy_localization.dart';
import 'package:jma/core/utils/key-lang.dart';

class Validator {
  static String? validateName({required String? name}) {
    if (name == null) {
      return null;
    }

    if (name.isEmpty) {
      return KeyLang.errorusername.tr();
    }

    return null;
  }

  static String? validateFileNo({required String? FileNo}) {
    if (FileNo == null) {
      return null;
    }

    if (FileNo.isEmpty) {
      return KeyLang.errorFileNo.tr();
    } else if (FileNo.length > 13) {
      return "رقم الملف مكون من 13 رقم فقط";
    }

    return null;
  }

  static String? validateMeterNo({required String? MeterNo}) {
    if (MeterNo == null) {
      return null;
    }

    if (MeterNo.isEmpty) {
      return KeyLang.errorMeterNo.tr();
    }

    return null;
  }

  static validatemulit({required List? mulit}) {
    if (mulit == null) {
      return 'Choose';
    }

    return null;
  }

  static String? validatePassword({required String? password}) {
    if (password == null) {
      return null;
    }

    if (password.isEmpty) {
      return KeyLang.errorpassword.tr();
    } else if (password.length < 3) {
      return KeyLang.errorpassword2.tr();
    }

    return null;
  }
}
