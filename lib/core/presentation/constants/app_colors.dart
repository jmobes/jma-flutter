import 'package:flutter/material.dart';

class AppColors {
  // #ffffff
  static Color _colorFromHex(String hexColor) {
    String convert = hexColor.replaceAll("#", ''); // ffffff
    return Color(int.parse('FF$convert', radix: 16)); // 0xffffffff
  }

  // * color App

  static Color buttonandtitle = _colorFromHex('#127ABD');
  static Color subtitle = _colorFromHex('#858685');
  static Color littelblue = _colorFromHex('#EFF6FB');
  static Color littelblueTable = _colorFromHex('#eff0fb');
  static Color headerTable = _colorFromHex('#2398D7');
}
