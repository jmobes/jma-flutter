import 'package:flutter/material.dart';
import 'package:jma/core/presentation/routing/routes.dart';
import 'package:jma/features/aid_request/presentation/aid_request_page/aid_request_page.dart';
import 'package:jma/features/customer_comparison/presentation/report_full_match/report_full_match_page.dart';
import 'package:jma/features/customer_comparison/presentation/report_not_match/report_not_match_page.dart';
import 'package:jma/features/customer_comparison/presentation/report_simi_match/report_simi_match_page.dart';
import 'package:jma/features/dashboard/presentation/overview.dart';
import 'package:jma/features/summary/presentation/pages/provinces_summary_page.dart';

import '../../../features/summary/presentation/pages/summary_page.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case ordersPageRoute:
      return _getPageRoute(OverviewPage());
    case aidRequestPageRoute:
      return _getPageRoute(AidRequestPage());
    case reportFullPageRoute:
      return _getPageRoute(ReportFullMatch());
    case reportsimiPageRoute:
      return _getPageRoute(ReportSimiMatch());
    case reportnotmatchPageRoute:
      return _getPageRoute(ReportNotMatch());
    case summaryPageRoute:
      return _getPageRoute(SummaryPage());
    case provincesSummaryPageRoute:
      return _getPageRoute(ProvincesSummaryPage());
    default:
      return _getPageRoute(OverviewPage());
  }
}

PageRoute _getPageRoute(Widget child) {
  return MaterialPageRoute(builder: (context) => child);
}
