const rootRoute = "/";

const dashboardPageDisplayName = "الرئيسية";
const ordersPageRoute = "/dashboard";

const aidRequestPageDisplayName = "شاشة طلبات الدعم";
const aidRequestPageRoute = "/aid";

const reportFullPageDisplayName = "التقرير المطابق";
const reportFullPageRoute = "/reportF";

const reportsimiPageDisplayName = "التقرير شبه المطابق";
const reportsimiPageRoute = "/reportS";

const reportnotmatchPageDisplayName = "التقرير الغير المطابق";
const reportnotmatchPageRoute = "/reportN";

const summaryPageDisplayName = " ملخص أعداد الطلبات";
const summaryPageRoute = "/summary";

const provincesSummaryPageDisplayName = " ملخص المحافظات";
const provincesSummaryPageRoute = "/provincesSummary";

const authenticationPageDisplayName = "تسجيل الخروج";
const authenticationPageRoute = "/auth";

class MenuItem {
  final String name;
  final String route;

  MenuItem(this.name, this.route);
}

List<MenuItem> sideMenuItemRoutes = [
  MenuItem(dashboardPageDisplayName, ordersPageRoute),
  MenuItem(aidRequestPageDisplayName, aidRequestPageRoute),
  MenuItem(reportFullPageDisplayName, reportFullPageRoute),
  MenuItem(reportsimiPageDisplayName, reportsimiPageRoute),
  MenuItem(reportnotmatchPageDisplayName, reportnotmatchPageRoute),
  MenuItem(summaryPageDisplayName, summaryPageRoute),
  MenuItem(provincesSummaryPageDisplayName, provincesSummaryPageRoute),
  MenuItem(authenticationPageDisplayName, authenticationPageRoute),
];
