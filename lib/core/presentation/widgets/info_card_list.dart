import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:jma/core/domain/entities/info_card_data.dart';
import 'package:jma/core/presentation/constants/style.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';
import 'package:jma/core/utils/extensions.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class InfoCardList extends StatelessWidget {
  final List<InfoCardData> infoCardData;
  final Color topColor;

  const InfoCardList({
    Key? key,
    required this.infoCardData,
    required this.topColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.only(left: 5, right: 5),
        height: Responsive.isSmallScreen(context) ? 400 : 300,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white60,
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 6),
              color: lightGrey.withOpacity(.1),
              blurRadius: 12,
            )
          ],
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    color: topColor,
                    height: 5,
                  ),
                )
              ],
            ),
            Expanded(child: Container()),
            ...infoCardData.mapIndexed(
              (i, e) => Container(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text(
                        e.title,
                        style: TextStyle(
                          fontSize: 16,
                          color: lightGrey,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        e.value.toString(),
                        style: TextStyle(
                          fontSize: 16,
                          color: dark,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    if (e.percentage != null)
                      CircularPercentIndicator(
                        radius: 35.0,
                        lineWidth: 6.0,
                        percent: e.percentage! / 100,
                        center: Text('${e.percentage!.toPrecision(2)} %'),
                        progressColor: e.percentageColor,
                      ),
                  ],
                ),
              ),
            ),
            Expanded(child: Container()),
          ],
        ),
      ),
    );
  }
}
