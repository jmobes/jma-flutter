import 'package:flutter/material.dart';

Widget loadingViewWithText(String text) {
  return Center(
    child: Row(
      children: [
        Container(margin: const EdgeInsets.all(20), child: Text(text)),
        const CircularProgressIndicator(),
      ],
    ),
  );
}

Widget loadingViewWithText2(String text) {
  return Center(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(height: 200),
        Container(margin: const EdgeInsets.only(right: 20), child: Text(text)),
        const CircularProgressIndicator(),
      ],
    ),
  );
}
