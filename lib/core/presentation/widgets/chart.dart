import 'package:flutter/material.dart';
import 'package:jma/core/domain/entities/chart_data.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

Widget appChart({
  required String chartTitle,
  required List<ChartData> chartData,
  required List<String> categories,
  required double visibleMax,
}) {
  return SfCartesianChart(
    primaryXAxis: CategoryAxis(visibleMaximum: visibleMax),
    zoomPanBehavior: ZoomPanBehavior(
      enablePinching: true,
      zoomMode: ZoomMode.x,
      enablePanning: true,
    ),
    primaryYAxis: LogarithmicAxis(logBase: 20),
    // Chart title
    title: ChartTitle(
      text: chartTitle,
    ),
    // Enable legend
    legend: Legend(isVisible: true),
    // Enable tooltip
    tooltipBehavior: TooltipBehavior(enable: true),
    series: <ChartSeries<ChartData, String>>[
      ColumnSeries<ChartData, String>(
          color: Colors.teal[500],
          dataSource: chartData,
          xValueMapper: (ChartData total, _) => total.date,
          yValueMapper: (ChartData total, _) => total.values[0],
          name: categories[0],
          // Enable data label
          dataLabelSettings: const DataLabelSettings(isVisible: true),
          markerSettings: const MarkerSettings(isVisible: true)),
      ColumnSeries<ChartData, String>(
        color: Colors.amber[500],
        dataSource: chartData,
        xValueMapper: (ChartData total, _) => total.date,
        yValueMapper: (ChartData total, _) => total.values[1],
        name: categories[1],
        // Enable data label
        dataLabelSettings: const DataLabelSettings(isVisible: true),
        markerSettings: const MarkerSettings(isVisible: true),
      ),
      ColumnSeries<ChartData, String>(
        color: Colors.red[300],
        dataSource: chartData,
        xValueMapper: (ChartData total, _) => total.date,
        yValueMapper: (ChartData total, _) => total.values[2],
        name: categories[2],
        // Enable data label
        dataLabelSettings: const DataLabelSettings(isVisible: true),
        markerSettings: const MarkerSettings(isVisible: true),
      ),
    ],
  );
}
