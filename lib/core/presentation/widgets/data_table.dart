import 'package:flutter/material.dart';
import 'package:jma/core/presentation/constants/app_colors.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

Widget getDataTable(
  Key key,
  DataGridSource dataGridSource,
  List<String> columns,
) {
  return SfDataGridTheme(
    data: SfDataGridThemeData(
      headerColor: AppColors.buttonandtitle,
    ),
    child: SfDataGrid(
      isScrollbarAlwaysShown: true,
      rowsPerPage: 10,
      key: key,
      allowSorting: true,
      source: dataGridSource,
      columns: _getColumns(columns),
      columnWidthMode: ColumnWidthMode.fill,
    ),
  );
}

Widget getDataTable2(
  Key key,
  DataGridSource dataGridSource,
  List<String> columns,
) {
  return SfDataGridTheme(
    data: SfDataGridThemeData(
      headerColor: AppColors.buttonandtitle,
    ),
    child: SfDataGrid(
      isScrollbarAlwaysShown: true,
      rowsPerPage: 10,
      key: key,
      defaultColumnWidth: 150,
      allowSorting: true,
      source: dataGridSource,
      columns: _getColumns(columns),
    ),
  );
}

List<GridColumn> _getColumns(List<String> columns) {
  return columns.map((e) => _getGridColumn(e)).toList();
}

GridColumn _getGridColumn(
  String title, {
  Color tColor = Colors.white,
}) {
  return GridColumn(
    columnName: title,
    label: Center(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: Text(
                title,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: tColor,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
