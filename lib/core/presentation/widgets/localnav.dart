import 'package:flutter/material.dart';
import 'package:jma/core/presentation/constants/controllers.dart';
import 'package:jma/core/presentation/routing/router.dart';
import 'package:jma/core/presentation/routing/routes.dart';

Navigator localNavigator() => Navigator(
      key: navigationController.navigationKey,
      onGenerateRoute: generateRoute,
      initialRoute: dashboardPageDisplayName,
    );
