import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jma/core/domain/use_case/get_server_date_use_case.dart';
import 'package:jma/core/presentation/manager/server_date_state.dart';
import 'package:jma/core/usecases/use_case.dart';

class ServerDateCubit extends Cubit<ServerDateState> {
  final GetServerDateUseCase getServerDateUseCase;

  ServerDateCubit(this.getServerDateUseCase) : super(ServerDateInitial());

  Future<void> getServerDate() async {
    emit(ServerDateLoading());

    final result = await getServerDateUseCase(NoParams());

    result.fold(
      (l) => emit(ServerDateFailed(l.message)),
      (r) {
        final theDates = r.currentDate.split('-');
        final year = int.parse(theDates[2]);
        final month = int.parse(theDates[1]);
        final day = int.parse(theDates[0]);
        final DateTime date = DateTime(
          year,
          month,
          day,
        );
        emit(ServerDateLoaded(date));
      },
    );
  }
}
