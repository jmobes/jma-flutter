import 'dart:convert';
import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jma/core/presentation/manager/export/state.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';
import 'package:syncfusion_flutter_datagrid_export/src/export_to_excel.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart'
    hide Alignment, Column, Row;

class ExportCubit extends Cubit<ExportCubitState> {
  ExportCubit() : super(const ExportInitialState());

  Future<void> exportDataGridToExcel(
      GlobalKey<SfDataGridState> _key, String title) async {
    try {
      final Workbook workbook = _key.currentState!.exportToExcelWorkbook();

      final List<int> bytes = workbook.saveAsStream();
      workbook.dispose();

      await AnchorElement(
          href:
              'data:application/octet-stream;charset=utf-16le;base64,${base64.encode(bytes)}')
        ..setAttribute('download', title)
        ..click();
    } on Exception catch (e) {
      emit(ExportFailedState(e.toString()));
    }
  }
}
