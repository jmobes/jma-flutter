import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class ExportCubitState {
  const ExportCubitState();
}

class ExportInitialState extends ExportCubitState {
  const ExportInitialState();
}

class ExportFailedState extends ExportCubitState implements Equatable {
  final String error;

  const ExportFailedState(this.error);

  @override
  List<Object?> get props => [error];

  @override
  bool? get stringify => false;
}
