import 'package:equatable/equatable.dart';

abstract class ServerDateState extends Equatable {
  const ServerDateState();
}

class ServerDateInitial extends ServerDateState {
  @override
  List<Object> get props => [];
}

class ServerDateLoading extends ServerDateState {
  @override
  List<Object> get props => [];
}

class ServerDateLoaded extends ServerDateState {
  final DateTime serverDate;

  const ServerDateLoaded(this.serverDate);

  @override
  List<Object> get props => [serverDate];
}

class ServerDateFailed extends ServerDateState {
  final String message;

  const ServerDateFailed(this.message);

  @override
  List<Object> get props => [message];
}
