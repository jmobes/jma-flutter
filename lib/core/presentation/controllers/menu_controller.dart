import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:jma/core/presentation/constants/style.dart';
import 'package:jma/core/presentation/routing/routes.dart';
import 'package:jma/core/utils/constants.dart';

class MenuController extends GetxController {
  static MenuController instance = Get.find();

  RxString activeItem = dashboardPageDisplayName.obs;
  RxString hoverItem = "".obs;

  get lightGrey => null;

  changeActiveitemTo(String itemName) {
    activeItem.value = itemName;
  }

  onHover(String itemName) {
    if (!isActive(itemName)) {
      hoverItem.value = itemName;
    } else {}
  }

  bool isActive(String itemName) => activeItem.value == itemName;

  bool isHovering(String itemName) => hoverItem.value == itemName;

  Widget returnIconFor(String itemName) {
    switch (itemName) {
      case dashboardPageDisplayName:
        return _customIcon(kHomeScreen, itemName);
      case aidRequestPageDisplayName:
        return _customIcon(kOrderIcon, itemName);
      case reportFullPageDisplayName:
        return _customIcon(kReportFull, itemName);
      case reportsimiPageDisplayName:
        return _customIcon(kReportSimi, itemName);
      case reportnotmatchPageDisplayName:
        return _customIcon(kReportNot, itemName);
      case summaryPageDisplayName:
        return _customIcon(kSummaryIcon, itemName);
      case provincesSummaryPageDisplayName:
        return _customIcon(kProvincesIcon, itemName);
      case authenticationPageDisplayName:
        return _customIcon2(Icons.logout, itemName);
      default:
        return _customIcon(kHomeScreen, itemName);
    }
  }

  Widget _customIcon(String icon, String itemName) {
    /*if (isActive(itemName)) {
      return SvgPicture.asset(
        icon,
        width: 22,
        height: 22,
      );
    } else {
      return SvgPicture.asset(
        icon,
        width: 22,
        height: 22,

      );
    }*/
    return SvgPicture.asset(
      icon,
      width: 22,
      height: 22,
    );
  }

  Widget _customIcon2(IconData icon, String itemName) {
    if (isActive(itemName)) {
      return Icon(
        icon,
        size: 22,
        color: dark,
      );
    } else {
      return Icon(
        icon,
      );
    }
  }
}
