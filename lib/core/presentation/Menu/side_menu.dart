import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:jma/core/presentation/Menu/side_menu_item.dart';
import 'package:jma/core/presentation/constants/controllers.dart';
import 'package:jma/core/presentation/constants/custom_text.dart';
import 'package:jma/core/presentation/constants/style.dart';
import 'package:jma/core/presentation/routing/routes.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';
import 'package:jma/features/aid_request/presentation/manager/aid_request_cubit.dart';
import 'package:jma/features/dashboard/presentation/manager/dashboard_cubit.dart';
import 'package:jma/features/summary/presentation/manager/summary_cubit.dart';

import '../../../features/customer_comparison/presentation/manager/customer_comparison_cubit.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;

    return Container(
      color: light,
      child: ListView(
        children: [
          if (Responsive.isSmallScreen(context))
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 40,
                ),
                Row(
                  children: [
                    SizedBox(width: _width / 48),
                    Flexible(
                      child: CustomText(
                        text: "القائمة",
                        size: 20,
                        weight: FontWeight.bold,
                        color: active,
                      ),
                    ),
                    SizedBox(width: _width / 48),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          Divider(
            color: lightGrey.withOpacity(.1),
          ),
          Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: sideMenuItemRoutes
                  .map(
                    (item) => SideMenuItem(
                      itemName: item.name,
                      onTap: () async {
                        if (item.route == authenticationPageRoute) {
                          Get.offAllNamed(authenticationPageRoute);
                          menuController
                              .changeActiveitemTo(dashboardPageDisplayName);
                        }

                        if (!menuController.isActive(item.name)) {
                          context.read<CustomerComparisonCubit>().clear();
                          context.read<AidRequestCubit>().clear();
                          context.read<SummaryCubit>().clear();
                          context.read<DashboardCubit>().clear();

                          menuController.changeActiveitemTo(item.name);
                          if (Responsive.isSmallScreen(context)) Get.back();
                          navigationController.navigateTo(item.route);
                        }
                      },
                    ),
                  )
                  .toList(),
            ),
          )
        ],
      ),
    );
  }
}
