import 'package:flutter/material.dart';
import 'package:jma/core/presentation/constants/style.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';

AppBar topNavigationBar(BuildContext context, GlobalKey<ScaffoldState> key) =>
    AppBar(
      leading: !Responsive.isSmallScreen(context)
          ? Text('')
          : IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                key.currentState!.openDrawer();
              }),
      title: Container(
          child: Row(children: [
        Visibility(
          visible: !Responsive.isSmallScreen(context),
          child: Text("القائمة",
              style: TextStyle(
                color: Colors.blue,
                fontSize: 15,
                fontWeight: FontWeight.bold,
              )),
        ),
      ])),
      iconTheme: IconThemeData(color: dark),
      elevation: 0,
      backgroundColor: Colors.transparent,
    );
