import 'package:flutter/material.dart';
import 'package:jma/core/presentation/Menu/horizontal_menu_item.dart';
import 'package:jma/core/presentation/Menu/vertical_menu_item.dart';
import 'package:jma/core/presentation/widgets/responsive.dart';

class SideMenuItem extends StatelessWidget {
  String itemName;

  final VoidCallback onTap;
  SideMenuItem({
    Key? key,
    required this.itemName,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (Responsive.isMediumScreen(context)) {
      return VerticalMenuItem(itemName: itemName, onTap: onTap);
    } else {
      return HorizontalMenuItem(itemName: itemName, onTap: onTap);
    }
  }
}
