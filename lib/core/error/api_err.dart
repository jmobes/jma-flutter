import 'package:json_annotation/json_annotation.dart';

part 'api_err.g.dart';

@JsonSerializable()
class APIError {
  String errorType;
  String title;
  int status;
  String traceId;
  String errors;

  APIError(this.errorType, this.title, this.status, this.traceId, this.errors);

  factory APIError.fromJson(Map<String, dynamic> json) =>
      _$APIErrorFromJson(json);
}
/*
{
    "errorType": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "خطأ",
    "status": -1,
    "traceId": "ae743db6-cb55-473e-9a73-26db4dbe1de8",
    "errors": "اسم المستخدم أو كلمة السر غير صحيحة"
}
*/