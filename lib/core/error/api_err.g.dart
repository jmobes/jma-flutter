// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_err.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

APIError _$APIErrorFromJson(Map<String, dynamic> json) => APIError(
      json['errorType'] as String,
      json['title'] as String,
      json['status'] as int,
      json['traceId'] as String,
      json['errors'] as String,
    );

Map<String, dynamic> _$APIErrorToJson(APIError instance) => <String, dynamic>{
      'errorType': instance.errorType,
      'title': instance.title,
      'status': instance.status,
      'traceId': instance.traceId,
      'errors': instance.errors,
    };
