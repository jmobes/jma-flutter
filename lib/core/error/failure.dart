import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  final String message;

  const Failure({required this.message});

  @override
  List<Object?> get props => [];
}

// General failures
class ServerFailure extends Failure {
  const ServerFailure({required String message}) : super(message: message);

  @override
  List<Object?> get props => [message];
}

class CacheFailure extends Failure {
  const CacheFailure() : super(message: '');

  @override
  List<Object?> get props => [message];
}
