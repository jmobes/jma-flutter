import 'package:dartz/dartz.dart';
import 'package:jma/core/data/remote/data_sources/server_date_remote_data_source.dart';
import 'package:jma/core/domain/entities/server_date.dart';
import 'package:jma/core/domain/repositories/server_date_repository.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/utils/helpers.dart';

class ServerDateRepositoryImpl implements ServerDateRepository {
  final ServerDateRemoteDataSource remoteDataSource;

  const ServerDateRepositoryImpl(this.remoteDataSource);

  @override
  Future<Either<Failure, ServerDate>> getServerDate() {
    return getRepositoryResponse<ServerDate>(remoteDataSource.getServerDate());
  }
}
