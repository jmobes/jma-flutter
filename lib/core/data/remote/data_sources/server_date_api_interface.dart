import 'package:dio/dio.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/utils/constants.dart';
import 'package:retrofit/http.dart';

part 'server_date_api_interface.g.dart';

@RestApi(baseUrl: kBaseUrl)
abstract class ServerDateAPIInterface {
  factory ServerDateAPIInterface(Dio dio){
    return _ServerDateAPIInterface(dio);
  }

  @POST(kGetServerDateEndpoint)
  Future<APIResponse> getServerDate(@Body() Map<String, dynamic> map);
}
