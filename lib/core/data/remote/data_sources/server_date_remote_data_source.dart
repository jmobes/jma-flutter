import 'package:equatable/equatable.dart';
import 'package:jma/core/data/remote/data_sources/server_date_api_interface.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/data/remote/resource/app_api_res.dart';
import 'package:jma/core/data/remote/resource/lang_params.dart';
import 'package:jma/core/utils/helpers.dart';

abstract class ServerDateRemoteDataSource {
  Future<APPApiResponse> getServerDate();
}

class ServerDateRemoteDataSourceImpl implements ServerDateRemoteDataSource {
  final ServerDateAPIInterface apiInterface;

  ServerDateRemoteDataSourceImpl({
    required this.apiInterface,
  });

  @override
  Future<APPApiResponse> getServerDate() {
    return getAppApiResponse<APIResponse>(
      apiInterface.getServerDate(
        const LangParamsReq('AR').toJson(),
      ),
    );
  }
}
