import 'package:jma/core/domain/entities/server_date.dart';

class ServerDateModel extends ServerDate {
  const ServerDateModel({required String currentDate})
      : super(currentDate: currentDate);

  factory ServerDateModel.fromJson(Map<String, dynamic> json) {
    return ServerDateModel(
      currentDate: (json['currentDate'] as String).split(' ')[0],
    );
  }
}
