import 'package:jma/core/domain/entities/token.dart';

/// "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDYyMjY4NTgsImlzcyI6IkplcGNvLmNvbSIsImF1ZCI6IkplcGNvLmNvbSJ9.izEV4wWzS7P_ffF99yIwIn1TJIE2jxZZOZtU0lHZWL8"

class TokenModel extends Token {
  const TokenModel({
    required String token,
  }) : super(
          token: token,
        );

  factory TokenModel.fromJson(Map<String, dynamic> json) {
    return TokenModel(
      token: json['token'] as String,
    );
  }
}
