import 'package:json_annotation/json_annotation.dart';

part 'api_res.g.dart';

@JsonSerializable()
class APIResponse {
  String statusCode;
  String message;
  dynamic body;

  APIResponse(this.statusCode, this.message, this.body);

  factory APIResponse.fromJson(Map<String, dynamic> json) =>
      _$APIResponseFromJson(json);

  Map<String, dynamic> toJson() => _$APIResponseToJson(this);
}
