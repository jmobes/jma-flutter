import 'package:equatable/equatable.dart';

class LangParamsReq extends Equatable {
  final String languageId;

  const LangParamsReq(this.languageId);

  Map<String, dynamic> toJson() {
    return {'LanguageId': languageId};
  }

  @override
  // TODO: implement props
  List<Object?> get props => [];
}
