class APPApiResponse<T> {
  dynamic body;
  String? message;
  dynamic error;

  APPApiResponse({this.body, this.message, this.error});
}
