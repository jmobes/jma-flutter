//routes
const String kSplashRoute = '/';
const String kLoginRoute = '/';
const String kDashboardRoute = '/dbScreen';

//API
const String kBaseUrl = 'https://mobile.jepco.com.jo/EMRCPortalApis/';

///ENDPOINTS
const String kGetTokenEndpoint = 'LoginController/Login';
const String kLoginEndpoint = 'EMRCEnergyAidJepcoCustomers/UserLogin';
const String kGetTotalsEndpoint =
    'EMRCEnergyAidJepcoCustomers/GetJepocoCustomersDesevreDashboard';
const String kGetLastAidRequestsEndpoint =
    'EMRCEnergyAidJepcoCustomers/GetCustomersDesevreAidsLastStatusData';
const String kGetAidRequestsEndpoint =
    'EMRCEnergyAidJepcoCustomers/GetCustomersDesevreAidsHistory';
const String kGetFullMCustomerComparisonsEndpoint =
    'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDataFullMatch';
const String kGetSimiMCustomerComparisonsEndpoint =
    'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDatasimiMatch';
const String kGetNoMCustomerComparisonsEndpoint =
    'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDataNotMatch';
const String kExportAllFullMatchEndpoint =
    'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDataFullMatchExportExcel';
const String kExportAllSimiMatchEndpoint =
    'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDatasimiMatchExportExcel';
const String kExportAllNoMatchEndpoint =
    'EMRCEnergyAidJepcoCustomers/CompareJepocoCustomersWithEMRCDataNotMatchExportExcel';
const String kGetSummaryTotalsEndpoint =
    'EMRCEnergyAidJepcoCustomers/GetJepocoCustomersDesevrePerView';
const String kGetProvincesSummaryTotalsEndpoint =
    'EMRCEnergyAidJepcoCustomers/GetJepocoCustomersGovernateDashboard';

const String kGetServerDateEndpoint =
    'EMRCEnergyAidJepcoCustomers/GetServerCurrentDate';

//keys
const String kKeyApiToken = 'api_token';
const String kKeyLoggedIn = 'isLoggedIn';
const String kKeyUser = 'user';

// images
const String kSaudi = 'assets/images/saudi.png';
const String kUnitedStates = 'assets/images/united-states.png';
const String kLogo = 'assets/images/logo.svg';
const String kBackground = 'assets/images/background.jpg';

//icons
const String kHomeScreen = 'assets/images/dashboard.svg';
const String kOrderIcon = 'assets/images/orderIconScreen.svg';
const String kReportFull = 'assets/images/reportFullmatch.svg';
const String kReportSimi = 'assets/images/reportSimi.svg';
const String kReportNot = 'assets/images/reportNot.svg';
const String kSummaryIcon = 'assets/images/summaryIcon.svg';
const String kProvincesIcon = 'assets/images/provincesSummaryIcon.svg';
