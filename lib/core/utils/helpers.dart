import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:jma/core/data/remote/resource/api_res.dart';
import 'package:jma/core/data/remote/resource/app_api_res.dart';
import 'package:jma/core/domain/entities/server_date.dart';
import 'package:jma/core/error/api_err.dart';
import 'package:jma/core/error/failure.dart';
import 'package:jma/core/network/conn_interceptor.dart';
import 'package:jma/features/aid_request/data/remote/models/aid_request_model.dart';
import 'package:jma/features/aid_request/domain/entities/aid_request.dart';
import 'package:jma/features/auth/data/remote/models/user_model.dart';
import 'package:jma/features/customer_comparison/data/remote/models/customer_comparison_model.dart';
import 'package:jma/features/customer_comparison/data/remote/models/export_all_model.dart';
import 'package:jma/features/customer_comparison/domain/entities/customer_comparison.dart';
import 'package:jma/features/customer_comparison/domain/entities/export_all.dart';
import 'package:jma/features/dashboard/data/remote/models/dbtotal_model.dart';
import 'package:jma/features/dashboard/domain/entities/dbtotal.dart';
import 'package:jma/features/summary/data/remote/models/province_summary_totals_model.dart';
import 'package:jma/features/summary/data/remote/models/summary_totals_model.dart';
import 'package:jma/features/summary/domain/entities/amman_totals.dart';
import 'package:jma/features/summary/domain/entities/balqa_totals.dart';
import 'package:jma/features/summary/domain/entities/madaba_totals.dart';
import 'package:jma/features/summary/domain/entities/province_totals.dart';
import 'package:jma/features/summary/domain/entities/provinces_summary_totals.dart';
import 'package:jma/features/summary/domain/entities/summary_totals.dart';
import 'package:jma/features/summary/domain/entities/zarqa_totals.dart';

import '../data/remote/models/server_date_model.dart';

APPApiResponse handleSuccess<T>(T response) {
  dynamic rawRes;
  if (response is APIResponse) {
    rawRes = response.body;
  } else {
    rawRes = response;
  }

  final APPApiResponse appApiResponse = APPApiResponse();
  appApiResponse.body = rawRes;
  return appApiResponse;
}

APPApiResponse handleError(dynamic e) {
  final APPApiResponse appApiResponse = APPApiResponse();
  appApiResponse.error = e;
  return appApiResponse;
}

Future<APPApiResponse> getAppApiResponse<T>(Future<T> apiCall) async {
  try {
    final response = await apiCall;

    return handleSuccess(response);
  } on DioError catch (e) {
    return handleError(e.response?.data);
  } catch (e) {
    return handleError(e.toString());
  }
}

Future<Either<Failure, T>> getRepositoryResponse<T>(
  Future<APPApiResponse> call,
  // NetworkInfo networkInfo,
) async {
  // if (await networkInfo.isConnected) {
  final res = await call;
  if (res.body != null) {
    return Right(getResFromJson<T>(res.body));
  } else if (res.error != null) {
    if (res.error is String) {
      return Left(ServerFailure(message: res.error as String));
    } else {
      try {
        final apiError = APIError.fromJson(res.error as Map<String, dynamic>);
        return Left(ServerFailure(message: apiError.errors));
      } on Exception catch (_) {
        return const Left(ServerFailure(message: 'حدث خطأ حاول مرة أخرى'));
      }
    }
  } else {
    return const Left(ServerFailure(message: 'حدث خطأ حاول مرة أخرى'));
  }
  // } else {
  //   return const Left(ServerFailure(message: "لا يوجد إتصال بالإنترنت!"));
  // }
}

T getResFromJson<T>(dynamic body) {
  late T theT;
  switch (T) {
    case UserModel:
      {
        theT = UserModel.fromJson(body as Map<String, dynamic>) as T;
        break;
      }
    case ServerDate:
      {
        theT = ServerDateModel.fromJson(body as Map<String, dynamic>) as T;
        break;
      }
    case List<DBTotal>:
      {
        final List<DBTotal> dbTotals = [];
        for (final Map<String, dynamic> total in body) {
          dbTotals.add(DBTotalModel.fromJson(total));
        }
        theT = dbTotals as T;
        break;
      }
    case List<AidRequest>:
      {
        final List<AidRequest> dbTotals = [];
        for (final Map<String, dynamic> aidRequest in body) {
          dbTotals.add(AidRequestModel.fromJson(aidRequest));
        }
        theT = dbTotals as T;
        break;
      }
    case List<CustomerComparison>:
      {
        final List<CustomerComparison> customerComparisons = [];
        for (final Map<String, dynamic> customerComparison in body) {
          customerComparisons
              .add(CustomerComparisonModel.fromJson(customerComparison));
        }
        theT = customerComparisons as T;
        break;
      }
    case ExportAll:
      {
        theT = ExportAllModel.fromJson(body as Map<String, dynamic>) as T;
        break;
      }
    case List<SummaryTotals>:
      {
        final List<SummaryTotals> summaryTotals = [];
        for (final Map<String, dynamic> summaryTotal in body) {
          summaryTotals.add(SummaryTotalsModel.fromJson(summaryTotal));
        }
        theT = summaryTotals as T;
        break;
      }
    case ProvincesSummaryTotals:
      {
        final theModel =
            ProvinceSummaryTotalsModel.fromJson(body as Map<String, dynamic>);

        final ammanTotals = AmmanTotals([
          theModel.intAmmanUserTypeTotalJordaian,
          theModel.intAmmanUserTypeTotalGaza,
          theModel.intAmmanUserTypeforeign
        ], [
          theModel.intAmmanUserCatgoryNormal,
          theModel.intAmmanUserCatgoryTakaful,
          theModel.intAmmanUserCatgorySyrianRefugee,
          theModel.intAmmanUserCatgoryMarriedtoNonJordanian,
        ]);
        final zarqaTotals = ZarqaTotals([
          theModel.intZarqaUserTypeTotalJordaian,
          theModel.intZarqaTypeTotalGaza,
          theModel.intZarqaUserTypeforeign
        ], [
          theModel.intZarqaUserCatgoryNormal,
          theModel.intZarqaUserCatgoryTakaful,
          theModel.intZarqaUserCatgorySyrianRefugee,
          theModel.intZarqaUserCatgoryMarriedtoNonJordanian,
        ]);
        final madabaTotals = MadabaTotals([
          theModel.intMadbaUserTypeTotalJordaian,
          theModel.intMadbaTypeTotalGaza,
          theModel.intMadbaUserTypeforeign
        ], [
          theModel.intMadbaUserCatgoryNormal,
          theModel.intMadbaUserCatgoryTakaful,
          theModel.intMadbaUserCatgorySyrianRefugee,
          theModel.intMadbaUserCatgoryMarriedtoNonJordanian,
        ]);
        final balqaTotals = BalqaTotals([
          theModel.intBalqaUserTypeTotalJordaian,
          theModel.intBalqaTypeTotalGaza,
          theModel.intBalqaUserTypeforeign
        ], [
          theModel.intBalqaUserCatgoryNormal,
          theModel.intBalqaUserCatgoryTakaful,
          theModel.intBalqaUserCatgorySyrianRefugee,
          theModel.intBalqaUserCatgoryMarriedtoNonJordanian,
        ]);

        final List<ProvinceTotals> theTotals = [
          ammanTotals,
          zarqaTotals,
          madabaTotals,
          balqaTotals,
        ];

        theT = ProvincesSummaryTotals(
            theModel.intAmmanTotalRequestsAid,
            theModel.intZarqaTotalRequestsAid,
            theModel.intMadbaTotalRequestsAid,
            theModel.intBalqaTotalRequestsAid,
            theTotals,) as T;
        break;
      }
  }

  return theT;
}

void showSnackBar(BuildContext context, String msg) {
  ScaffoldMessenger.of(context)
    ..hideCurrentSnackBar()
    ..showSnackBar(
      SnackBar(
        content: Text(msg),
      ),
    );
}

Future showProgressDialog(BuildContext context) {
  final AlertDialog alert = AlertDialog(
    content: Row(
      children: [
        const CircularProgressIndicator(),
        Container(
          margin: const EdgeInsets.only(left: 7),
          child: const Text("Exporting..."),
        ),
      ],
    ),
  );
  return showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

Dio get appDio {
  final Dio dio = Dio();
  dio.options = BaseOptions(
    connectTimeout: 10000,
    receiveTimeout: 10000,
    sendTimeout: 10000,
    contentType: 'application/json',
  );

  dio.interceptors.add(ConnInterceptor(dio));

  return dio;
}
