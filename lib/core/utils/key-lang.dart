// ignore: avoid_classes_with_only_static_members
abstract class KeyLang {
  static String appName = 'appName';
  static String selectLanguage = 'selectLanguage';
  static String english = 'english';
  static String arabic = 'arabic';
  static String username = 'username';
  static String password = 'password';
  static String ok = 'ok';
  static String cancel = 'cancel';
  static String login = 'login';
  static String dropdownError = 'dropdownError';
  static String deletError = 'deletError';
  static String alert = 'alert';
  static String titleloginweb = 'titleloginweb';
  static String titileweb = 'titileweb';
  static String back = 'back';
  static String logout = 'logout';
  static String export = 'export';
  static String search = 'search';
  static String fromDateC = 'fromDateC';
  static String toDateC = 'toDateC';
  static String errordate = 'errordate';
  static String errorDateMore30 = 'errorDateMore30';
  static String clear = 'clear';
  static String errorusername = 'errorusername';
  static String errorpassword = 'errorpassword';
  static String errorpassword2 = 'errorpassword2';
  static String homeScreen = 'homeScreen';
  static String totalCustomers = 'totalCustomers';
  static String totalRequests = 'totalRequests';
  static String totalCustomerDeserverAidRequest =
      'totalCustomerDeserverAidRequest';
  static String totalCustomerCancelDeserverRequestAid =
      'totalCustomerCancelDeserverRequestAid';
  static String totalCustomerDeserverAidRequestCountToday =
      'totalCustomerDeserverAidRequestCountToday';
  static String totalCustomerCancelDeserverAidRequestCountToday =
      'totalCustomerCancelDeserverAidRequestCountToday';
  static String fileNumber = 'fileNumber';
  static String emrcMeterNumber = 'emrcMeterNumber';
  static String jepcoMeterNumber = 'jepcoMeterNumber';
  static String emrcCustomerName = 'emrcCustomerName';
  static String jepcoCustomerName = 'jepcoCustomerName';
  static String fullNameCompersionStatus = 'fullNameCompersionStatus';
  static String fullNameCompersionPercentage = 'fullNameCompersionPercentage';
  static String emrcCustomerPhoneNumber = 'emrcCustomerPhoneNumber';
  static String emrcNationalNumber = 'emrcNationalNumber';
  static String total_read_chart_title = 'total_read_chart_title';
  static String loading_total_read_chart = 'loading_total_read_chart';
  static String chart_type = 'chart_type';
  static String total_reads = 'total_reads';
  static String read = 'read';
  static String unread = 'unread';
  static String summary_chart_title = 'summary_chart_title';
  static String week = 'week';
  static String month = 'month';
  static String duringPeriod = 'duringPeriod';
  static String current_month = 'current_month';
  static String last12months = 'last12months';
  static String customers = 'customers';
  static String requests = 'requests';

  static const String fromDate = 'fromDate';
  static const String toDate = 'toDate';
  static const String meterNumber = 'meterNumber';
  static const String firstName = 'firstName';
  static const String secondName = 'secondName';
  static const String thirdName = 'thirdName';
  static const String lastName = 'lastName';
  static const String requestType = 'requestType';
  static const String requestDate = 'requestDate';
  static const String aidRequest = 'aidRequest';
  static const String cancelAid = 'cancelAid';
  static const String all = 'all';
  static const String history = 'history';
  static const String exportAll = 'exportAll';
  static const String reports = 'reports';
  static const String searchThrough = 'searchThrough';
  static const String errorFileNo = 'errorFileNo';
  static const String errorMeterNo = 'errorMeterNo';
  static String supportType = 'supportType';
  static String renewableFlag = 'renewableFlag';
  static String userCategory = 'userCategory';
  static String userType = 'userType';
  static String fullSupport = 'fullSupport';
  static String unstableSupport = 'unstableSupport';
  static String renewableEnergy = 'renewableEnergy';
  static String nonrenewableEnergy = 'nonrenewableEnergy';
  static String standardSubscription = 'standardSubscription';
  static String royalGenerosityAndaidfund = 'royalGenerosityAndaidfund';
  static String marriedtoanonJordanian = 'marriedtoanonJordanian';
  static String syrianRefugee = 'syrianRefugee';
  static String jordanian = 'jordanian';
  static String temporaryPassports = 'temporaryPassports';
  static String gaza = 'gaza';
  static String notFound = 'notFound';
  static String activeSubscriptions = 'activeSubscriptions';
  static String inactiveSubscriptions = 'inactiveSubscriptions';
  static String disconnctedMeters = 'disconnctedMeters';
  static String numberofMetersService = 'numberofMetersService';
  static String residentialNumber = 'residentialNumber';

  // static String notFound = 'notFound';

}
