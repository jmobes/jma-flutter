import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:jma/core/data/remote/models/token_model.dart';
import 'package:jma/core/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConnInterceptor extends QueuedInterceptor {
  final Dio _dio;

  ConnInterceptor(this._dio);

  @override
  Future<void> onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    if (options.headers.containsKey('removeAuth')) return handler.next(options);

    // get tokens from local storage, you can use Hive or flutter_secure_storage
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    final String? token = prefs.getString(kKeyApiToken);

    var _refreshed = true;

    if (token == null) {
      // regenerate access token
      _dio.interceptors.requestLock.lock();
      _dio.interceptors.responseLock.lock();
      _refreshed = await getToken() != null;
      _dio.interceptors.requestLock.unlock();
      _dio.interceptors.responseLock.unlock();
    }

    if (_refreshed) {
      // add access token to the request header
      options.headers["Authorization"] = 'Bearer $token';
      return handler.next(options);
    } else {
      // create custom dio error
      final error = DioError(requestOptions: options);
      return handler.reject(error);
    }
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    handler.next(response);
  }

  @override
  Future<void> onError(DioError e, ErrorInterceptorHandler handler) async {
    if (e.response?.statusCode == 403 || e.response?.statusCode == 401) {
      _dio.interceptors.requestLock.lock();
      _dio.interceptors.responseLock.lock();

      final refreshedToken = await getToken();

      _dio.interceptors.requestLock.unlock();
      _dio.interceptors.responseLock.unlock();

      if (refreshedToken != null) {
        final res = await retry(e, refreshedToken);
        if (res is DioError) {
          return handler.reject(res);
        } else {
          return handler.resolve(res as Response<dynamic>);
        }
      } else {
        return handler.reject(e);
      }
    } else {
      return handler.reject(e);
    }
  }

  Future<String?> getToken() async {
    try {
      final http.Response response = await http.post(
        Uri.parse('http://192.168.123.103/EMRCAPIPortal/LoginController/Login'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          "UserName": "EMRCPortalIntegrationUser",
          "Password": "EMRCPortal@jepco@123"
        }),
      );

      if (response.statusCode == 200) {
        final jsonRes = jsonDecode(response.body) as Map<String, dynamic>;
        // final jsonBody = jsonDecode(jsonRes['body']);
        final body = jsonRes['body'];
        final tokenModel = TokenModel.fromJson(body as Map<String, dynamic>);

        final SharedPreferences prefs = await SharedPreferences.getInstance();

        final isOk = await prefs.setString(kKeyApiToken, tokenModel.token);

        if (isOk) {
          return tokenModel.token;
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<dynamic> retryNoDio(DioError e, String token) async {
    final headers = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
      'Content-Type': 'application/json'
    };
    final path = e.requestOptions.baseUrl + e.requestOptions.path;
    final request = http.Request(e.requestOptions.method, Uri.parse(path));
    request.body = json.encode(e.requestOptions.data);
    request.headers.addAll(headers);

    final http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      return http.Response.fromStream(response);
    } else {
      return e;
    }
  }

  Future<dynamic> retry(DioError e, String token) async {
    final Dio dio = Dio();
    dio.options.baseUrl = kBaseUrl;
    final RequestOptions requestOptions = e.requestOptions;
    final opts = Options(method: requestOptions.method);
    dio.options.headers["Authorization"] = "Bearer $token";
    dio.options.headers['Content-Type'] = 'application/json';
    dio.options.headers["Accept"] = "*/*";
    opts.receiveDataWhenStatusError = true;

    final data = jsonEncode(requestOptions.data);
    final path = requestOptions.path;
    try {
      return await dio.request(path, options: opts, data: data);
    } on DioError catch (e) {
      return e;
    }
  }
}
