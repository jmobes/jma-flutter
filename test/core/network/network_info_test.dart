// import 'package:flutter_test/flutter_test.dart';
// // import 'package:internet_connection_checker/internet_connection_checker.dart';
// import 'package:jma/core/network/network_info.dart';
// import 'package:mockito/mockito.dart';

// import 'network_info_test.mocks.dart';

// // @GenerateMocks([InternetConnectionChecker])
// void main() {
//   group('isConnected', () {
//     test(
//       'should forward the call to DataConnectionChecker.hasConnection',
//       () async {
//         // arrange
//         final MockInternetConnectionChecker mockInternetConnectionChecker =
//             MockInternetConnectionChecker();
//         final NetworkInfo networkInfo =
//             NetworkInfoImpl(mockInternetConnectionChecker);

//         final tHasConnectionFuture = Future.value(true);

//         when(mockInternetConnectionChecker.hasConnection)
//             .thenAnswer((_) => tHasConnectionFuture);
//         // act
//         final result = networkInfo.isConnected;
//         // assert
//         verifyNever(mockInternetConnectionChecker.hasConnection);
//         expect(result, tHasConnectionFuture);
//       },
//     );
//   });
// }
